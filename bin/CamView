#!/usr/bin/env python
#----------------------------------------------------------------------
# Description:
# Author: Carsten Richter <carsten.richter@esrf.fr>
# Modified by: Steven Leake
# Created at: Sa 6. Mai 16:04:24 CEST 2017
# Computer: lid01gpu1.
# System: Linux 3.16.0-4-amd64 on x86_64
#----------------------------------------------------------------------
#
# App for reading out subsequent camera images from given Url
# - image feature matching
# - determine shifts / rotations
# - move center of rotation to point of interest
#
# Currently relies on the newest SILX version (0.5.0)
# Can work for python 3 and python 2
# A suitable python 3 environment is here:
#
#   source /data/id01/inhouse/crichter/venv3.4/bin/activate
#----------------------------------------------------------------------
import os

os.environ.pop("http_proxy", None) # for ID01
os.environ.pop("https_proxy", None)

import sys

for p_i in range(len(sys.path)):
    if "dist-packages" in sys.path[p_i]:
        sys.path.append(sys.path.pop(p_i))


devicetype = "CPU" if "--cpu" in sys.argv else "GPU"

import time
import collections
import platform
PV = platform.python_version()
print("Python version %s"%PV)
import numpy as np
print("Using numpy %s"%np.__version__)
from scipy import linalg, ndimage
from PIL import Image
#print("Using PIL.Image %s"%Image.VERSION)
from PyQt5 import QtGui as Q
from PyQt5 import QtCore
from PyQt5.QtWidgets import QWidget, QMainWindow, QDialog, QApplication, QAction, QGridLayout, QHBoxLayout, QSplitter, QFrame, QFormLayout, QLineEdit, QCheckBox, QComboBox, QLabel, QPushButton, QTextEdit, QDialogButtonBox

print("Using PyQt %s"%QtCore.QT_VERSION_STR)

_use_console = True
_use_console = _use_console and PV.startswith("3.")

import silx
print("Using silx %s"%silx.version)
from silx.gui import plot, qt
from silx.gui.plot import PlotActions
import silx.gui.icons
from silx.image import sift
if _use_console:
    from silx.gui import console


import id01lib
from id01lib import image
from id01lib.process import microscope


iconpath = os.path.dirname(os.path.join(id01lib.__file__))
iconpath = os.path.join(iconpath, "media", "camview.png")


#_default_url = "http://220.221.164.165:8000/jpg/image.jpg"
#_default_url = "http://skycam.mmto.arizona.edu/skycam/latest_image.png"
#_default_url = "http://jimstar11.com/DSICam/SkyEye.jpg"
#_default_url = "http://www.webcam.cannstatter-volksfest.de/2013/live/live.jpg"
_default_url = "http://vidid011.esrf.fr/jpg/1/image.jpg"

_default_motors = ["thx", "thy"]

_hints = dict()

_hints["Exposure"] = 'Press to acquire new picture.'
_hints["Get COR"] = ('Identifies features on both images, estimates '
                     'the affine transform between them and returns '
                     'Center Of Rotation.')
_hints["POI to COR"] = ('Move selected Point Of Interest into Center Of '
                      'Rotation')
_hints["Get Sharpness"] = ('Compute a measure for the sharpness of the image '
                        'in arbitrary units. Uses the latest image.')
_hints["AutoFocus"] = ('Use ROI sharpness and optimization of a motor position '
                       'to focus the image.')
_hints["background"] = 'take a background image'                  
_hints["bg on/off"] = 'subtract the image or not'
_hints["selectPOI"] = 'Select Point of Interest (POI)'
_hints["Navg"] = 'Number of subsequent camera images to average'
_hints["Refresh"] = 'refresh time of live plot window'
_hints["enhance"] = 'Strech contrast of the camera image'
_hints["saveit"] = 'Save the new image to the current directory'


_valid = {int:Q.QIntValidator(),
          float:Q.QDoubleValidator()}

class CrosshairAction(PlotActions.CrosshairAction):
    """
        Overridden silx class
    """
    def _actionTriggered(self, checked=False):
        super(CrosshairAction, self)._actionTriggered(checked)
        if checked:
            self.plot.setInteractiveMode("select")
        else:
            self.plot.setInteractiveMode(**self.plot._defaultMode)



class ClearPointsAction(PlotActions.PlotAction):
    def __init__(self, plot, parent=None):
        super(ClearPointsAction, self).__init__(
                            plot,
                            icon='image-select-erase',
                            text='Clear SIFT keypoints',
                            tooltip='Clear keypoints found by SIFT',
                            triggered=self.trigger,
                            parent=parent)

    def trigger(self):
        self.plot.update_keypoints(None)



class CamPlot(plot.PlotWindow):
    roi = None
    poi = None
    def __init__(self, data=None, title=None, parent=None):
        super(CamPlot, self).__init__(parent=parent, resetzoom=True, 
                             autoScale=False,
                             logScale=False, grid=False,
                             curveStyle=False, colormap=True,
                             aspectRatio=True, yInverted=False,
                             copy=True, save=True, print_=True,
                             control=False,
                             roi=False, mask=False)
        self.setXAxisAutoScale(True)
        self.setYAxisAutoScale(True)
        self.setKeepDataAspectRatio(True)
        self.setYAxisInverted(True)
        #self.setKeepDataAspectRatio(True)

        if not data is None:
            self.addImage(data, resetzoom=True)
        self.setGraphTitle(title)

        
        clearpoints = ClearPointsAction(self)
        self.toolBar().addAction(clearpoints)
        self._clearpoints = clearpoints

        #self.timer=qt.QTimer()
        #self.timer.timeout.connect(self._queryupdateImage)
        #self.timer.start(100)
        #r = self.getDataRange()
        #self.setGraphXLimits(r[0][0], r[0][1])
        #self.setGraphYLimits(r[1][0], r[1][1])
        #self.resetZoom()
        #self.profile = plot.Profile.ProfileToolBar(plot=self)
        #self.addToolBar(self.profile)

            
    def update_roi(self, event):
#        if "button" in event and event["button"] == "right":
#            self.remove("roi")
#            self.roi = None
#            return # Problem: No right click signal in draw mode

        xlim = np.clip(event["xdata"], 0, None)
        ylim = np.clip(event["ydata"], 0, None)
        xlim.sort()
        ylim.sort()

        if xlim[0]==xlim[1] or ylim[0]==ylim[1]:
            self.remove("roi")
            self.roi = None
            if self.getInteractiveMode()["mode"] is 'draw':
                self.parent().parent().echo("Empty ROI -> removed ROI.")

            return

        self.roi = xlim.astype(int), ylim.astype(int)

        
        x = [xlim[i] for i in (0,1,1,0,0)]
        y = [ylim[i] for i in (0,0,1,1,0)]
        self.addCurve(x, y, resetzoom=False, legend="roi", color="r")

    
    def update_poi(self, event,name):
        print(name)
        exec("self.%s = %s = event[\"x\"], event[\"y\"]"%(name,name))
        m = "m = self.addMarker(%s[0], %s[1], symbol=\"o\", legend=\"%s\", color=(.3,1.,1.,1.), text=\"%s\")"%(name,name,name,name)
        exec(m)
        #m = self._getItem("marker", m)
        #print(m.getSymbolSize())
        #m.setSymbolSize(1)
        #print(m.getSymbolSize())
        #self.addCurve([event["x"]], [event["y"]], symbol="o", linestyle=" ",
        #              legend="poi", linewidth=5, color="c", resetzoom=False)
        #c = self.getCurve("poi")
        #c.setSymbolSize(10)

    
    def get_roi_data(self):
        imdata= self.getImage().getData()
        if not self.roi is None:
            xlim, ylim = self.roi
            roidata = imdata[ylim[0]:ylim[1], xlim[0]:xlim[1]]
            return roidata
        else:
            return imdata

    def update_keypoints(self, xy=None):
        plotcfg = dict(legend="keypoints", color=(.3,1.,.3,.8), symbol=".",
                       resetzoom=False, linestyle=" ")
        if xy is None:
            self.remove(plotcfg['legend'])
        else:
            self.addCurve(xy[0], xy[1], **plotcfg)




class ControlWidget(QWidget):
    Input = dict()
    def __init__(self, parent=None, **kw):
        super(ControlWidget, self).__init__(parent=parent, **kw)
        self.home()

    def home(self):
        font = Q.QFont()
        font.setPointSize(9)
        self.setFont(font)

        layout = QHBoxLayout(self)
        self.splitter = splitter = QSplitter(QtCore.Qt.Horizontal) 
        layout.addWidget(splitter)
        _reg = self.registerWidget
        self.form = form = QFrame(self)
        form.setFrameShape(QFrame.StyledPanel)
        form.layout = QFormLayout(form)

        hbox = QHBoxLayout()
        url = _reg(QLineEdit(_default_url), "url")

        enhance = QCheckBox('enhance', self)
        enhance.setStatusTip(_hints['enhance'])
        enhance = _reg(enhance, "enhanced")

        saveit = QCheckBox('save', self)
        saveit.setStatusTip(_hints['saveit'])
        saveit = _reg(saveit, "saveit")


        Navg = QLineEdit("1")
        Navg.setValidator(_valid[int])
        Navg.setStatusTip(_hints['Navg'])
        Navg.setMaxLength(3)
        Navg.setFixedWidth(25)
        Navg = _reg(Navg, "Navg")

        hbox.addWidget(QLabel("URL"))
        hbox.addWidget(url)
        #hbox.addSpacing(5)
        form.layout.addRow(hbox)
        hbox = QHBoxLayout()
        hbox.addWidget(QLabel("No. exps to avg"))
        hbox.addWidget(Navg)
        hbox.addSpacing(5)


        refresh = QLineEdit(".5")
        refresh.setValidator(_valid[float])
        refresh.setStatusTip(_hints['Refresh'])
        refresh.setMaxLength(3)
        refresh.setFixedWidth(25)
        refresh = _reg(refresh, "Refresh")
        
        hbox.addWidget(QLabel("Refresh Rate (secs)"))
        hbox.addWidget(refresh)
        hbox.addSpacing(5)
        
        hbox.addWidget(enhance)
        hbox.addWidget(saveit)
        form.layout.addRow(hbox)

        hbox = QHBoxLayout()
        for k in ("E&xposure", "Get CO&R", "POI to COR", "Get Sharpness", "AutoFocus", "background","bg on/off"):
            name = k.replace("&","")
            btn = _reg(QPushButton(k, self), name)
            btn.setStatusTip(_hints[name])
            hbox.addWidget(btn)
        form.layout.addRow(hbox)
        """
        hbox = QHBoxLayout()
        CORx = QLineEdit("0")
        CORx.setValidator(_valid[int])
        #CORx.setStatusTip(_hints['COR'])
        CORx.setMaxLength(3)
        CORx.setFixedWidth(25)
        CORx.textChanged[str].connect(self.onChangedCORx)
        CORx = _reg(CORx, "COR(x)")
        
        hbox.addWidget(QLabel("COR x:"))
        hbox.addWidget(CORx)
        hbox.addSpacing(5)

        CORy = QLineEdit("0")
        CORy.setValidator(_valid[int])
        #CORy.setStatusTip(_hints['COR'])
        CORy.setMaxLength(3)
        CORy.setFixedWidth(25)
        CORy.textChanged[str].connect(self.onChangedCORy)
        CORy = _reg(CORy, "COR(y)")
        
        hbox.addWidget(QLabel("y:"))
        hbox.addWidget(CORy)
        hbox.addSpacing(5)
        
        POI0x = QLineEdit("0")
        POI0x.setValidator(_valid[int])
        #POI0x.setStatusTip(_hints['POI'])
        POI0x.setMaxLength(3)
        POI0x.setFixedWidth(25)
        POI0x.textChanged[str].connect(self.onChangedPOI0x)
        POI0x = _reg(POI0x, "POI0(x)")
        
        hbox.addWidget(QLabel("POI0 x:"))
        hbox.addWidget(POI0x)
        hbox.addSpacing(5)

        POI0y = QLineEdit("0")
        POI0y.setValidator(_valid[int])
        #POI0y.setStatusTip(_hints['POI'])
        POI0y.setMaxLength(3)
        POI0y.setFixedWidth(25)
        POI0y.textChanged[str].connect(self.onChangedPOI0y)
        POI0y = _reg(POI0y, "POI0(y)")
        
        hbox.addWidget(QLabel("y:"))
        hbox.addWidget(POI0y)
        hbox.addSpacing(5)
        
        POI1x = QLineEdit("0")
        POI1x.setValidator(_valid[int])
        #POI1x.setStatusTip(_hints['POI1'])
        POI1x.setMaxLength(3)
        POI1x.setFixedWidth(25)
        POI1x.textChanged[str].connect(self.onChangedPOI1x)
        POI1x = _reg(POI1x, "POI1(x)")
        
        hbox.addWidget(QLabel("POI1 x:"))
        hbox.addWidget(POI1x)
        hbox.addSpacing(5)

        POI1y = QLineEdit("0")
        POI1y.setValidator(_valid[int])
        #POI1y.setStatusTip(_hints['POI1'])
        POI1y.setMaxLength(3)
        POI1y.setFixedWidth(25)
        POI1y.textChanged[str].connect(self.onChangedPOI1y)
        POI1y = _reg(POI1y, "POI1(y)")
        
        hbox.addWidget(QLabel("y:"))
        hbox.addWidget(POI1y)
        hbox.addSpacing(5)
        form.layout.addRow(hbox)
        """

        form.layout.addRow(_reg(QTextEdit(""), "output"))
        self.Input["output"].setReadOnly(True)
        textFont = Q.QFont("Monospace", 9)
        textFont.setStyleHint(Q.QFont.Monospace)
        self.Input["output"].setCurrentFont(textFont)


        splitter.addWidget(form)
        """
        if _use_console:
            banner = "Inspect/Modify `MainWindow` App instance."
            ipython = console.IPythonWidget(self)#, custom_banner=banner)
            #ipython.banner += banner
            mainWindow = self.parent().parent()

            ipython.pushVariables({"MainWindow": mainWindow})
            #ipython.font_size = 
            ipython.change_font_size(-2)
            self.console = ipython
            #ipython.clear()
            splitter.addWidget(ipython)
        else:
            splitter.addWidget(QPushButton("Dummy"))
        """
        splitter.setSizes([500,500])

        self.setLayout(layout)

    def registerWidget(self, QtObject, name):
        self.Input[name] = QtObject
        if isinstance(QtObject, QLineEdit):
            #QtObject.setFixedWidth(length)
            pass
        else:
            QtObject.resize(QtObject.minimumSizeHint())
        return QtObject

    def onChangedCORx(self, text):
        self.CORx=text
        #self.update_poi()
    def onChangedCORy(self, text):
        self.CORx=text 
    def onChangedPOI0x(self, text):
        self.POI0x=text
    def onChangedPOI0y(self, text):
        self.POI0y=text
    def onChangedPOI1x(self, text):
        self.POI1x=text 
    def onChangedPOI1y(self, text):
        self.POI1y=text
        
    def update_poi(self, event,name):
        print("TODO: not implemented")
        #print(name)
        #m = self.addMarker(%s[0], %s[1], symbol="o", legend="%s", color=(.3,1.,1.,1.), text="%s")
        #exec(m)

class Window(QMainWindow):
    _ignoreEvent = False
    _eventSource = None
    resultsCOR = dict()
    def __init__(self):
        super(Window, self).__init__()
        self.setGeometry(200, 100, 1000, 750)
        self.setWindowTitle("Cam view processing")
        if iconpath is not None and os.path.isfile(iconpath):
            print("setting icon %s"%iconpath)
            self.setWindowIcon(Q.QIcon(iconpath))

        extractAction = QAction("&Quit", self)
        extractAction.setShortcut("Ctrl+Q")
        extractAction.setStatusTip('Leave The App')
        extractAction.triggered.connect(self.close_application)

        #testAction = QAction("&Test", self)
        #testAction.setStatusTip('Test action')

        #testAction.triggered.connect(isbusy)

        #self.setStatusBar(Q.QStatusBar())
        self.statusBar()

        mainMenu = self.menuBar()
        fileMenu = mainMenu.addMenu('&File')
        fileMenu.addAction(extractAction)

        #extractAction = QAction(Q.QIcon('todachoppa.png'), 'Quit', self)
        #extractAction = QAction('Quit', self)
        #extractAction.triggered.connect(self.close_application)
        #extractAction.setStatusTip('Leave The App')
        self.toolBar = self.addToolBar("General")
        self.toolBar.addAction(extractAction)
        #self.toolBar.addAction(testAction)

        self.home()
        #self.show()
        self.toggle_background = False

    def home(self):
        cw = QWidget(self)
        self.grid = g = QGridLayout(cw)
        self.setCentralWidget(cw)

        
        data = np.random.random((512,512))

        self.plotLeft  = pleft  = CamPlot(data, "Latest", cw)
        self.plotRight = pright = CamPlot(data, "Previous", cw)
        g.addWidget(pleft,  1,0)
        g.addWidget(pright, 1,1)
        self.plotLive = plive = CamPlot(data, "Live", cw)
        g.addWidget(plive, 0,0)

        crosshair = CrosshairAction(pleft, color="b")
        crosshair.setToolTip(_hints["selectPOI"])
        pleft.toolBar().addAction(crosshair)
        pleft.crosshair = crosshair
        crosshair1 = CrosshairAction(plive, color="b")
        crosshair1.setToolTip(_hints["selectPOI"])
        plive.toolBar().addAction(crosshair1)
        plive.crosshair = crosshair1

        pleft.setCallback( lambda event: self.handle_event(event, "l"))
        pright.setCallback(lambda event: self.handle_event(event, "r"))
        plive.setCallback(lambda event: self.handle_event(event, "live"))

        pleft.setInteractiveMode("draw", shape="rectangle"
                                       , color=(1.,1.,0.,0.8))

        for p in (pleft, pright, plive):
            p._defaultMode = p.getInteractiveMode()

        self.control = control = ControlWidget(cw)
        g.addWidget(control, 0, 1, 1, 1)
        # control.adjustSize()

        # Connect:
        control.Input["Exposure"].clicked.connect(self.update_plots)
        control.Input["Get COR"].clicked.connect(self.get_center_of_rotation)
        control.Input["POI to COR"].clicked.connect(self.poi_to_cor)
        control.Input["Get Sharpness"].clicked.connect(self.calc_sharpness)
        control.Input["AutoFocus"].clicked.connect(self.autofocus)
        control.Input["background"].clicked.connect(self.background)
        control.Input["bg on/off"].clicked.connect(self.toggle_background)

        #self.plotLeft.resetZoom() #doesn't work
        self.show()
        self.refresh=1
        self.POI0x,self.POI0y = 0,0
        self.POI1x,self.POI1y = 0,0
        self.CORx,self.CORy = 0,0
        
        
        
        self.timer=qt.QTimer()
        self.timer.timeout.connect(self._queryupdateImage)
        self.timer.start(self.refresh*1000)

    def _queryupdateImage(self,):
        #if self.pollContinuous:
        self._updateImage()
        
    def _updateImage(self,):
        self.timer.setInterval(self.refresh*1000)
        self.refresh = float(self.control.Input["Refresh"].text())#.toAscii()

        #self.echo("aaah: %i"%self.refresh)

        #iso_time = time.strftime("%Y-%m-%dT%H:%M:%S", time.localtime())
        url = self.control.Input["url"].text()#.toAscii()
        url = str(url)
        Navg = int(self.control.Input["Navg"].text())#.toAscii()
        if Navg < 1:
            #self.echo("Error: need Navg > 0")
            return
        Ntext = "once" if Navg is 1 else "%i times"%Navg
        #self.echo("Exposure - %s"%iso_time)
        #self.echo("Fetching %s %s..."%(url, Ntext))
        try:
            img = image.url2array(url, Navg)
            #self.echo("Image shape: (%i, %i)"%img.shape)
        except Exception as emsg:
            #self.echo("...failed: %s"%emsg)
            return

        do_enhance = self.control.Input["enhanced"].checkState()
        if do_enhance:
            img = image.stretch_contrast(img)

        imLive = self.plotLive.getImage()
        
        if self.toggle_background:
            imLive.setData(img-self.bg_img)
        else:
            imLive.setData(img)
        #self.plotLeft.resetZoom()
        

    def handle_event(self, event, side):
        if event["event"] is "drawingFinished":
            #print(event["xdata"], event["ydata"])
            for p in [self.plotLeft, self.plotRight, self.plotLive]:
                p.update_roi(event)

        elif event["event"]=="limitsChanged"  \
          and not self._eventSource==event["source"]  \
          and not self._ignoreEvent:
            self._eventSource=event["source"]
            self._ignoreEvent = True
            if side is "l":
                self.plotRight.setLimits(*(event["xdata"]+event["ydata"]))
            elif side is "r":
                self.plotLeft.setLimits(*(event["xdata"]+event["ydata"]))
            elif side is "live":
                self.plotLive.setLimits(*(event["xdata"]+event["ydata"]))
            self._ignoreEvent = False
        elif event["event"] is "mouseClicked" and side is "l":
            if event["button"] is "left" and \
              not self.plotLeft.getGraphCursor() is None:
                for p in [self.plotLeft, self.plotRight,self.plotLive]:
                    p.update_poi(event,"poi0")
        elif event["event"] is "mouseClicked" and side is "live":
            if event["button"] is "left" and \
              not self.plotLive.getGraphCursor() is None:
                for p in [self.plotLeft, self.plotRight,self.plotLive]:
                    p.update_poi(event,"poi1")
#            if event["button"] is "right":
#                for p in [self.plotLeft, self.plotRight]:
#                    p.update_roi(event)

    def update_plots(self):
        iso_time = time.strftime("%Y-%m-%dT%H:%M:%S", time.localtime())
        url = self.control.Input["url"].text()#.toAscii()
        url = str(url)
        Navg = int(self.control.Input["Navg"].text())#.toAscii()
        if Navg < 1:
            self.echo("Error: need Navg > 0")
            return
        Ntext = "once" if Navg is 1 else "%i times"%Navg
        self.echo("Exposure - %s"%iso_time)
        self.echo("Fetching %s %s..."%(url, Ntext))
        try:
            img = image.url2array(url, Navg)
            self.echo("Image shape: (%i, %i)"%img.shape)
        except Exception as emsg:
            self.echo("...failed: %s"%emsg)
            return

        do_enhance = self.control.Input["enhanced"].checkState()
        if do_enhance:
            img = image.stretch_contrast(img)

        do_save = self.control.Input["saveit"].checkState()
        if do_save:
            try:
                impath = "CamView_%s.png"%iso_time
                im = Image.fromarray(img*255./img.max())
                im = im.convert("RGB")
                im.save(impath)
                self.echo("Saved image to: %s"%os.path.abspath(impath))
            except Exception as emsg:
                self.echo("Saving failed: %s"%emsg)

        imLeft = self.plotLeft.getImage()
        imRight = self.plotRight.getImage()
        oldData = imLeft.getData()
        if self.toggle_background:
            imRight.setData(oldData)
            imLeft.setData(img-self.bg_img)
        else:
            imRight.setData(oldData)
            imLeft.setData(img)
        self.plotLeft.resetZoom()


    def get_center_of_rotation(self):
        imLeft = self.plotLeft.get_roi_data().astype(float)
        imRight = self.plotRight.get_roi_data().astype(float)

        if not imLeft.size or not imRight.size:
            self.echo("Error: ROI outside image data.")
            return

        roi = self.plotLeft.roi
        dx, dy = np.array(roi)[:,0] if roi is not None else (0,0)

        #print(imLeft.shape, imRight.shape, roi)

        #sigma = float(self.control.Input["sigma"].text().toFloat()[0])
        sigma = 1.6 # default value
        t0 = time.time()
        try:
            sa = sift.LinearAlign(imLeft, devicetype=devicetype,init_sigma=sigma)
            res = sa.align(imRight, shift_only=False, return_all=True, 
                           double_check=False, relative=False, orsa=False)
        except Exception as emsg:
            self.echo("Error during alignment: %s"%emsg)
            return
        self.echo("Calculation time: %.2f ms"%((time.time() - t0)*1000))
        self.resultsCOR = dict(align=res)

        if res is None or res["matrix"] is None or res["offset"] is None:
            self.echo("Warning: No matching keypoints found.")
            return

        self.plot_matchpoints(res)
        numpoints = len(res["matching"])


        if numpoints<18:
            self.echo("Too few matching keypoints found (%i)."%numpoints)
            return

        self.echo("Matching keypoints found: %i"%numpoints)

        matrix, offset = res["matrix"][::-1,::-1], res["offset"][::-1]
        #offset[0] += dx
        #offset[1] += dy

        U, S, V = linalg.svd(matrix)
        R = U.dot(V) # Rotation part
        self.resultsCOR.update(dict(U=U, S=S, V=V, R=R))
        relrot = abs(R[0,0] - 1)
        if relrot < 1e-3:
            self.echo("Estimation of rotation failed. Too small? (%.3g)"%relrot)
            return
        angle = np.degrees(np.arctan2(R[0,1], R[1,1]))
        self.echo("Rotation of %.2f deg found."%angle)


        cor = linalg.solve(matrix - np.eye(2), -offset).squeeze()
        cor[0] += dx
        cor[1] += dy
        self.resultsCOR["cor"] = cor
        self.echo("Center of rotation estimated at (%.2f, %.2f) px."%tuple(cor))

        plotcfg = dict(symbol="o", legend="cor", 
                        color=(1.,.3,.3,1.), text="COR")
        self.plotLeft.addMarker(cor[0], cor[1], **plotcfg)
        self.plotRight.addMarker(cor[0], cor[1], **plotcfg)
        self.plotLive.addMarker(cor[0], cor[1], **plotcfg)


    def calc_sharpness(self):
        imLeft = self.plotLeft.get_roi_data().astype(float)
        sharpness = image.contrast(imLeft)
        self.echo("Computed sharpness of the left image ROI: %f"%sharpness)

    def autofocus(self):
        url = str(self.control.Input["url"].text())
        print(url)
        Navg = int(self.control.Input["Navg"].text())
        do_enhance = bool(self.control.Input["enhanced"].checkState())

        roi = self.plotLeft.roi
        if not roi is None:
            roi = tuple(roi[1]) + tuple(roi[0])
        if not hasattr(self, "_AutoFocus"):
            self._AutoFocus = microscope.AutoFocus(url)

        af = self._AutoFocus
        af.url = url
        af.roi = roi

        ddefaults = collections.OrderedDict()
        ddefaults["motor"] = af.motor.name
        ddefaults["lower_limit"] = af._ll
        ddefaults["upper_limit"] = af._ul
        ddefaults["Navg"] = Navg
        ddefaults["contrast"] = image._models
        ddefaults["enhance"] = do_enhance
        ddefaults["controller"] = ""
        ddefaults["config"] = ""

        dialog = AutoFocusDialog(self, defaults=ddefaults)
        dialog.exec_()
        results = dict.fromkeys(ddefaults)
        
        print(results)
        for field in ddefaults:
            result = dialog.Input[field]
            if isinstance(ddefaults[field], bool):
                result = bool(result.checkState())
            elif isinstance(ddefaults[field], list):
                result = str(result.currentText())
            else:

                result = type(ddefaults[field])(result.text())
            results[field] = result

        af.motor = results["motor"]
        af.limits = results["lower_limit"], results["upper_limit"]
        af.navg = results["Navg"]
        af.stretch = results["enhance"]
        af.contrast = results["contrast"]
        if not dialog.result():
            return
        self.echo("Starting autofocus...")

        try:
            d = BusyDialog(self, "focusing...")
            d.open()
            self.app.processEvents()
            fit = af.focus() ### Do the focusing
            d.close()
            self.app.processEvents()

            self.echo("Done. Status: %s"%fit.message)
            self.echo("New Position: %s=%f"%(af.motor.name,fit.x.item()))
        except Exception as emsg:
            self.echo("Error: %s"%emsg)

    def plot_matchpoints(self, res):
        roi = self.plotLeft.roi
        dx, dy = np.array(roi)[:,0] if roi is not None else (0,0)

        xk1 = res["matching"].x[:,0] + dx
        xk2 = res["matching"].x[:,1] + dx
        yk1 = res["matching"].y[:,0] + dy
        yk2 = res["matching"].y[:,1] + dy

        self.plotLeft.update_keypoints((xk1, yk1))
        self.plotRight.update_keypoints((xk2, yk2))


    def poi_to_cor(self):
        url = str(self.control.Input["url"].text())
        Navg = int(self.control.Input["Navg"].text())
        do_enhance = bool(self.control.Input["enhanced"].checkState())

        roi = self.plotLeft.roi
        poi = self.plotLeft.poi
        cor = self.resultsCOR.get("cor", None)

        if not roi is None:
            roi = tuple(roi[1]) + tuple(roi[0])
        if poi is None:
            self.echo("Use crosshair to select point of interest first.")
            return
        if cor is None:
            self.echo("Error: No center of rotation found.")
            return

        diff = cor - poi
        self.echo("Distance: (%.2f, %.2f) px"%tuple(diff))



        if not hasattr(self, "_Align"):
            self._Align = microscope.AlignImages(url)

        ddefaults = collections.OrderedDict()
        ddefaults["motor 1"] = "pix"
        ddefaults["motor 2"] = "piy"
        ddefaults["motor 1 step"] = "10"
        ddefaults["motor 2 step"] = "10"
        ddefaults["Navg"] = Navg
        ddefaults["enhance"] = do_enhance
        ddefaults["wait time"] = 1.
        ddefaults["method"] = image._imreg_meth

        dialog = Poi2CorDialog(self, defaults=ddefaults)
        dialog.exec_()

        results = dict.fromkeys(ddefaults)
        for field in ddefaults:
            result = dialog.Input[field]
            if isinstance(ddefaults[field], bool):
                result = bool(result.checkState())
            elif isinstance(ddefaults[field], list):
                result = str(result.currentText())
            else:
                result = type(ddefaults[field])(result.text())
            results[field] = result

        if not dialog.result():
            return

        align = self._Align
        align.url = url
        align.roi = roi

        align.motor1 = results["motor 1"]
        align.motor2 = results["motor 2"]
        align.steps = map(float, (results["motor 1 step"], results["motor 2 step"]))
        align.navg = int(results["Navg"])
        align.stretch = results["enhance"]
        align.waittime = results["wait time"]
        align.method = results["method"]

        try:
            d = BusyDialog(self, "Aligning...")
            d.open()
            self.app.processEvents()
            result = align.align(diff) ### Do the alignment
            d.close()
            self.app.processEvents()
        except Exception as emsg:
            d.close()
            self.echo("Error: %s"%emsg)
            
    def background(self,):
        iso_time = time.strftime("%Y-%m-%dT%H:%M:%S", time.localtime())
        url = self.control.Input["url"].text()#.toAscii()
        url = str(url)
        Navg = int(self.control.Input["Navg"].text())#.toAscii()
        if Navg < 1:
            self.echo("Error: need Navg > 0")
            return
        Ntext = "once" if Navg is 1 else "%i times"%Navg
        self.echo("Exposure - %s"%iso_time)
        self.echo("Fetching %s %s..."%(url, Ntext))
        try:
            img = image.url2array(url, Navg)
            self.echo("Image shape: (%i, %i)"%img.shape)
        except Exception as emsg:
            self.echo("...failed: %s"%emsg)
            return

        do_enhance = self.control.Input["enhanced"].checkState()
        if do_enhance:
            img = image.stretch_contrast(img)
            
        self.bg_img = img
        self.echo("background image taken")
        
    def toggle_background(self,):
        if self.toggle_background:
            self.toggle_background = False
            self.echo("background is OFF")
        else:
            self.toggle_background = True
            self.echo("background is ON")
        
            
    def echo(self, output):
        self.control.Input["output"].append(output)

    def close_application(self):
        choice = Q.QMessageBox.question(self, 'Quit',
                                "Do you really want to quit?",
                                Q.QMessageBox.Yes | Q.QMessageBox.No)
        if choice == Q.QMessageBox.Yes:
            sys.exit()
        else:
            pass


class CamViewDialog(QDialog): # Base Class
    Input = dict()
    def __init__(self, parent=None, defaults=dict()):
        super(CamViewDialog, self).__init__(parent)
        self.resize(300,200)
        self.defaults = defaults
        self.home()

    def home(self):
        pass

    def showEvent(self, event):
        geom = self.frameGeometry()
        geom.moveCenter(Q.QCursor.pos())
        self.setGeometry(geom)
        super(CamViewDialog, self).showEvent(event)

    def keyPressEvent(self, event):
        if event.key() == QtCore.Qt.Key_Enter:
            pass
        elif event.key() == QtCore.Qt.Key_Escape:
            self.hide()
            event.accept()
        else:
            super(CamViewDialog, self).keyPressEvent(event)

    def registerWidget(self, QtObject, name):
        self.Input[name] = QtObject
        if isinstance(QtObject, QLineEdit):
            #QtObject.setFixedWidth(length)
            pass
        else:
            QtObject.resize(QtObject.minimumSizeHint())
            #QtObject.resize(200)
        return QtObject


class AutoFocusDialog(CamViewDialog):
    def home(self):
        font = Q.QFont()
        font.setPointSize(9)
        self.setFont(font)

        _reg = self.registerWidget
        layout = QHBoxLayout(self)
        self.form = form = QFrame(self)
        form.setFrameShape(QFrame.StyledPanel)
        form.layout = QFormLayout(form)
        #form.layout = QGridLayout()

        defaults = self.defaults
        for i, field in enumerate(defaults):
            #hbox = QHBoxLayout()
            val = defaults.get(field, None)
            if isinstance(val, bool):
                qobj = QCheckBox(field, self)
                qobj.setCheckState(val)
            elif isinstance(val, list):
                qobj = QComboBox(self)
                qobj.addItems(val)
            else:
                qobj = QLineEdit(str(val))
                for chktyp in _valid:
                    if isinstance(val, chktyp):
                        qobj.setValidator(_valid[chktyp])
            qobj = _reg(qobj, field)
            qlabel = QLabel(field.capitalize())
            form.layout.addRow(qlabel, qobj)
        #form.layout.addRow(QPushButton("Start"), QPushButton("Cancel"))
        buttonBox = QDialogButtonBox(QDialogButtonBox.Cancel | QDialogButtonBox.Ok)
        buttonBox.accepted.connect(self.accept)
        buttonBox.rejected.connect(self.reject)
        form.layout.addRow(buttonBox)
        layout.addWidget(form)
        self.setLayout(layout)


class Poi2CorDialog(CamViewDialog):
    def home(self):
        font = Q.QFont()
        font.setPointSize(9)
        self.setFont(font)

        _reg = self.registerWidget
        layout = QHBoxLayout(self)
        self.form = form = QFrame(self)
        form.setFrameShape(QFrame.StyledPanel)
        form.layout = QFormLayout(form)

        defaults = self.defaults
        for i, field in enumerate(defaults):
            #hbox = QHBoxLayout()
            val = defaults.get(field, None)
            if isinstance(val, bool):
                qobj = QCheckBox(field, self)
                qobj.setCheckState(val)
            elif isinstance(val, list):
                qobj = QComboBox(self)
                qobj.addItems(val)
            else:
                qobj = QLineEdit(str(val))
                for chktyp in _valid:
                    if isinstance(val, chktyp):
                        qobj.setValidator(_valid[chktyp])
            qobj = _reg(qobj, field)
            qlabel = QLabel(field.capitalize())
            form.layout.addRow(qlabel, qobj)

        buttonBox = Q.QDialogButtonBox(Q.QDialogButtonBox.Cancel | Q.QDialogButtonBox.Ok)
        buttonBox.accepted.connect(self.accept)
        buttonBox.rejected.connect(self.reject)
        form.layout.addRow(buttonBox)
        layout.addWidget(form)
        self.setLayout(layout)




class BusyDialog(QDialog):
    def __init__(self, parent=None, message="Busy..."):
        super(BusyDialog, self).__init__(parent)
        self.message = message
        self.resize(200,100)
        self.home()

    def home(self):
        layout = QHBoxLayout(self)
        qlabel = QLabel(self.message)
        layout.addWidget(qlabel)

    def showEvent(self, event):
        geom = self.frameGeometry()
        geom.moveCenter(Q.QCursor.pos())
        self.setGeometry(geom)
        super(BusyDialog, self).showEvent(event)

    def keyPressEvent(self, event):
        if event.key() == QtCore.Qt.Key_Escape:
            return



def run():
    app = QApplication(sys.argv)
    #app.setStyle("CleanLooks")
    GUI = Window()
    GUI.app = app
    sys.exit(app.exec_())
    #app.exec_()

if __name__=="__main__":
    run()
