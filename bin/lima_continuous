#!/usr/bin/env python
#!/users/blissadm/bin blissrc
# -*- coding: utf-8 -*-
"""
Created on Wed Feb  7 21:02:27 2018

Liveviewer for reading out detector images on the fly

@author: opid01  - S.Leake 

TODO:
 add proper argparser
 ROI interaction tool?  or better a polygon ROI option outside of spec
 something missing in the setup of LIMA for Eiger - oxidis gets it working for some reason
"""

# /*##########################################################################
#
# Copyright (c) 2017 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/
"""This script shows how to subclass :class:`PlotWidget` to tune its tools.

It subclasses a :class:`silx.gui.plot.PlotWidget` and adds toolbars and
a colorbar by using pluggable widgets:

- QAction from :mod:`silx.gui.plot.actions`
- QToolButton from :mod:`silx.gui.plot.PlotToolButtons`
- QToolBar from :mod:`silx.gui.plot.PlotTools`
- :class:`ColorBarWidget` from :mod:`silx.gui.plot.ColorBar`.
"""

__authors__ = ["T. Vincent"]
__license__ = "MIT"
__date__ = "05/09/2017"

from PyTango import DeviceProxy

from silx.gui import qt
from silx.utils.deprecation import deprecated
from silx.gui.plot import PlotWidget, PlotWindow
from silx.gui.plot import actions
from silx.gui.plot import items
from silx.gui.plot.PlotTools import LimitsToolBar,PositionInfo
from silx.gui.plot.Profile import ProfileToolBar
from silx.gui.plot.ColorBar import ColorBarWidget
from silx.gui.plot.tools.roi import RegionOfInterestManager
from silx.gui.plot.tools.roi import RegionOfInterestTableWidget
from silx.gui.plot.items.roi import RectangleROI
from silx.image import shapes
import silx.third_party.EdfFile as EdfFile
import numpy as np
import os.path as osp
import time
from scipy import ndimage
import struct
import pdb



class Plot2D(PlotWindow):
    """PlotWindow with a toolbar specific for images.

    This widgets provides the plot API of :~:`.PlotWidget`.

    :param parent: The parent of this widget
    :param backend: The backend to use for the plot (default: matplotlib).
                    See :class:`.PlotWidget` for the list of supported backend.
    :type backend: str or :class:`BackendBase.BackendBase`
    """

    def __init__(self, devProxyBasePath="id01", devName="mpx4", devProxyName="mpx_1x4", devAttr="Image", devProxyLimaccd="limaccd", devProxyLiveviewer="liveviewer",pollContinuous=False, parent=None, backend=None):
        # List of information to display at the bottom of the plot
        posInfo = [
            ('X', lambda x, y: x),
            ('Y', lambda x, y: y),
            ('Data', self._getImageValue)]

        super(Plot2D, self).__init__(parent=parent, backend=backend,
                                     resetzoom=True, autoScale=False,
                                     logScale=False, grid=False,
                                     curveStyle=False, colormap=True,
                                     aspectRatio=True, yInverted=True,
                                     copy=True, save=True, print_=True,
                                     control=False, position=posInfo,
                                     roi=False, mask=True)
        if parent is None:
            self.setWindowTitle('Plot2D')
        self.getXAxis().setLabel('Columns')
        self.getYAxis().setLabel('Rows')

        self.profile = ProfileToolBar(plot=self)
        self.addToolBar(self.profile)

        self.getColorBarWidget().setVisible(True)
        
        """
        # Put colorbar action after colormap action
        actions = self.toolBar().actions()
        for index, action in enumerate(actions):
            if action is self.getColormapAction():
                break
        self.toolBar().insertAction(
            actions[index + 1],
            self.getColorBarWidget().getToggleViewAction())
        """
        self.devName=devName
        self.devProxyName=devProxyName
        self.tangoDev_liveviewer=DeviceProxy(osp.join(devProxyBasePath,devProxyLiveviewer,devProxyName))
        print(osp.join(devProxyBasePath,devProxyLiveviewer,devProxyName))
        self.tangoDev_limaccd=DeviceProxy(osp.join(devProxyBasePath,devProxyLimaccd,devProxyName))
        self.tangoDev_limaccd.video_active=True
        print(osp.join(devProxyBasePath,devProxyLimaccd,devProxyName))
        self._limaVideoType2Qub = {0 : np.uint8,
                                   1 : np.uint16,
                                   2 : np.int32,
                                   3 : np.int64}
        self.tangoDev_shutter=DeviceProxy("id01/v-bsh/1")        
        self.devAttr=devAttr

        self.timer=qt.QTimer()
        self.timer.timeout.connect(self._queryupdateImage)
        self.timer.start(100)
        self.exposure_time=0.05

        self.update=False
        self.toggle=True
        
        self.setYAxisInverted(True)
        self.setKeepDataAspectRatio(True)
        
        self.mask=EdfFile.EdfFile("/data/id01/archive/setup/spatcorr-files/%s/%s_hotmask.edf"%(devName,devName)).GetData(0)
        self.pollContinuous=pollContinuous
        
        self.tangoDev_limaccd.set_attribute("saving_mode","MANUAL")
        self.tangoDev_limaccd.set_attribute("acc_nb_frames",0) # maybe -1
        
    def _expose_maxipix(self):
        self.tangoDev_limaccd.command_inout("prepareAcq")
        self.tangoDev_limaccd.command_inout("startAcq")
        time.sleep(self.exposure_time)
        self.tangoDev_limaccd.command_inout("stopAcq");
    # Set the name of each created region of interest
    def _updateAddedRegionOfInterest(self):
        """Called for each added region of interest: set the name"""
        if self.roi.getLabel() == '':
            self.roi.setLabel('ROI %d' % len(self.roiManager.getRois()))
                    
    def old_getROIs(self,):
        self.ROInames=self.tangoDev_roiCounter.getNames()[1:]
        for name in self.ROInames:
            self.ROIs[name]=self.tangoDev_roiCounter.getRois([name])

    def old_plotrois(self,):
        for roi in self.ROIs:
            tmpROI = self.ROIs[roi]
            xlim=[tmpROI[1],tmpROI[1]+tmpROI[3]]
            ylim=[tmpROI[2],tmpROI[2]+tmpROI[4]]
            x = [xlim[i] for i in (0,1,1,0,0)]
            y = [ylim[i] for i in (0,0,1,1,0)]
            self.addCurve(x, y, resetzoom=False, legend=roi, color="r")

    def _getROIs(self,):
        self.ROInames=self.tangoDev_roiCounter.getNames()[1:]
        for name in self.ROInames:
            self.ROIs[name]=self.tangoDev_roiCounter.getRois([name])
            
    def _plotrois(self,):
        for ROI in self.ROIs:
            # Add a rectangular region of interest
            self.roi = RectangleROI()
            self.roi.setGeometry(origin=(self.ROIs[ROI][1], self.ROIs[ROI][2]), size=(self.ROIs[ROI][3], self.ROIs[ROI][4]))
            self.roi.setLabel(ROI)
            self.roiManager.addRoi(self.roi)

    def _getImageValue(self, x, y):
        """Get status bar value of top most image at position (x, y)

        :param float x: X position in plot coordinates
        :param float y: Y position in plot coordinates
        :return: The value at that point or '-'
        """
        value = '-'
        valueZ = -float('inf')
        mask = 0
        maskZ = -float('inf')

        for image in self.getAllImages():
            data = image.getData(copy=False)
            isMask = isinstance(image, items.MaskImageData)
            if isMask:
                zIndex = maskZ
            else:
                zIndex = valueZ
            if image.getZValue() >= zIndex:
                # This image is over the previous one
                ox, oy = image.getOrigin()
                sx, sy = image.getScale()
                row, col = (y - oy) / sy, (x - ox) / sx
                if row >= 0 and col >= 0:
                    # Test positive before cast otherwise issue with int(-0.5) = 0
                    row, col = int(row), int(col)
                    if (row < data.shape[0] and col < data.shape[1]):
                        v, z = data[row, col], image.getZValue()
                        if not isMask:
                            value = v
                            valueZ = z
                        else:
                            mask = v
                            maskZ = z
        if maskZ > valueZ and mask > 0:
            return value, "Masked"
        return value

    def getProfileToolbar(self):
        """Profile tools attached to this plot

        See :class:`silx.gui.plot.Profile.ProfileToolBar`
        """
        return self.profile


    @deprecated(replacement="getProfilePlot", since_version="0.5.0")
    def getProfileWindow(self):
        return self.getProfilePlot()

    def getProfilePlot(self):
        """Return plot window used to display profile curve.

        :return: :class:`Plot1D`
        """
        return self.profile.getProfilePlot()
        
    def _updateImage(self,):
        if not self.toggle:
            xmin,xmax=self.getGraphXLimits()
            ymin,ymax=self.getGraphYLimits() 
        
        try:    
			if False:
                        #if self.devProxyName=="mpx_1x4" or self.devProxyName=="frelondp3": #if False:
                		print("TRUE")
                		image=self.tangoDev_liveviewer.read_attribute(self.devAttr).value
			else:			
        			#print(self.tangoDev_limaccd.getImage().shape)
        			#pdb.set_trace()			
        			# alternative image from non depracated limaccd tango dserver
        			_,raw_data = self.tangoDev_limaccd.video_last_image
        			VIDEO_HEADER_FORMAT = '!IHHqiiHHHH'
        			minimumPacketSize = struct.calcsize(VIDEO_HEADER_FORMAT)
        			#print("bla",len(raw_data),minimumPacketSize)
        			if len(raw_data) >= minimumPacketSize:
                        		(magic, header_version, image_mode, image_frameNumber,image_width, image_height,endian, header_size, pad0, pad1) = struct.unpack(VIDEO_HEADER_FORMAT,raw_data[:minimumPacketSize])
                        		mode = self._limaVideoType2Qub.get(image_mode)
                        		#print("bla2",magic,header_version,mode,magic == 0x5644454f)
                        		#print(header_version == 1,mode is not None)
                        		if(magic == 0x5644454f and header_version == 1 and mode != None) :
                            			#self.__last_image_nb = image_frameNumber
                            			data = np.fromstring(raw_data[minimumPacketSize:],dtype=mode)
                            			data.shape = image_height,image_width
        			                    
        			
        			image=data
			image*=self.mask
			self.addImage(image)
			COM=ndimage.measurements.center_of_mass(image)
			print("COM: (%.3f,%.3f), MAX: %.3f, SUM: %.3e, MEAN: %.3f"%(COM[0],COM[1],image.max(),image.sum(),image.mean()))
			if (format(self.tangoDev_shutter.read_attribute("state").value)!="OPEN"):
				print('\x1b[1;37;41m %s \x1b[0m'%"***   WARNING: SAFETY SHUTTER STATE (%s)   ***"%format(self.tangoDev_shutter.read_attribute("state").value))
			
        except:
            print("failed to poll the image")
            pass 
            
        if not self.toggle:
            self.setGraphXLimits(xmin,xmax)
            self.setGraphYLimits(ymin,ymax)  
        else:
            self.toggle=False
        #print(".")
        
        """          
        for key in self.ROIs.keys():
            if self.ROIs[key].tolist()!=self.refROIs[key].tolist():
                self.toggleroiManagerChange=True
                
            
        if self.toggleroiManagerChange:
            self._getROIs()
            self.refROIs=self.ROIs.copy()
            self._plotrois()
        """
        #self.old_getROIs()
        #self.old_plotrois()

        
    def _queryupdateImage(self,):
        if self.pollContinuous:
            self._expose_maxipix()
            self._updateImage()
        else:
            #print(self.tangoDev_limaccd.read_attribute("acq_status").value)
            if self.tangoDev_limaccd.read_attribute("acq_status").value !='Ready':
                self.update = True
            if self.update and self.tangoDev_limaccd.read_attribute("acq_status").value =='Ready':
                #print("New image: ",time.asctime())
                self._updateImage()
                self.update = False

def main():
    global app
    app = qt.QApplication([])
    devProxyDict={"mpx4":"mpx_1x4","andor":"andor_zyla","eiger2M":"eiger2M","bvmicro":"bvmicro","frelon3":"frelondp3"}
    devProxyLiveviewerDict={"mpx4":"liveviewer","andor":"liveviewer","eiger2M":"liveviewer","bvmicro":"liveviewer","frelon3":"liveviewer"}
    devProxyLimaccdDict={"mpx4":"limaccd","andor":"limaccds","eiger2M":"limaccds","bvmicro":"limaccds","frelon3":"limaccd"}
    import sys
    #print sys.argv[1]
    # Create the ad hoc plot widget and change its default colormap
    try:
        devName=sys.argv[1]
        devProxyName=devProxyDict[sys.argv[1]]
        print("device:",devProxyName)
        devProxyLiveviewer=devProxyLiveviewerDict[sys.argv[1]]
        devProxyLimaccd=devProxyLimaccdDict[sys.argv[1]]
    except:
        devName="mpx4"
        print "device: mpx4 (default)"
        devProxyName=devProxyDict["mpx4"]
        devProxyLiveviewer=devProxyLiveviewerDict["mpx4"]
        devProxyLimaccd=devProxyLimaccdDict["mpx4"]
        
    try:
        if sys.argv[2] == "cont":
            pollContinuous=True
    except:
        pollContinuous=False

    devAttr="Image"
    plot = Plot2D("id01",devName,devProxyName,devAttr,devProxyLimaccd,devProxyLiveviewer,pollContinuous)
    colourmap={'name': 'viridis', 'normalization': 'log'}
    #self.getDefaultColormap().setName('viridis')

    plot.setDefaultColormap(colourmap)

    plot.show()
    # interact with spec in some way   

    import sys
    
    try:
        #raise #tests
        from id01lib.process import SpecClientWrapper
    except:
        print("trying local import.")
        sys.path.insert(0, os.path.join(os.path.abspath(os.pardir)))
        from id01lib.process import SpecClientWrapper

    app.exec_()


if __name__ == '__main__':
    main()
