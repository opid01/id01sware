#!/usr/bin/env python
# 20160420
# mpx4_pickbeampos.py
#
# now getting fancy put the beam somewhere on the detector
#
# 20160420  SL first iteration - needs testing
#           need pixperdeg in spec variables from det_calib
#
# 20160725  SL works for del/nu test in beam
#           same logic as det_calib_fancy
#
# 20161004  SL add pixel move and mpx/y move
#     
#       allow nu/mpxz del/mpxy combinations if for some reason it is useful
#
# TODO 
#       update for id01sware 
#       detector independent
#
################### le code #################
import numpy as np
import sys
from matplotlib.colors import LogNorm
from scipy.ndimage.measurements import center_of_mass
import pylab as pl


try:
    #raise #tests
    from id01lib.process import SpecClientWrapper
    from id01lib.process import PScanTracker
except:
    print("trying local import.")
    sys.path.insert(0, os.path.join(os.path.abspath(os.pardir)))
    from id01lib.process import SpecClientWrapper
    from id01lib.process import PScanTracker


def pixmotpos(pos, pixperdeg, ref_pix, ref_pix_ang):
  '''
  set the origin of the detector at 0,0 get the angles
  find all positions relative to this
  ref_pix_ang = [nu/del]
  
  '''
  origin = ref_pix_ang-ref_pix/pixperdeg
  pixpos = origin+np.array(pos)/pixperdeg
  return pixpos
  
cosd = lambda alpha : np.cos(np.radians(alpha))

def pix2motpos_huber(trgt_pix, pixperdeg, ref_pix, ref_ang ):
    '''
        set the origin of the detector at 0,0 get the angles
        find all positions relative to this
        ref_ang = (del, nu)
    '''
    trgt_pix = np.array(trgt_pix)
    ref_pix  = np.array(ref_pix)
    ref_ang  = np.array(ref_ang)
    #tmp_ppd = np.sin(np.radians(1))*det_distance/pix_size  # in mm
    print(pixperdeg,ref_ang)
    
    angles_ppd = np.array((pixperdeg[0], pixperdeg[1]*cosd(ref_ang[0])))
    #print(angles_ppd)
    #pixperdeg = np.array((tmp_ppd, tmp_ppd*cosd(ref_ang[0]))) # del on top of nu

    diffpix = trgt_pix - ref_pix
    trgt_ang = ref_ang + diffpix/angles_ppd
    return trgt_ang

def pixmotpos_huber(pos, pixperdeg, ref_pix, ref_pix_ang):
  '''
  ref_pix_ang = [del/nu]
  
  '''
  #print   pos, pixperdeg, ref_pix, ref_pix_ang
  origin = ref_pix_ang-ref_pix/pixperdeg
  #print origin
  pixpos = origin+np.array(pos)/pixperdeg
  return pixpos
  
def pixmotpos_det(pos, ref_pix, ref_pix_pos, pixel_size=np.array([0.055,-0.055])):
  '''
  ref_pix_pos = [mpxz/mpxy]
  
  '''
  origin = ref_pix_pos-ref_pix*pixel_size
  pixpos = origin+pos*pixel_size
  return pixpos

specname = "nano3:psic_nano"
specsession = SpecClientWrapper.SpecClientSession(specname=specname, verbose=False)
roi_list, device = specsession.find_roi_list()

if device == 'mpx4':
  pix_size = np.array([0.055,-0.055])
elif device == 'eiger2M':
  pix_size = np.array([0.075,-0.075])
  
# get last image from the detector
image = specsession.get_last_image(device)

_pixperdeg = float(specsession.get_sv("ID01META_STATIC_instrument")['EXPH']['pixperdeg'])

_mpx4align = specsession.get_sv("MPX4_ALIGN")


# quick pixperdeg calibration

_mot_del_start = specsession.get_motor("del")
_mot_nu_start = specsession.get_motor("nu")

_mot_mpxy_start = specsession.get_motor("mpxy")
_mot_mpxz_start = specsession.get_motor("mpxz")


# turn off the contour
specsession.send_sc('limashowroi 0')
specsession.send_sc('ct 0.5')

_det0 = specsession.get_last_image(device)
com0 = center_of_mass(_det0)

if _mpx4align["auto"]=="1":
    target_x = float(_mpx4align["x"])
    target_y = float(_mpx4align["y"])
else:
	fig = pl.figure()
	ax = fig.add_subplot(111)
	tracker = PScanTracker.PScanTracker(ax, specsession, norm=mynorm,exit_onclick=True)
	pl.show()

	target_x = tracker.POI[0]
	target_y = tracker.POI[1]

if _mpx4align["motor"]== "mpx4":
  pixpos0 = pixmotpos_det(np.array([target_y,target_x]),np.array([com0[0],com0[1]]),np.array([_mot_mpxz_start,_mot_mpxy_start]),pixel_size=pix_size)
  specsession.send_sc('umv mpxz %.3f mpxy %.3f'%(pixpos0[0],pixpos0[1]))
  print('umv mpxz %.3f mpxy %.3f'%(pixpos0[0],pixpos0[1]))
elif _mpx4align["motor"]== "diff": 
  pixpos0 = pix2motpos_huber(np.array([target_y,target_x]),[_pixperdeg,_pixperdeg],np.array([com0[0],com0[1]]),np.array([_mot_del_start,_mot_nu_start]))
  specsession.send_sc('umv del %.3f nu %.3f'%(pixpos0[0],pixpos0[1]))
  print('umv del %.3f nu %.3f'%(pixpos0[0],pixpos0[1]))
else:
  print("... use either mpx4/diff ...")
  
#speccmd.executeCommand('umv del %.3f nu %.3f'%(pixpos0[0],pixpos0[1]))
#print   'umv del %.3f nu %.3f'%(pixpos0[0],pixpos0[1])
#print   'umv mpxz %.3f mpxy %.3f'%(pixpos0[0],pixpos0[1])
specsession.send_sc('limashowroi 100')
specsession.send_sc('ct 0.5')

_det1 = specsession.get_last_image(device)
com1 = center_of_mass(_det1)

print("old COM : ",com0)
print("new COM : ",com1)
print(".. press any key (except spacebar) to continue..")


"""
def det_move_beam '{
  global MPX4_ALIGN
  local command
  print $#, $1
  if ($#==1){
    if ("$1"=="diff"){
      MPX4_ALIGN["motor"] = "diff"
      MPX4_ALIGN["auto"] = 0
      }
    else if ("$1"=="mpx4"){
      MPX4_ALIGN["motor"] = "mpx4"
      MPX4_ALIGN["auto"] = 0
      }
    else {
      print("USAGE: det_move_beam <mpx4/diff> <x> <y>")
      exit
      }
    MPX4_ALIGN["x"]=0
    MPX4_ALIGN["y"]=0    
  }
  else if ($#==3){
    if ("$1"=="diff"){
      MPX4_ALIGN["motor"] = "diff"
      MPX4_ALIGN["auto"] = 0
      }
    if ("$1"=="mpx4"){
      MPX4_ALIGN["motor"] = "mpx4"
      MPX4_ALIGN["auto"] = 0
      }
    MPX4_ALIGN["auto"] = 1
    MPX4_ALIGN["x"]=$2
    MPX4_ALIGN["y"]=$3
  }
  else{
    print("USAGE: det_move_beam <mpx4/diff> <x> <y>")
    exit
    }
  command = sprintf("u python /users/blissadm/local/python/mpx4_pickbeampos.py &")
  eval(command)
  #print(command)
}'
"""

