#!/usr/bin/env python
# encoding: utf-8
# 20151216
# sample_visual_lab_frame_ID01.py
# Borrowed from X.Wang Paul Scherrer Institut
#
#
######
#
# 20151216  SL convert to SpecClient from epicsPVs
#
# 20160111  SL add rotation matrices for a true 3D beam definition
#
######



####
# borrowed from transformations.py

# -*- coding: utf-8 -*-
# transformations.py

# Copyright (c) 2006-2015, Christoph Gohlke
# Copyright (c) 2006-2015, The Regents of the University of California
# Produced at the Laboratory for Fluorescence Dynamics
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright
#   notice, this list of conditions and the following disclaimer.
# * Redistributions in binary form must reproduce the above copyright
#   notice, this list of conditions and the following disclaimer in the
#   documentation and/or other materials provided with the distribution.
# * Neither the name of the copyright holders nor the names of any
#   contributors may be used to endorse or promote products derived
#   from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

####

def rotation_matrix(angle, direction, point=None):
    """Return matrix to rotate about axis defined by point and direction.

    >>> R = rotation_matrix(math.pi/2, [0, 0, 1], [1, 0, 0])
    >>> numpy.allclose(numpy.dot(R, [0, 0, 0, 1]), [1, -1, 0, 1])
    True
    >>> angle = (random.random() - 0.5) * (2*math.pi)
    >>> direc = numpy.random.random(3) - 0.5
    >>> point = numpy.random.random(3) - 0.5
    >>> R0 = rotation_matrix(angle, direc, point)
    >>> R1 = rotation_matrix(angle-2*math.pi, direc, point)
    >>> is_same_transform(R0, R1)
    True
    >>> R0 = rotation_matrix(angle, direc, point)
    >>> R1 = rotation_matrix(-angle, -direc, point)
    >>> is_same_transform(R0, R1)
    True
    >>> I = numpy.identity(4, numpy.float64)
    >>> numpy.allclose(I, rotation_matrix(math.pi*2, direc))
    True
    >>> numpy.allclose(2, numpy.trace(rotation_matrix(math.pi/2,
    ...                                               direc, point)))
    True

    """
    sina = math.sin(angle)
    cosa = math.cos(angle)
    direction = unit_vector(direction[:3])
    # rotation matrix around unit vector
    R = numpy.diag([cosa, cosa, cosa])
    R += numpy.outer(direction, direction) * (1.0 - cosa)
    direction *= sina
    R += numpy.array([[ 0.0,         -direction[2],  direction[1]],
                      [ direction[2], 0.0,          -direction[0]],
                      [-direction[1], direction[0],  0.0]])
    M = numpy.identity(4)
    M[:3, :3] = R
    if point is not None:
        # rotation not around origin
        point = numpy.array(point[:3], dtype=numpy.float64, copy=False)
        M[:3, 3] = point - numpy.dot(R, point)
    return M

def concatenate_matrices(*matrices):
    """Return concatenation of series of transformation matrices.

    >>> M = numpy.random.rand(16).reshape((4, 4)) - 0.5
    >>> numpy.allclose(M, concatenate_matrices(M))
    True
    >>> numpy.allclose(numpy.dot(M, M.T), concatenate_matrices(M, M.T))
    True

    """
    M = numpy.identity(4)
    for i in matrices:
        M = numpy.dot(M, i)
    return M

def phi(angle,point = None):
   return tfs.rotation_matrix(angle, zaxis, point)

def mu(angle,point = None):
   return tfs.rotation_matrix(angle, zaxis, point)

def eta(angle,point = None):
   return tfs.rotation_matrix(-angle, yaxis, point)

def ID01_nanorot(angmu,angeta,angphi,point = None):
       return tfs.concatenate_matrices(mu(angmu,point),eta(angeta,point),phi(angphi,point))
       
       
####

from PyQt4 import QtCore, QtGui
from SpecClient import SpecVariable
from SpecClient import SpecMotor
import math

class MainWindow(QtGui.QWidget):

    def __init__(self, scene = None, parent=None):
        super(MainWindow,self).__init__(parent)

        self.createUI()
        self.loadImage(imgname)
        self.createPVs()

        self.timer = QtCore.QTimer(self);
        self.timer.setObjectName("timerUpdate")
        self.timer.start(500)

        QtCore.QMetaObject.connectSlotsByName(self)

    def createUI(self):
        layout = QtGui.QVBoxLayout()
        self.setLayout(layout)

        #panel = self.createControlPanel()
        #layout.addLayout(panel)

        view  = self.createGraphicsView()
        layout.addWidget(view)

    def createControlPanel(self):
        hlayout = QtGui.QHBoxLayout()

        button = QtGui.QPushButton()
        button.setObjectName("buttonLoad")
        button.setText("Load ...")
        button.clicked.connect(self.loadImage)
        hlayout.addWidget(button)

        hlayout.addStretch()

        label = QtGui.QLabel("Dimension")
        hlayout.addWidget(label)

        spin = QtGui.QDoubleSpinBox()
        hlayout.addWidget(spin)

        spin = QtGui.QSpinBox()
        hlayout.addWidget(spin)

        label = QtGui.QLabel("Rotation")
        hlayout.addWidget(label)

        spin = QtGui.QDoubleSpinBox()
        hlayout.addWidget(spin)

        hlayout.addStretch()

        label = QtGui.QLabel("Center")
        hlayout.addWidget(label)

        spin = QtGui.QSpinBox()
        hlayout.addWidget(spin)

        spin = QtGui.QSpinBox()
        hlayout.addWidget(spin)

        return hlayout

    def createGraphicsView(self):
        self.scene = self.createGraphicsScene()
        view = QtGui.QGraphicsView(self.scene)
        view.setRenderHint(QtGui.QPainter.Antialiasing)
        view.setCacheMode(QtGui.QGraphicsView.CacheBackground)
        view.setDragMode(QtGui.QGraphicsView.ScrollHandDrag)
        view.setBackgroundBrush(QtCore.Qt.lightGray)
        return view

    def createGraphicsScene(self):
        scene = QtGui.QGraphicsScene()
        scene.setSceneRect(-300, -300, 600, 600)
        scene.setItemIndexMethod(QtGui.QGraphicsScene.NoIndex)

        self.sample = QtGui.QGraphicsPixmapItem()
        scene.addItem(self.sample)

        self.spot = QtGui.QGraphicsEllipseItem()
        self.spot.setPen(QtCore.Qt.yellow)
        scene.addItem(self.spot)

        crossh = QtGui.QGraphicsLineItem(-10, 0, 10, 0)
        scene.addItem(crossh)
        crossv = QtGui.QGraphicsLineItem(0, -10, 0, 10)
        scene.addItem(crossv)

	self.spi = QtGui.QGraphicsEllipseItem(-1.5, -1.5, 3, 3)
        self.spi.setBrush(QtCore.Qt.black)
        scene.addItem(self.spi)

	self.spi_text = QtGui.QGraphicsTextItem('Pivot Point = (0,0,0)')
	self.spi_text.setPos(0, -300)
        scene.addItem(self.spi_text)

        return scene

    def createPVs(self):
        specname = "nano2:psic_nano"
        self.pvs= {}
        self.pvs['thx'] = SpecMotor.SpecMotor('thx', specname)
        self.pvs['thy'] = SpecMotor.SpecMotor('thy', specname)
        self.pvs['thz'] = SpecMotor.SpecMotor('thz', specname)
        
        self.pvs['pix'] = SpecMotor.SpecMotor('pix', specname) # microns
        self.pvs['piy'] = SpecMotor.SpecMotor('piy', specname) # microns
        self.pvs['piz'] = SpecMotor.SpecMotor('piz', specname) # microns
        
        self.pvs['mu'] = SpecMotor.SpecMotor('mu', specname)
        self.pvs['phi'] = SpecMotor.SpecMotor('phi', specname)
        self.pvs['eta'] = SpecMotor.SpecMotor('eta', specname)
        
        self.pvs['SHEXA_PAR'] = SpecVariable.SpecVariable('SHEXA_PAR',specname)

    #@QtCore.pyqtSlot()
    def on_buttonLoad_clicked(self):
        fname = QtGui.QFileDialog.getOpenFileName(filter = "Images (*.png *.jpg)")
        if fname:
            self.loadImage(fname)

    def loadImage(self, fname):
        global center
        original = QtGui.QPixmap(imgname)
        pixmap = original.scaled(500, 500, QtCore.Qt.KeepAspectRatio)
        self._pixmap = pixmap
        if center:
            scale = 1.0 * pixmap.width() / original.width()
            center = (center[0]*scale, center[1]*scale)
        else:
            center = (pixmap.width()/2,pixmap.height()/2)

        self.scaling_factor = pixmap.width()/size[0]
        self.sample.setPixmap(pixmap)
        self.sample.setOffset(QtCore.QPointF(-center[0], -center[1]))

    #@QtCore.pyqtSlot()
    def on_timerUpdate_timeout(self):
        # re-position samplebased on motor positions and offset
        tmp_thx = self.pvs['thx'].getPosition() + self.pvs['pix'].getPosition()/1000. - offset['thx']
        tmp_thy = self.pvs['thy'].getPosition() + self.pvs['piy'].getPosition()/1000. - offset['thy']
        tmp_thz = self.pvs['thz'].getPosition() + self.pvs['piz'].getPosition()/1000. - offset['thz']
        tmp_phi = math.radians(self.pvs['phi'].getPosition())
        tmp_eta = math.radians(self.pvs['eta'].getPosition())
        tmp_mu = math.radians(self.pvs['mu'].getPosition())
        
        # rotation matrix
        rotmat = ID01_nanorot(tmp_mu, tmp_eta, tmp_phi)
        # rotation point
        rotpoint = [x,y,z]
        # current position
	    current_pos = np.dot(rotmat,np.matrix([[tmp_thx],[tmp_thy],[tmp_thz],[0]]))

        laby = tmp_thx*math.cos(tmp_phi)-tmp_thy*math.sin(tmp_phi)-offset_im[1] 
        labx = tmp_thy*math.cos(tmp_phi)+tmp_thx*math.sin(tmp_phi)-offset_im[0]

        if geometry=='h':
            incidence = self.pvs['mu'].getPosition()-self.pvs['phi'].getPosition()
            roz =  self.pvs['phi'].getPosition()+self.pvs['mu'].getPosition()
            beam_width  = beam[0]
            beam_height = beam[1]
        else:
            incidence = self.pvs['eta'].getPosition()
            roz =  self.pvs['phi'].getPosition()+self.pvs['mu'].getPosition()+offset_ov
            beam_width  = beam[1]
            beam_height = beam[0]
        incidence = max(incidence, 0.00001)

        tmp_piz = self.pvs['pix'].getPosition()
        tmp_piy = self.pvs['piy'].getPosition()
        tmp_piz = self.pvs['piz'].getPosition()

        if not self.sample.pixmap():
            return
        self.sample.setMatrix(QtGui.QMatrix())
        self.sample.setPos(labx*self.scaling_factor, laby*self.scaling_factor)
        self.sample.rotate(roz)

	# pivot point hexapod
	pp = map(float,self.pvs['SHEXA_PAR'].getValue()["17"]["misc_par_1"].split(','))
        self.spi.setPos(pp[0],pp[1])
	self.spi_text.setPlainText('Pivot Point = (%s, %s, %s)' % (pp[0],pp[1],pp[2]))

    # COR diffractometer
    # this is effectively the origin and should remain constant. the hexapod pivot point 
    # should move relative to this as you move the hexapod

	# beam spot
        visual_beam_width  = beam_width * self.scaling_factor
        visual_beam_height = beam_height * self.scaling_factor
        spot_length = visual_beam_height/math.sin(incidence/180.*math.pi)
        #print spot_length,self.scaling_factor
        self.spot.setRect(-spot_length/2., -visual_beam_width/2., spot_length, visual_beam_width)

if __name__ == '__main__':
    import os
    import sys
    import getopt

    # default arguments
    size = [1,1]
    beam = [0.002, 0.002]
    offset_im = [0.0,0.0]
    geometry = 'v'
    offset_ov = 0.0
    center = None
    # parse arguments
    options, remainder = getopt.gnu_getopt(sys.argv[1:], 'b:c:s:r:g:o:p:h', ['beam=', 'center=',
                                                             'size=',
                                                             'geometry=','offset_ov','offset_im'
                                                             'help'
                                                             ])
    for opt, arg in options:
        if opt in ('-c', '--center'):
            center = [int(x) for x in arg.split(',')]
        elif opt in ('-s', '--size'):
            size = [float(x) for x in arg.split(',')]
        elif opt in ('-b', '--beam'):
            beam = [float(x) for x in arg.split(',')]
        elif opt in ('-g','--geometry'):
            geometry = arg
	elif opt in ('-o','--ov_offset'):
	    offset_ov = float(arg)
	elif opt in ('-p','--im_offset'):
            offset_im = [float(x) for x in arg.split(',')]
        elif opt in ('-h', '--help'):
            print """
sample-visual.py - GUI to visualize sample orientation for X04SA surface diffractometer
================

Usage:
        sample-visual.py [<options>] sample_image

        Valid options are:
            -h                  display this help text
            -b|--beam w,h       specify beam size in mm, default is 0.002,0.002 mm
            -c|--center x,y     specify image center in pixel, defaul is image center
            -g|--geomery v|h    specify diffractometer geometry, default is v
	    -o|--ov_offset	offset of sample relative to ov zero position
            -p|--image_offset
            -s|--size   w,h     specify sample actual size in mm, default is 1,1 mm
            """
            sys.exit(0)

    if not remainder:
        print "Image file must be specified"
        sys.exit(1)
    else:
        imgname = remainder[0]
        if not os.path.exists(imgname):
            print "Image file does not exist"
            sys.exit(1)

    # create application and main window
    app = QtGui.QApplication(sys.argv)
    win = MainWindow()
    win.resize(700,700)
    win.show()

    # enter main event loop
    app.exec_()
