import collections
import numpy as np
from silx.third_party import EdfFile
from id01lib.process import vector
from id01lib import image
from id01lib.id01h5 import fetch_image_files
from id01lib.xrd import detectors


def get_vector(delta, nu):
    """ vector of the diffracted beam at (delta, nu) """
    return vector.Rz(-nu).dot(vector.Ry(-delta)).dot((1,0,0))


def get_det_calib(scan, device=None, rebin=None, upsample=1):
    """
        ToDo: - estimate cen_pix
              - com vs cc?
    """
    img_list = fetch_image_files(scan)
    if device is None:
        device = list(img_list)[0]

    detector = detectors.order[device]() # detector instance
    pixsize = detector.pixsize[0]
    img_list = img_list[device]

    delta = scan["instrument/positioners/del"].value
    nu = scan["instrument/positioners/nu"].value
    icenter = ((delta - delta.mean())**2 + (nu - nu.mean())**2).argmin()
    images = [EdfFile.EdfFile(path).GetData(0) for path in img_list]
    if rebin is not None:
        images = [image.rebin(img, rebin) for img in images]
        pixsize *= rebin
    images = np.array(images)


    shifts = image.get_shift(images, images[icenter], upsample)
    Intensity = shifts.pop("sum")
    Imed = np.median(Intensity)
    Isigma = np.sqrt(Imed)

    v_ref = get_vector(delta[icenter], nu[icenter])


    distances = collections.defaultdict(list)
    for i in range(len(delta)):
        #if abs(Intensity[i] - Imed) > (10*Isigma):
        #    continue
        if i==icenter:
            continue
        _del = delta[i]
        _nu = nu[i]

        vec = get_vector(_del, _nu)
        angle = vector.angle_between(vec, v_ref)
        for mode in shifts:
            shift = shifts[mode][i]
            distances[mode].append(np.linalg.norm(shift)/np.tan(angle) * pixsize)
    #print distances

    meandist = dict()
    for mode in distances:
        distance = np.array(distances[mode])
        distance = distance[np.isfinite(distance)]
        distance.sort()
        N = distance.size
        meandist[mode] = np.mean(distance[N//5:(4*N)//5])
    result = collections.namedtuple("CalibrationResult", 
                                    ("pixshifts",
                                     "delta",
                                     "nu",
                                     "distances",
                                     "average",
                                     "intensity"))

    return result(shifts, delta, nu, distances, meandist, Intensity)






