#!/uOBsr/bin/env python
# -*- coding: utf-8 -*-
#----------------------------------------------------------------------
# Description:
# Author: Carsten Richter <carsten.richter@esrf.fr>
# Created at: Wed Jun  7 18:28:51 CEST 2017
# Computer: rnice8-0207 
# System: Linux 3.16.0-4-amd64 on x86_64
#----------------------------------------------------------------------
"""
    Originally these are some functions that are used by CamView, but
    they can be useful for other routines and scripts!
"""

import collections
import platform
PV = platform.python_version()
if PV.startswith("2."):
    from urllib2 import urlopen
elif PV.startswith("3."):
    from urllib.request import urlopen


from io import BytesIO
from PIL import Image
import numpy as np
from scipy.ndimage.measurements import center_of_mass
from scipy.ndimage import gaussian_laplace


def com(*args, **kwargs):
    return np.array(center_of_mass(*args, **kwargs))

def url2array(url, navg=1):
    for i in range(navg):
        response = urlopen(url,timeout=4)
        im = Image.open(BytesIO(response.read()))
        if not i:
            img = np.array(im).sum(-1)
        else:
            img +=np.array(im).sum(-1)
    return img/navg



def percentile_interval(arr, low=1., high=99.):
    """
        Returns the borders of an interval having all values that are
        larger than `low`%% of values and smaller than `high`%%
        of values. This way it can be used to discard (100-high+low)%%
        of outliers.
    """
    pos = ((arr.size-1)*np.array((low,high), dtype=float)/100.).astype(int)
    vmin, vmax = np.sort(arr, axis=None)[pos]
    return vmin, vmax



def stretch_contrast(image, percentile_low=5., percentile_high=95.):
    """
        To stretch contast of an image discarding
        ((100-percentile_low) + percentile_low) percent of the data.
    """
    #assert image.ndim==2, 'Wrong input shape.'
    Imin, Imax = percentile_interval(image, percentile_low, percentile_high)
    image = np.clip(image, Imin, Imax)
    return image

_models = ["diff", "msd", "gradient"]

def contrast(array, model="diff"):
    """
        Calculates the sharpness of a greyscale image.
        Maybe it should not be named `contrast`?
    """
    array = array.astype(float)
    #array /= array.max()
    if model=="gradient":
        ddx, ddy = np.gradient(array)
        return np.mean(ddx**2 + ddy**2)
    if model=="diff":
        return np.mean( (np.diff(array,axis=0)**2)[:,1:] + \
                        (np.diff(array,axis=1)**2)[1:,:] )
    if model=="msd":
        mean = np.mean(array)
        return np.mean((array-mean)**2)

    if model not in _models:
        return _models



_imreg_meth = ["correlation", "sift"]

def get_shift(images, ref_img,
              method="correlation",
              upsample=100.,
              sigma=3.,
              devicetype="GPU"):
    """
        Convenience function to guess the shift of images with
        respect to a reference.

        methods:
            - phase correlation (skimage):
                uses arguments:
                    `upsample` for sub-pixel precision
                    `sigma` for gaussian-laplacian filtering

            - sift (silx)
                uses arguments:
                    `devicetype` (GPU/CPU)
                    `sigma` for gaussian-laplace filtering
    """
    if method not in _imreg_meth:
        raise ValueError("`method` needs to be one of: " +
                         ", ".join(_imreg_meth))
    if method == "correlation":
        from skimage.feature import register_translation
        def _get_shift(im0, im1):
            if sigma>1e-3:
                im0 = gaussian_laplace(im0, sigma)
                im1 = gaussian_laplace(im1, sigma)
            cc, _, _ = register_translation(im0, im1, upsample_factor=upsample)
            return cc
    elif method == "sift":
        from silx.image import sift
        sa = sift.LinearAlign(ref_img, devicetype=devicetype)
        def _get_shift(im0, im1):
            res = sa.align(im1, shift_only=True, return_all=True)
            if res is None or res["matrix"] is None or res["offset"] is None:
                raise ValueError("Warning: No matching SIFT-keypoints found.")
            cc = -res["offset"]
            return cc

    shifts = collections.defaultdict(list)
    com0 = com(ref_img)
    if np.ndim(images) == 2:
        images = [images]
    for i, frame in enumerate(images):
        shift = _get_shift(ref_img, frame)
        shifts["sum"].append(frame.sum())
        shifts["shift"].append(shift)
        shifts["com"].append(com(frame)-com0)
    return shifts




def rebin(image, factor):
    factor = np.ones(2, dtype=int)*factor
    assert image.ndim == 2
    assert not np.sometrue(np.mod(image.shape, factor)), \
        "size mismatch"

    shape = image.shape//factor
    sh = shape[0],factor[0],shape[1],factor[1]
    return image.reshape(sh).sum(3).sum(1)
