import numpy as np



edf_header_types = {
    'ByteOrder': bytes,
    'DataType': bytes,
    'Dim_1': int,
    'Dim_2': int,
    #'HeaderID': bytes, # do not use it according to PB
    'Image': int,
    'Offset_1': int,
    'Offset_2': int,
    'Size': int,
    'BSize_1': int,
    'BSize_2': int,
    'ExposureTime': float,
    'Title': bytes,
    'TitleBody': bytes,
    'acq_acc_max_expotime': float,
    'acq_acc_time_mode': int,
    'acq_autoexpo_mode': np.dtype('S4'),
    'acq_expo_time': float,
    'acq_latency_time': float,
    'acq_mode': bytes,
    'acq_frame_nb': int,
    'acq_trigger_mode': bytes,
    'image_bin': bytes,
    'image_flip': bytes,
    'image_roi': bytes,
    'image_rotation': bytes,
    'time': np.dtype('S24'), #np.string_, # fixed length str
    'time_of_day': float,
    'time_of_frame': float
    }




