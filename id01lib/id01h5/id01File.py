#!/usr/bin/env python
#----------------------------------------------------------------------
# Description:
# Author: Carsten Richter <carsten.richter@esrf.fr>
# Created at: Do 16. Mar 16:57:08 CET 2017
# Computer: lid01gpu1.
# System: Linux 3.16.0-4-amd64 on x86_64
#----------------------------------------------------------------------
#
# Todo:
#       - autoappend to an hdf5 if the sample exists
#       - parallelization
#         (does it make sense if harddisk I/O is the biggest part of the work?)
#       - read/write of 2d slices of .edf cubes faster than whole cube?
#       - define proper nx classes + id01-specific parsing of spec file header
#       - better compatability to xsocs .h5 format
#       - more comments / doc
#
#----------------------------------------------------------------------
"""
 A layer on top of h5py and silx.io for conversion of ID01 data
 to hd5f

 New proposed hdf5 file structure
 To be discussed...

 Adopted from silx SpecH5 combined with Edf Converter
  /
      1.1/
          title = "..."
          start_time = "..."
          instrument/
              specfile/
                  file_header = "..."
                  scan_header = "..."
              positioners/
                  motor_name = value
                  ...
              mca_0/
                  data = ...
                  calibration = ...
                  channels = ...
                  preset_time = ...
                  elapsed_time = ...
                  live_time = ...

              mca_1/
                  ...
+             detector_0/
+                 data = ...
+                 others/
+                     time = ...
+                     NumImages = ...
+                     ...
              ...
          measurement/
              colname0 = ...
              colname1 = ...
              ...
              mca_0/
                   data -> /1.1/instrument/mca_0/data
                   info -> /1.1/instrument/mca_0/
              ...
+             image_0/ --> link to detector_0
              ...
      2.1/
          ...
      3.0.kmap_00001/
          ...
      3.0.kmap_00002/
          ...
      3.0.kmap_00003/
          ...
          ...
      3.1/
          ...
"""


import os
import sys
import h5py
import time
import traceback
import itertools
import collections
import numpy as np
import dateutil.parser
import pdb
from PIL import Image


import silx.io
import silx.third_party.EdfFile as EdfFile
#import silx.io.fabioh5
#from silx.io.spectoh5 import write_spec_to_h5

from ..settings import edf_header_types
from ..xrd import detectors

from . import id01SpecH5 # a wrapper around silx.io.spech5



## Definition of the skeleton. This needs discussion and further work.
#### Now to be done in id01SpecH5!! ##
#DefaultInstrument = dict()
##not compatible with silx:
##DefaultInstrument["detectors"] = dict(_descr="name, flatfield, dimensions, pixel sizes, central pixel, timestamp, threshold")
#DefaultInstrument["diffractometer"] = dict(_descr="horizontal vertical")
#DefaultInstrument["positioners"] = dict(_descr="hexapod, xv, alp, oh, del, gamma, nu, ov, maybe the groups from groupdump")
#DefaultInstrument["beam_conditioner"] = dict(_descr="filters, transm, I_zero, scalers, slit sizes (move to optics?)")
#DefaultInstrument["optics"] = dict(_descr="all the set up params X1/X2/MI1/MI2")
#DefaultInstrument["source"] = dict(_descr="energy, U gap, lambda, current")
#DefaultInstrument["sample_environment"] = dict(_descr="gas, temperature, etc.")
#
#DefaultMeasurement = dict()


isfile = id01SpecH5.isfile

def _makedefaults(g, defaults, verbose=False):
    """
        Recursive creation of a skeleton of subgroups
        from the dictionary ``defaults''
    """
    for k in defaults:
        v = defaults[k]
        if isinstance(k, str) and k.startswith("_"):
            g.attrs[k[1:]] = v
        elif isinstance(v, dict):
            newgroup = g.create_group(k)
            _makedefaults(newgroup, v)
        else:
            if verbose:
                sys.stdout.write("setting %s/%s = %s%s"%(g.name, k, str(v), os.linesep))
            g[k] = v
    return


def _len(obj):
    try:
        return len(obj)
    except:
        return 0


def _spech5_deepcopy(scan, origin="/"):
    def helper(s, obj):
        if silx._version.MINOR < 6 and silx._version.MAJOR == 0:
            s = os.path.relpath(s, origin)
        if not s:
            return
        if isinstance(obj, silx.io.spech5.SpecH5Dataset):
            if s not in scan:
                scan.create_dataset(name=s,
                                    shape=obj.shape,
                                    data=obj.value,
                                    dtype=obj.dtype,
                                    chunks=obj.chunks
                                    )
        else:
            if s not in scan:
                scan.create_group(s)
        scan[s].attrs.update(obj.attrs)
    return helper



def safeslice(start, stop=None, step=None):
    if stop == 0:
        stop = None
    return slice(start, stop, step)


class Scan(h5py.Group):
    debug = False
    def __init__(self, bind, skeleton=False):
        super(Scan, self).__init__(bind)

    def addEdfFile(self, edf, detector_num=None,
                              detector_name=None,
                              subdir="instrument",
                              compr_lvl=6,
                              chunksize=500,
                              dtype=None):
        """
            Adds an 2D or 3D EDF image to the stack of
            images or creates a new stack.
            If the detector number is not specified,
            the images will be appended to the first
            detector matching the shape.

            Accepts .edf in hdf5 format as it is provided
            after opening with silx.
        """
        if isinstance(edf, EdfFile.EdfFile):
            pass
        elif isfile(edf):
            ### open the image file
            if isinstance(edf, bytes):
                edf = edf.decode()
            edf = EdfFile.EdfFile(edf, fastedf=True)

        if not edf.NumImages:
            return

        newshape = (edf.NumImages, edf.Images[0].Dim2, edf.Images[0].Dim1)

        if dtype is None:
            dtype = edf.GetDefaultNumpyType(edf.GetStaticHeader(0)["DataType"])

        if self.debug:
            sys.stdout.write("Loaded image. Shape %ix%ix%i"%newshape + os.linesep)
        instrument = self[subdir]

        ### find which detector these frames belong to if not given
        if detector_num is None:
            i = 0
            while True:
                key = "detector_%i"%i
                if key not in instrument:
                    break
                elif "data" not in instrument[key]:
                    break
                if instrument[key]["data"].shape[1:] == newshape[1:]:
                    break
            detector_num = i
        else:
            key = "detector_%i"%detector_num

        meta_keys = list(itertools.chain(edf.GetHeader(0),
                                         edf.GetStaticHeader(0)))

        ### prepare the target h5py datasets
        if key not in instrument or "data" not in instrument[key]:
            # create new datasets
            """
            if detector_name is not None:
                detector = instrument.require_group("detectors/%s"%detector_name)
                detector.create_dataset("name", data=detector_name)
                #detector = instrument["detectors/%s"%detector_name]
                instrument[key] = detector
                detector_instance = detectors.order[detector_name]()
                disk_chunks = (1,) + detector_instance.chunks
            else:
                detector = instrument.require_group(key)
                disk_chunks = True # auto chunking? or better?: (1,) + newshape[1:]
            """

            detector = instrument.require_group(key)
            disk_chunks = True # auto chunking? or better?: (1,) + newshape[1:]
            detector.create_dataset("data",
                                  newshape,
                                  maxshape=(None,newshape[1],newshape[2]),
                                  compression="gzip",
                                  compression_opts=compr_lvl,
                                  chunks=disk_chunks,
                                  dtype=dtype) # Todo: think about chunks - only implement for kmaps

            # write metadata
            others = detector.require_group("others")
            for name in meta_keys:
                if name in edf_header_types:
                    mdtype = edf_header_types[name]
                    if mdtype is bytes:
                        mdtype = h5py.special_dtype(vlen=bytes)
                else:
                    continue
                others.create_dataset(name,
                                      (newshape[0],),
                                      maxshape=(None,),
                                      dtype=mdtype)

            # make a hard link: << change to softlink
            if subdir == 'instrument': # spech5 default
                #self["measurement/image_%i/data"%detector_num] = self["%s/%s"%(subdir,key)] # hard link
                self["measurement/image_%i/data"%detector_num] = h5py.SoftLink(self.name+"/instrument/detector_%s/data"%detector_num) # soft link

        else:
            # do the resizing
            detector = self[subdir][key]
            newlen = detector["data"].shape[0] + newshape[0]
            detector["data"].resize((newlen, newshape[1], newshape[2]))
            if self.debug:
                sys.stdout.write("New dataset size: "
                                 "%ix%ix%i"%detector["data"].shape
                                 + os.linesep)
            # process metadata
            others = detector["others"]
            for name in meta_keys:
                if name not in others:
                    continue
                newlen = others[name].shape[0] + newshape[0]
                others[name].resize((newlen,)) # strictly 1d


        ### do the actual writing of data -- chunkwise
        data = detector["data"]
        #pdb.set_trace()
        chunk = np.empty(shape=(chunksize, newshape[1], newshape[2]),
                         dtype=dtype)
        #TODO
        j = 0
        meta = collections.defaultdict(list)
        for i in range(0,edf.NumImages):
            for (k,v) in itertools.chain(edf.GetHeader(i).items(),
                                     edf.GetStaticHeader(i).items()):
                meta[k].append(v)
            chunk[i%chunksize] = edf.GetData(i)

            if not (i+1)%chunksize or (i+1)==edf.NumImages:
                #print newshape, (-newshape[0]+j), (-newshape[0]+i+1)
                target = safeslice(-newshape[0]+j, -newshape[0]+i+1)
                source = safeslice(0,i%chunksize+1)

                data[target] = chunk[source]

                for name in meta_keys:
                    if name not in others:
                        continue
                    mdtype = edf_header_types[name]
                    value = np.asarray(meta[name], dtype=mdtype)
                    others[name][target] = value[source]
                meta.clear()
                j=i+1


        self.file.flush()

        return detector.name # may be useful

    def addTifFile(self, tifpath, detector_num=None,
                              detector_name=None,
                              subdir="instrument",
                              compr_lvl=6,
                              chunksize=500,
                              dtype=None):
        """
            Adds an 2D or 3D TIF image to the stack of
            images or creates a new stack.
            If the detector number is not specified,
            the images will be appended to the first
            detector matching the shape.

            Accepts .edf in hdf5 format as it is provided
            after opening with silx.
        """

 
        tif = np.array(Image.open(tifpath))
        #print(tif.max())
        tif = tif[np.newaxis,:,:]
        newshape = tif.shape

        if dtype is None:
            dtype = np.int32

        instrument = self[subdir]

        ### find which detector these frames belong to if not given
        if detector_num is None:
            i = 0
            while True:
                key = "detector_%i"%i
                if key not in instrument:
                    break
                elif "data" not in instrument[key]:
                    break
                if instrument[key]["data"].shape[1:] == newshape[1:]:
                    break
            detector_num = i
        else:
            key = "detector_%i"%detector_num

        ### prepare the target h5py datasets
        if key not in instrument or "data" not in instrument[key]:
            # create new datasets
            """
            if detector_name is not None:
                detector = instrument.require_group("detectors/%s"%detector_name)
                detector.create_dataset("name", data=detector_name)
                #detector = instrument["detectors/%s"%detector_name]
                instrument[key] = detector
                detector_instance = detectors.order[detector_name]()
                disk_chunks = (1,) + detector_instance.chunks
            else:
                detector = instrument.require_group(key)
                disk_chunks = True # auto chunking? or better?: (1,) + newshape[1:]
            """

            detector = instrument.require_group(key)
            disk_chunks = True # auto chunking? or better?: (1,) + newshape[1:]
            #pdb.set_trace()
            
            detector.create_dataset("data",
                                  newshape,
                                  maxshape=(None,newshape[1],newshape[2]),
                                  compression="gzip",
                                  compression_opts=compr_lvl,
                                  chunks=disk_chunks,
                                  dtype=dtype) # Todo: think about chunks - only implement for kmaps

            # write metadata
            others = detector.require_group("others")

            # make a hard link: << change to softlink
            if subdir == 'instrument': # spech5 default
                #self["measurement/image_%i/data"%detector_num] = self["%s/%s"%(subdir,key)] # hard link
                self["measurement/image_%i/data"%detector_num] = h5py.SoftLink(self.name+"/instrument/detector_%s/data"%detector_num) # soft link

        else:
            # do the resizing
            detector = self[subdir][key]
            newlen = detector["data"].shape[0] + newshape[0]
            detector["data"].resize((newlen, newshape[1], newshape[2]))
            if self.debug:
                sys.stdout.write("New dataset size: "
                                 "%ix%ix%i"%detector["data"].shape
                                 + os.linesep)
            # process metadata
            others = detector["others"]

        detector["data"][-1,:,:] = tif
        #print(detector["data"][-1,:,:].max(),tif.max())


        self.file.flush()

        return detector.name # may be useful

    def make_roi(self, xmin, xmax, ymin, ymax, image='image_0', store=False,
                       roinum=None):
        """
            Saves to scan["measurement/image_X_roiY"] if store==True
        """
        if isinstance(image, int):
            image = "image_%i"%image
        measurement = self.get("measurement")
        if measurement is None:
            raise ValueError("No measurement found.")
        data = self["measurement"].get(image, None)
        if data is None:
            raise ValueError("Image `%s` not found in scan."%image)
        xmin, xmax = map(int, sorted((xmin, xmax)))
        ymin, ymax = map(int, sorted((ymin, ymax)))
        roi = data[:,ymin:ymax, xmin:xmax].sum((1,2)) # y is the first image dimension
        if store:
            if roinum is not None:
                if not isinstance(roinum, int):
                    roiname = roinum
                else:
                    roiname = "%s_roi%i"%(image, roinum)
            else:
                i = 0
                while True:
                    roiname = "%s_roi%i"%(image, i)
                    if not roiname in self["measurement"]:
                        break
                    i+=1
            new = self["measurement"].create_dataset(roiname, data=roi)
            sys.stdout.write("Created dataset %s"%new.name + os.linesep)
        return roi

    def fetch_edf_spec(self, pathonly=False, verbose=True, imgroot=None, explicit_beamline=None,
                             **edf_kw):

        img_paths = id01SpecH5.fetch_image_files(self, imgroot=imgroot, explicit_beamline=explicit_beamline)
        if not img_paths:
             return []

        detnames=img_paths.keys()
        for detnum,key in enumerate(detnames):
            #print img_paths
            detname=key
            #pdb.set_trace()
            img_paths_tmp = list(img_paths[key])# what if we have several detectors?
            #detnumber = list(detectors.order).index(detname)
            #if not "detector_num" in edf_kw:
            #    edf_kw["detector_num"] = detnumber
            edf_kw["detector_name"] = detname
            edf_kw["detector_num"] = detnum
            img_paths_tmp = np.array(img_paths_tmp, dtype=np.string_)
            #dt = h5py.special_dtype(vlen=bytes)
            self["measurement"].create_dataset("image_files_%s"%detnum,
                                               shape=img_paths_tmp.shape,
                                               dtype=img_paths_tmp.dtype,
                                               data=img_paths_tmp)
            #pdb.set_trace()                                   
            self['data_%s'%detnum]=h5py.SoftLink(self.name+"/instrument/detector_%s/data"%detnum)
            _i=0
            img_paths_tmp.sort() # very lazy but works
            for impath in img_paths_tmp:
                if isinstance(impath, bytes):
                    impath = impath.decode()
                #impath = impath
                if impath.endswith("tif"):
                    #print("###TIFS####")
                    try: # load tif file
                        if verbose:
                            sys.stdout.write("Loading image %i of %i: %s "%(_i+1, len(img_paths_tmp),impath))
                            sys.stdout.write("\r") # carriage return only. does it work on Win?
                            sys.stdout.flush()
                        #print(impath)
                        self.addTifFile(impath, **edf_kw)
                    except Exception as emsg:
                        sys.stdout.write("Could not load file: %s"%emsg)
                        sys.stdout.write(os.linesep)
                        #if self.debug:
                        raise
                        
                elif not isfile(impath) and verbose:
                    
                    sys.stdout.write("Warning: File not found: %s. Skipping..."%impath + os.linesep)
                    continue
                elif pathonly:
                    if not verbose:
                        break
                    sys.stdout.write("  Found path %s"%impath + os.linesep)
                else:
                    try: # load edf file
                        if verbose:
                            sys.stdout.write("Loading image %i of %i: %s "%(_i+1, len(img_paths_tmp),impath))
                            sys.stdout.write("\r") # carriage return only. does it work on Win?
                            sys.stdout.flush()
                        #print(impath)
                        self.addEdfFile(impath, **edf_kw)
                    except Exception as emsg:
                        sys.stdout.write("Could not load file: %s"%emsg)
                        sys.stdout.write(os.linesep)
                        #if self.debug:
                        raise
                _i+=1
            if verbose:
                sys.stdout.write(os.linesep)
        return img_paths





class Sample(h5py.Group):
    scans = []
    timefmt = "%Y-%m-%dT%H:%M:%S"
    def __init__(self, bind, description=None):
        super(Sample, self).__init__(bind)
        if not description is None:
            self.attrs["description"] = description


    def lastScan(self): # deprecated
        return (sorted(self.scans)[-1]) if self.scans else 0

    def addScan(self, number=None, *datafiles):
        """
            Trying to maintain Silx compatability.

            number : int, str
                number of the scan

            *datafiles : different kinds of data files to be processed
                         by silx
        """
        if number is None:
            number = self.lastScan() + 1
        if isinstance(number, int):
            name = '%i.1'%number
        else:
            name = number
            try:
                number = int(name.split(".")[0])
            except ValueError:
                number = -1
        #print self.keys()
        if name not in self:
            with h5py._hl.base.phil:
                # new scan
                name, lcpl = self._e(name, lcpl=True)
                gid = h5py.h5g.create(self.id, name, lcpl=lcpl)
                scan = Scan(gid)
                scan.attrs["_is_a_scan"] = True
                self.scans.append(number)
        else:
            # select scan
            scan = Scan(self[name].id)

        for f in datafiles:
            if isinstance(f, bytes):
                f = f.decode()
            if isinstance(f, EdfFile.EdfFile):
                scan.addEdfFile(f) # fast !

            #elif hasattr(f, "_File__fabio_image") and \
            #     isinstance(f._File__fabio_image, EdfImage): #already an h5 file?
            #    scan.addEdfH5Image(f) # slow!

            elif isfile(f):
                if f.lower().endswith(".edf") or f.lower().endswith(".edf.gz"):
                    scan.addEdfFile(f)

        return scan

    addScanData = addScan # the same method for two purposes



    def addSpecScan(self, specscan, number=None, overwrite=False,
                          fetch_edf=True, imgroot=None, verbose=True, explicit_beamline=None, **edf_kw):
        if not isinstance(specscan, silx.io.spech5.SpecH5Group):
            raise ValueError("Need `silx.io.spech5.SpecH5Group` as spec scan input.")
        sf = specscan.file

        ### check if still writing scan:
        t1 = sf.attrs['file_time'].item()
        t2 = specscan['start_time'].value.item()
        d1 = dateutil.parser.parse(t1)
        d2 = dateutil.parser.parse(t2)

        if "Epoch" in specscan["measurement"]:
            tpast = specscan["measurement/Epoch"].value
        elif "timer" in specscan["measurement"]:
            tpast = specscan["measurement/timer"].value
        else:
            tpast = [0,0]
        if len(tpast)<2:
            tpast = [0,0]

        tdiff = abs((d1 - d2).total_seconds()) \
              - (tpast[-1] - tpast[0]) \
              - (tpast[-1] - tpast[-2])
        if verbose == 2:
            print("Scan written %fs ago"%tdiff)

        if tdiff < 10 and verbose:
            sys.stdout.write("Warning: Omitting scan %5s (too recent)%s"
                        %(specscan.name, os.linesep))
            return


        filename = sf.filename
        #fast = filename.split("_")[-2] == "fast"
        fast = "_fast_" in os.path.basename(filename)
        root = specscan.name.split("/")[1]
        #specnumber = int(root.split(".")[0]) #+ (1 if fast else 0)
        specnumber = [int(n) for n in root.split(".")] #+ (1 if fast else 0)

        if number is None or fast:
            if fast:
                number = int(os.path.splitext(filename)[0][-5:])
            else:
                number = specnumber

        if isinstance(number, int):
            if fast:
                name = '%i.0.kmap_%05i'%(number, specnumber[0])
                if len(specnumber)==2:
                    name = name + ".%i"%specnumber[1]
            else:
                name = '%i.1'%number
        elif _len(number)==2:
            name = '%i.%i'%tuple(number)
        else:
            name = number

        #print('specnum:{0}, number:{1}'.format(specnumber, number))
        scan = self.addScan(name)


        if "title" in scan:
            if overwrite:
                self.pop(name)
                scan = self.addScan(name)
            else:
                if verbose:
                    sys.stdout.write("Warning: Scan %5s already exists in %s. "
                                    "Omitting. Consider the `overwrite` keyword%s"
                            %(name, self.name, os.linesep))
                return
        scan.attrs["_fast"] = fast
        scan.attrs["_specfile"] = filename

        if verbose:
            sys.stdout.write("Importing spec scan %s from %s to %s...%s"
                             %(root, filename, scan.name, os.linesep))


        try:
            #specscan.copy(specname, self, name=name) #copying does not work with spech5
            #TODO: replace by spectoh5.SpecToHdf5Writer:
            specscan.visititems(_spech5_deepcopy(scan, specscan.name))

            if fetch_edf and scan["measurement"]: # not aborted after 0 points
                scan.fetch_edf_spec(verbose=verbose, imgroot=imgroot, explicit_beamline=explicit_beamline,  **edf_kw)
            return scan
        except:
            print("An Error ocurred... cleaning up...")
            del scan
            del self[name]
            raise


    def importSpecFile(self, specfile, numbers=(), exclude=[], explicit_beamline=None, **addSpec_kw):
        """
            Convenience method that imports whole or parts of specfiles into the
            current `Sample` group.

            Inputs:
                specfile : str or id01lib.id01h5.id01SpecH5
                    The .spec file as path or id01SpecH5 instance

                numbers : sequence of (str or int) OR dict-like
                    The subset of scans to import (all if None).
                    If the `numbers` input is a dict, it represents
                    a mapping of scan numbers/names to new, arbitrary
                    names.

                exclude : sequence
                    a subset of scans to omit.

            For additional key word arguments see `addSpecScan`.

        """
        if isfile(specfile):
            s5f = id01SpecH5.id01SpecH5(specfile)
        elif isinstance(specfile, id01SpecH5.id01SpecH5):
            s5f = specfile
        else:
            if isinstance(specfile, str):
                raise TypeError("File not found: %s"%specfile)
            else:
                raise TypeError("Input type not supported: %s"%str(type(specfile)))

        if not numbers:
            numbers = list(s5f.keys())
        if isinstance(numbers, collections.Sequence):
            numbers = dict(zip(numbers, numbers))

        for i, number in enumerate(exclude):
            if isinstance(number, int):
                exclude[i] = "%i.1"%(number)

        for i, number in enumerate(numbers):
            if number in exclude:
                continue

            if isinstance(number, int):
                scanno = "%i.1"%(number)
            else:
                scanno = number

            try:
                scan = s5f[scanno]
            except Exception as emsg:
                sys.stdout.write("Warning: Could not load scan %s - %s:%s"
                                 %(specfile,str(scanno), os.linesep))
                sys.stdout.write("    %s. Skipping..."%emsg + os.linesep)
                continue
            self.addSpecScan(scan,
                             numbers[number],
                             explicit_beamline=explicit_beamline,
                             **addSpec_kw)


    def import_single_frames(self, image_dir, prefix="",
                                              compr_lvl=6,
                                              dest="_images_before"):
        """
            This is a way to store the frames recorded during `ct` into the
            hdf5 file. In the future there should be a reference to these
            frames in the spec file.

            This appends all images collected before a scan to a scan in the path 'dest'

            NB: you need to merge all scans first otherwise it will include images
             from scans not included in the file
        """
        # first get all existing scans with startdate and image files
        times = collections.OrderedDict()
        imgfiles = []
        firstimg = dict()
        for name, scan in self.items():
            starttime = scan.get("start_time", False)
            if not starttime:
                continue
            starttime = time.mktime(time.strptime(starttime.value, self.timefmt))
            times[name] = starttime
            scanimages = scan.get('measurement/image_files', [])
            if not scanimages:
                continue
            imgfiles.extend(map(os.path.basename, scanimages))

        _alltimes = np.array(times.values())
        _allscans = list(times)

        # now process all images in image_dir that are not in imgfiles
        # and therefore not part of any scan
        ii = 0
        paths = []
        for fname in set(os.listdir(image_dir)).difference(imgfiles):
            if not fname.startswith(prefix):
                continue
            if fname.lower().endswith(".edf.gz") or \
               fname.lower().endswith(".edf"):
                path = os.path.join(image_dir, fname)
                edf = EdfFile.EdfFile(path, fastedf=True)
                try:
                    edfheader = edf.GetHeader(edf.NumImages-1)
                except:
                    sys.stdout.write("Warning: corrupted file %s"%path + os.linesep)
                    continue
                if not "time_of_day" in edfheader:
                    nextname = "others"
                else:
                    time_last = float(edfheader['time_of_day'])
                    nexttime = _alltimes[_alltimes>time_last]
                    if not len(nexttime):
                        continue
                    nexttime = nexttime.min()
                    nextname = _allscans[np.where(_alltimes==nexttime)[0].item()]

                if nextname not in self:
                    scan = self.addScan(nextname)
                else:
                    scan = self[nextname]

                if dest not in scan:
                    destg = scan.create_group(dest)
                destg = scan[dest]

                skipit = False
                for grp in destg.values():
                    if path in grp.get("image_files", []):
                        skipit = True
                if skipit:
                    continue

                ii += 1
                sys.stdout.write("Adding single frame #%i (%s) to %s%s"
                      %(ii, fname, nextname, os.linesep))

                detname = scan.addEdfFile(edf, subdir=dest,
                                               compr_lvl=compr_lvl)

                detector = destg[detname]
                if "image_files" not in detector:
                    mdtype = h5py.special_dtype(vlen=bytes)
                    detector.create_dataset("image_files",
                                           (1,),
                                           maxshape=(None,),
                                           dtype=mdtype)
                else:
                    oldlen = detector['image_files'].shape[0]
                    detector['image_files'].resize((oldlen + 1,)) # strictly 1d
                detector['image_files'][-1] = path

            else:
                continue # todo: implement other typesprint

        return paths

    def __getitem__(self, name):
        o = super(Sample, self).__getitem__(name)
        #pdb.set_trace()
        if o.attrs.get("_is_a_scan", False):
            return Scan(o.id)
        else:
            return o



class ID01File(h5py.File):
    """
        Typical ID01 hdf5 file container
    """
    _samples = []
    def addSample(self, name, description=None,verbose=True):
        if name in self:
            if verbose:
                sys.stdout.write("Warning: returning already existing sample: %s%s"
                                %(name, os.linesep))
            return self[name]
        with h5py._hl.base.phil:
            name, lcpl = self._e(name, lcpl=True)
            gid = h5py.h5g.create(self.id, name, lcpl=lcpl)
            s = Sample(gid, description)
        s.attrs["_is_a_sample"] = True
        #self._samples.append(s.id)
        return s

    def __getitem__(self, name):
        """
            Messing around with h5py class definitions to
            change the dress of the group instances
        """
        o = super(ID01File, self).__getitem__(name)
        if o.attrs.get("_is_a_sample", False):
            return Sample(o.id)
        elif o.attrs.get("_is_a_scan", False):
            return Scan(o.id)
        else:
            return o





def print_hdf5_file_structure(file_name) :
    """Prints the HDF5 file structure"""
    file = h5py.File(file_name, 'r') # open read-only
    item = file #["/Configure:0000/Run:0000"]
    print_hdf5_item_structure(item)
    file.close()

def print_hdf5_item_structure(g, offset='    ') :
    """Prints the input file/group/dataset (g) name and begin iterations on its content"""

    if   isinstance(g,h5py.File) :
        print(g.file, '(File)', g.name)

    elif isinstance(g,h5py.Dataset) :
        print('(Dataset)', g.name, '    len =', g.shape) #, g.dtype

    elif isinstance(g,h5py.Group) :
        print('(Group)', g.name)

    else :
        print('WARNING: UNKNOWN ITEM IN HDF5 FILE', g.name)
        sys.exit ( "EXECUTION IS TERMINATED" )

    if isinstance(g, h5py.File) or isinstance(g, h5py.Group) :
        for key,val in dict(g).iteritems() :
            subg = val
            print(offset, key,) #,"   ", subg.name #, val, subg.len(), type(subg),
            print_hdf5_item_structure(subg, offset + '    ')
