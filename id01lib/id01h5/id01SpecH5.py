import os
import gzip
from silx.io.spech5 import SpecH5NodeDataset, SpecH5
from silx.io.commonh5 import Group
from datetime import datetime
import dateutil.parser
import collections
from id01lib.xrd import detectors
import numpy as np
import pdb
import re

ScanRange = collections.namedtuple("ScanRange", ['name', 'start', 'stop', 'numpoints'])

def isfile(path):
    try:
        return os.path.isfile(path)
    except TypeError:
        return False



def header_dict(header):
    result = collections.OrderedDict()
    for line in header:
        key, val = line.split(" ",1)
        val = val.strip()
        if key in result and val:
            if not isinstance(result[key], list):
                result[key] = [result[key]]
            result[key].append(val)
        else:
            result[key] = val
    return result




def _upcast(val):
    val = val.strip("{}")
    try:
        val = int(val)
        return val
    except ValueError:
        pass
    try:
        val = float(val)
        return val
    except ValueError:
        pass
    try:
        val = [float(v) for v in val.split("x")] # a bit bold
        return val
    except ValueError:
        pass
    return val




def _parse_line_dict(line):
    result = collections.OrderedDict()
    if not line:
        return result
    i = 0
    while True:
        pos = line.find("{", i)
        if pos<0:
            break
        i = pos+1
        s = line[line.find("{"):line.find("}")+1]
        s2 = s.replace(",", ";")
        line = line.replace(s, s2)
    line = line.split(",")
    for item in line:
        if not "=" in item:
            continue
        k,v = item.split("=")
        result[k] = _upcast(v)
    return result



def _add_line_dict_data(parent, groupname, line, varname=None):
    if line is None:
        return
    if varname is not None:
        line = "%s=%s"%(varname, line)
    subgroup = Group(groupname, parent=parent) # TODO: which detector?
    parent.add_node(subgroup)
    info = _parse_line_dict(line)
    for k,v in info.items():
        subgroup.add_node(
            SpecH5NodeDataset(name=k, data=v, parent=subgroup))
    return subgroup


def parse_spec_id01(scan):
    sh = scan["instrument/specfile/scan_header"]
    fh = scan["instrument/specfile/file_header"]

    sh_dict = header_dict(sh)
    fh_dict = header_dict(fh)

    start_time  = scan['start_time'].value.item()
    start_epoch = (dateutil.parser.parse(start_time) - datetime(1970,1,1,0,0,0)).total_seconds()
    scan.add_node(SpecH5NodeDataset(name="start_epoch",
                                    data=start_epoch,
                                    parent=scan))

    impath = [s for s in sh if s.startswith("#ULIMA_")] \
           + [s for s in fh if s.startswith("#ULIMA_")]
    #assert len(impath)==1, "Only one detector supported at the moment"
    #detector_name, impath = impath[0].split()
    #detector_number = list(detectors.order).index(detname)
    instrument = scan["instrument"]
    if "sample" not in scan:
        sample = Group("sample", parent=scan)
        scan.add_node(sample)
    sample = scan["sample"]
    """
    detectors = Group("detectors", parent=instrument)
    instrument.add_node(detectors)
    #pdb.set_trace()

    if impath:
        for entry in impath:
            detector_name, _ = entry.split()
            detector_name = detector_name[7:]
            detector = Group(detector_name, parent=detectors)
            detectors.add_node(detector)
        _add_line_dict_data(detector, "calibration", sh_dict.get("#UDETCALIB")) #TODO: which detector??
    else:
        print("Warning: no image path found in scan %s"%scan.name)
    """

    _add_line_dict_data(instrument, "monochromator", sh_dict.get("#UMONO"))

    _add_line_dict_data(instrument, "multilayer", sh_dict.get("#UMM"))

    _add_line_dict_data(instrument, "wb_mirror", sh_dict.get("#UMIR"))

    _add_line_dict_data(instrument, "insertion_devices", sh_dict.get("#UMA"))

    _add_line_dict_data(instrument, "filters", sh_dict.get("#UFILT1"), varname="fw")

    _add_line_dict_data(instrument, "tripod", sh_dict.get("#UTRIPOD"))

    #TODO: more clever parsing needed
    if "#UTRANSFOCATOR" in sh_dict:
        _add_line_dict_data(instrument, "transfocator", sh_dict["#UTRANSFOCATOR"])

    if "#UKBMIRROR" in sh_dict:
        lkb = "dimensions=" + sh_dict["#UKBMIRROR"].replace(":",",")
        _add_line_dict_data(instrument, "kb_mirror", lkb)

    if "#USLIT" in sh_dict:
        lslits = "dimensions=" + sh_dict["#USLIT"].replace(":",",")
        _add_line_dict_data(instrument, "slits", lslits)

    if "#UHEXAPOD" in sh_dict:
        instrument.add_node(SpecH5NodeDataset(name="hexapod",
                                          data=sh_dict["#UHEXAPOD"],
                                          parent=instrument))
        sample["hexapod"] = instrument["hexapod"]









def _read_edf_first_acq_nr(edfpath):
    """
        To find whether an .edf file is the only one of a pscan
        or if it is split up into several

        This is faster than opening the .edf file using `EdfFile`
    """
    if isinstance(edfpath, bytes):
        edfpath = edfpath.decode()
    if edfpath.endswith("edf.gz"):
        loader = gzip.open
    elif edfpath.endswith(".edf"):
        loader = open
    else:
        raise ValueError("Unsupported file: %s"%edfpath)
    with loader(edfpath) as fh:
        header = fh.read(400)
    lines = header.decode().splitlines()
    acq_frame_nb = [s for s in lines if s.startswith('acq_frame_nb')][0]
    acq_frame_nb = int(acq_frame_nb.split()[-2])
    return acq_frame_nb


def _fast_edf_collect(generic_path, idx):
    frame0 = _read_edf_first_acq_nr(generic_path%idx)
    if frame0>0 or not isfile(generic_path%idx):
        raise ValueError("Invalid starting file %s"%(generic_path%idx))
    paths = [generic_path%idx]
    idx += 1
    while True:
        path = generic_path%idx
        if not isfile(path):
            break
        frame0 = _read_edf_first_acq_nr(path)
        if frame0>0:
            paths.append(path)
            idx += 1
        else:
            break
    return paths



def fetch_image_files(scan, imgroot=None,explicit_beamline=None):
    # header is now a ndarray
    header = scan["instrument/specfile/scan_header"].value
    fheader = scan["instrument/specfile/file_header"].value
    if hasattr(header, "decode"): #silx 0.7.0
        header = header.decode().splitlines()
    if hasattr(fheader, "decode"): #silx 0.7.0
        fheader = fheader.decode().splitlines()
    if "_specfile" in scan.attrs: #id01h5
        specpath = os.path.realpath(scan.attrs["_specfile"])
    else:
        specpath = os.path.realpath(scan.file.filename) #spech5
    # this is useful to get relative paths in case the complete data
    # has been moved:
    for line in fheader:
        if line.startswith("#F"):
            orig_spec_path = os.path.realpath(line.lstrip("#F "))
            break
    orig_folder = os.path.dirname(orig_spec_path)
    #print specpath, orig_folder

    zap  = bool([s for s in header if s.startswith("#C ZAP")])
    fast = bool([s for s in header if s.startswith("#C pscan - fastscan")])

    if fast:
        impath = [s for s in header if s.startswith("#C imageFile")]
        if not impath:
            return []
        impath = impath[0]
        impath = impath.split()[2:]
        impath = dict((s.strip("]").split("[") for s in impath))
        if imgroot is None:
            imgroot = os.path.realpath(impath["dir"])
            # prefer relative because data is often moved:
            imgroot = os.path.relpath(imgroot, orig_folder)
            imgroot = os.path.join(os.path.dirname(specpath), imgroot)
            imgroot = os.path.abspath(imgroot)
        generic_path = os.path.join(
                imgroot,
                impath["prefix"] + impath["idxFmt"] + impath["suffix"]
                )
        idx = int(impath["nextNr"])
        #impath = generic_path%idx
        #counters = [s for s in header if s.startswith("#L")]
        imbase = [s for s in fheader if s.startswith("#ULIMA_")][0]
        detname, imbase = imbase.split()
        detname = detname[7:]
        all_paths = _fast_edf_collect(generic_path, idx)
        returndict = {detname:all_paths}
    elif zap:
        #pdb.set_trace()
        impath = [s for s in header if s.startswith("#C DIRECTORY")]
        zapdir = [s for s in header if s.startswith("#C RADIX")][0].split()[-1]
        zapno = [s for s in header if s.startswith("#C ZAP SCAN NUMBER")][0].split()[-1]
        imbase = [s for s in header if s.startswith("#ULIMA_")][0]
        detname, imbase = imbase.split()
        detname = detname[7:]
        impath=os.path.join(impath[0].split()[-1],zapdir+'_'+detname+"_%04.i"%int(zapno))
        all_paths = _fast_edf_collect(os.path.join(impath,zapdir+'_'+detname+"_%04.i"%int(zapno)+"_0000_%05i.edf"), 0)
        returndict = {detname:all_paths}
    else:
        #pdb.set_trace()
        no_imgs=len(scan[u'measurement/%s'%list(scan[u'measurement'].keys())[0]])
        #pdb.set_trace()
        impaths = [s for s in header if s.startswith("#ULIMA_")]
        if not impaths:
            if explicit_beamline=='APS34IDC':
                print("data from beamline: ", explicit_beamline )
                detname="amsGaAs"
                datafolder=os.path.join(imgroot,imgroot.split("/")[-2].split("AD")[-1]+"_S%04i"%int(header[0].split()[1])+"/")
                tiffs=[datafolder+x for x in os.listdir(datafolder) if x.endswith(b"tif")]
                returndict={detname:tiffs}
                #pdb.set_trace()
            else:
                print("no '#ULIMA_' tag found in scan header - data saving was switched off ...")
                #return dict()
                returndict = dict()
        
        for impath in impaths:
            detname, impath = impath.split() #what if we have several detectors?
            impath = os.path.realpath(impath)
            # prefer relative because data is often moved:
            impath = os.path.relpath(impath, orig_folder)
            #print impath
            impath = os.path.join(os.path.dirname(specpath), impath)
            impath = os.path.abspath(impath)
            detname = detname[7:] # discard "#ULIMA_"
            detshort = detectors.aliases.get(detname, detname)

            if imgroot is not None:
                impath = os.path.join(imgroot, os.path.basename(impath))
            """
            if detshort == "mpx22":
                inr = scan["measurement/%sir"%detshort]
            else:
                inr = scan["measurement/%sinr"%detshort]

            if not len(inr):
                return []
            
            startnr = int(inr[0])
            """
            
            pattern= r"\D(\d{%d})\D" % 5
            startnr = int(re.findall(pattern, impath)[-1])
            inr=np.arange(startnr,startnr+no_imgs)
            #print(impath,startnr,inr[-1],detshort)

            """
            #if ei2mworkaround:
            if not detshort == "ei2m": # workaround for new Eiger scans
            assert "_%05d"%startnr in impath, \
            "Error: %sinr not in image path"%detshort
            impath = impath.replace("_%05d"%startnr, "_%05d")
            else:
            assert "_%05d"%(startnr+1) in impath, \
            "Error: %sinr not in image path"%detshort
            impath = impath.replace("_%05d"%(startnr+1), "_%05d")
            #print(impath,startnr,detshort)
            """
            assert "_%05d"%startnr in impath, \
            "Error: %sinr not in image path"%detshort
            impath = impath.replace("_%05d"%startnr, "_%05d")
            #"""
            #assert "_%05d"%startnr in impath, \
            #     "Error: %sinr not in image path"%detshort
            #impath = impath.replace("_%05d"%startnr, "_%05d")
            all_paths = [impath%i for i in inr]
            #print all_paths,inr,detname
            returndict = {detname:all_paths}
    #print(returndict)
    return returndict



def load_pscan_rois(scan, rois):
    if isinstance(rois, str):
        rois = [rois]

    command = scan["title"].value
    command = command.split()
    #print command

    m1 = ScanRange(command[1], float(command[2]), float(command[3]), int(command[4]))
    m2 = ScanRange(command[5], float(command[6]), float(command[7]), int(command[8]))


    data = np.stack(scan["measurement"][roi].value for roi in rois)
    data = data.reshape(-1,m2.numpoints, m1.numpoints).squeeze()
    return m1, m2, data





class id01SpecH5(SpecH5):
    def __init__(self, filename):
        super(id01SpecH5, self).__init__(filename)
        for scan in self.values():
            parse_spec_id01(scan)
