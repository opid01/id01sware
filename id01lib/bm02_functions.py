"""
Set of functions to analyse data collected on BM02. See also scripts contained in /examples.
Needs to be documented.

EZ 7.01.19
"""


import numpy as np
import xrayutilities as xu
import h5py
import os
import sys
import fabio
import pyFAI

from silx.gui.plot.StackView import StackViewMainWindow
from pyFAI.distortion import Distortion
from silx.io.convert import write_to_h5
from scipy.signal import medfilt2d

def read_imgs(sample, scan_no):

    # Specify base dir and h5 file
    basedir = '/mntdirect/_data_id01_inhouse/edo/ih-hc3484'

    # Specify image directory
    imgdir = '{0}/{1}/{1}.spec_S{2}/'.format(basedir, sample, scan_no)

    # List files in image directory
    imglist = os.listdir(imgdir)
    imglist.sort()
    imglistlen = len(imglist)

    # Load distortion correction
    D5 = pyFAI.detector_factory("{0}/analysis/D5Geom_JK.h5".format(basedir))
    D5_dis = Distortion(D5, resize=True)

    # Load hot pix mask
    hotpix = fabio.open('{0}/analysis/maskD5_V1_9keV_fastOTNpulse-20171122.edf.gz'.format(basedir)).data

    # Load FF correction
    ff = fabio.open('{0}/analysis/flatD5_V1_9keV_fastOTNpulse-20171122.edf.gz'.format(basedir)).data

    # Open each image with FabIO and correct it for distorion, hotpix, FF
    openimglist = []
    for index, img  in enumerate(imglist):
        sys.stdout.write("Loading image {0} of {1}".format(index, imglistlen))
        sys.stdout.write("\r")
        raw_img = fabio.open(imgdir+img).data / ff
        raw_img[hotpix==1] = 0
        raw_img = medfilt2d(raw_img, [3,3])
        D5_cor = D5_dis.correct(raw_img)
        openimglist.append(D5_cor)

    # Stack the images (depth, detx, dety)
    imgstack = np.dstack(tuple(openimglist)).transpose(2,0,1)

    return imgstack


def qconvert(sample, scan_no, nbins, cenpix=[256,760],
             imgstack=None, deltas=[0,0,0,0,0], roi=[0, 579, 600, 1153],
             filtcorrection=True):

    """ deltas are: dth, dchi, dphi, dnu, dtth """

    # Init foil correction LUT
    foils = {0:1, 1:2.817, 2:7.934, 3:22.35, 4:62.95, 5:177.3, 6:499.4,
             7:1407, 8:3963, 9:1.116e4, 10:3.144e4, 11:8.856e4, 12:2.494e5,
             13:7.026e5, 14:1.979e6, 15:5.574e6}

    # Specify base dir, h5 files, specfile
    basedir = '/mntdirect/_data_id01_inhouse/edo/ih-hc3484'
    h5file_read = '{0}/analysis/{1}/{1}.h5'.format(basedir, sample)

    # Set up detector parameters
    en = 8000
    distance = 0.512
    cch = cenpix
    npix = [579,1153]
    pixsize = [130e-6, 130e-6]
    chpdeg = [67, 67]

    print('--> Cen pix: {0}'.format(cch))
    print('--> Filt correction: {0}'.format(filtcorrection))

    # kappa goniometer
    # Theta = LH, Chi = RH, Phi = LH, Nu= RH, tth=LH ,
    # x is along the beam, y is outboard, z is up.
    qconv = xu.experiment.QConversion(['y-','x+','z-'],['z+','y-'],[1,0,0])

    # Init the experiment class feeding it the geometry
    hxrd = xu.HXRD([1,0,0], [0,0,1], en=en, qconv=qconv)

    # Init the area detector
    hxrd.Ang2Q.init_area('y+','z-',
                        cch1=cch[0],
                        cch2=cch[1],
                        Nch1=npix[0],
                        Nch2=npix[1],
                        pwidth1=pixsize[0],
                        pwidth2=pixsize[1],
                        distance=distance,
                        roi=roi)

    # Read motors and images from h5, correct for filters
    motors = dict()
    with h5py.File(h5file_read, 'r') as h5f:
        pos = h5f['{0}.1/instrument/positioners'.format(scan_no)]
        for motor in pos:
            motors[motor] = pos[motor].value
        if imgstack is None:
            raw_imgs = h5f['{0}.1/imgs'.format(scan_no)].value
            print('>> Images loaded')
        else:
            raw_imgs = imgstack
        if filtcorrection:
            filterval = h5f['{0}.1/measurement/pfoil'.format(scan_no)].value
            for filt, index in zip(filterval, range(raw_imgs.shape[0])):
                mult = foils[filt]
                raw_imgs[index] = raw_imgs[index] * mult
        print(raw_imgs.min(), raw_imgs.max())

    # Set the angles
    angles = {'2THETA':0, 'THETA':0, 'nu':0, 'PHI':0, 'CHI':0}

    # Read the angles from the h5 file
    maxlen = 1
    for angle in angles:
        dset = motors[angle if angle is not "delta" else "del"]
         # if it's 0 motor is still, if it's 1 motor is changing during scan
        if len(dset.shape):
            maxlen = max(maxlen, dset.shape[0])
        if angle == 'CHI':
            position = dset-90
        else:
            position = dset
        angles[angle] = position

    # if the angle is kept constant during a scan, it is a scalar. If that's the case,
    # make it the same shape as the angle(s) which is being varied during a sca
    for angle in angles:
        if np.isscalar(angles[angle]):
            angles[angle] = np.ones(maxlen, dtype=float) * angles[angle]

    # Calculate q space values
    qx, qy, qz = hxrd.Ang2Q.area(angles['THETA'],angles['CHI'],
                                 0,angles['nu'],
                                 angles['2THETA'],delta=[deltas]) #replaced *deltas SJL 22/03/2019

    # Transpose the intensity array to match the dimensions of qx,y,z
    raw_imgs = raw_imgs.transpose(0,2,1)[:, roi[0]:roi[1], roi[2]:roi[3]]

    print(qx.shape, raw_imgs.shape)

    # Grid qspace
    gridder = xu.Gridder3D(*nbins)
    gridder(qx, qy, qz, raw_imgs)

    # Retrieve gridded values
    qxx, qyy, qzz = gridder.xaxis, gridder.yaxis, gridder.zaxis
    gint = gridder.data

    return qxx, qyy, qzz, gint

def viewstack(stack):
    # use only from ipython with %gui qt
    sv = StackViewMainWindow()
    sv.setColormap("jet", autoscale=True)
    sv.setStack( stack)
    sv.show()

def rm_unwanted_imgs(h5file, scan_no):

    # open image stack
    with h5py.File(h5file, 'r') as h5f:
            imstack = h5f['{0}.1/imgs'.format(scan_no)].value

    # make a list of the mean of each img in the stack
    lsmean = []
    for index in range(imstack.shape[0]):
        lsmean.append(imstack[index,:,:].mean())

    # find the index with the highest intensity
    lsmean = np.array(lsmean)
    maxidx = lsmean.argsort()[::-1][0]

    # replace idxs left and right of max idx
    imstack[maxidx-1,:,:] = imstack[maxidx-2,:,:]
    imstack[maxidx+1,:,:] = imstack[maxidx+2,:,:]
    imstack[maxidx,:,:] = (imstack[maxidx-1,:,:]+imstack[maxidx+1,:,:])/2

    return imstack
