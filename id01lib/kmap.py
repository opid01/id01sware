"""
    This will contain some convenience functions for 5D-KMAP data in
    hd5 format. Maybe, they should be moved to the `xsocs` package one day
"""
import collections
import h5py
from .xrd.qconversion import kmap_get_qcoordinates as get_qcoordinates
from silx.io import specfile as spec

ScanRange = collections.namedtuple("ScanRange", ['name',
                                                 'start',
                                                 'stop',
                                                 'numpoints'])

def get_scan_parameters(masterH5):
    with h5py.File(masterH5, 'r') as h5m:
        somekey = next(h5m.__iter__())
        someentry = h5m[somekey]
        scanpar = someentry["scan"]
        motors = []
        for i in range(2):
            key = "motor_%i"%i
            m = ScanRange(scanpar[key][()],
                          scanpar["%s_start"%key][()],
                          scanpar["%s_end"%key][()],
                          scanpar["%s_steps"%key][()]
                )
            motors.append(m)
        Nentries = len(h5m)
        det = someentry["instrument/detector"]
        result = dict()
        result["exposuretime"] = float(someentry["title"][()].split(" ")[-1])
        for key in det:
            ds = det[key]
            if not ds.shape:
                result[key] = ds[()]
    shape = (Nentries, motors[1].numpoints, motors[0].numpoints) #slow to fast
    result["shape"] = shape
    result["motor0"] = motors[0]
    result["motor1"] = motors[1]
    return result


def read_kmap(specfile, scan_no, rois, motors=False, imshow=False):
    """
    out: dictionary with rois/motors
    """
    # read the specfile
    sf = spec.SpecFile(specfile)
    # get the data
    data = sf[scan_no].data
    # get spec indexes
    index_list = sf.labels(scan_no)
    actual_num = sf.keys()[scan_no]
    # init results dict
    res = {}

    for index, roi in enumerate(rois):
        # load data
        rawdata = data[index_list.index(roi)]
        # remove hot pixels
        rawdata[rawdata>1e7] = 0
        # load motor positions
        motor_1 = data[index_list.index('adcX')]
        motor_2 = data[index_list.index('adcY')]
        command = sf.command(scan_no).split() # the command typed in spec
        new_shape = (int(command[8]), int(command[4]))
        # apply dimensions
        rawdata.shape, motor_1.shape, motor_2.shape = new_shape, new_shape, new_shape
        # append to dict
        if motors:
            res['motors'] = (motor_1, motor_2)
        if imshow:
            res[roi] = rawdata.T[::-1]
        else:
            res[roi] = rawdata

    return res
