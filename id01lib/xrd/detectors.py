"""
    This is the file where detectors are defined.
    Todo: Andor and Eiger
"""
import numpy as np
import collections



class AreaDetector(object):
    """
        The Base class.
        This is temporary until proper nexus classes are implemented
    """
    def __init__(self, directions, pixsize, pixnum, mask=None, chunks=None):
        if np.isscalar(pixsize):
            pixsize = (pixsize, pixsize)
        if np.isscalar(pixnum):
            pixnum = (pixnum, pixnum)
        self.directions = directions
        self.pixsize = pixsize
        self.pixnum  = pixnum
        self.mask = mask
        self.chunks = pixnum if chunks is None else chunks # for h5py
    
    @staticmethod
    def correct_image(image):
        pass # to be implemented for each detector


class MaxiPix(AreaDetector):
    alias = shortalias = "mpx4"
    def __init__(self, mask=None):
        super(MaxiPix, self).__init__(directions=("z-", "y+"),
                                      pixsize=55e-6,
                                      pixnum=516,
                                      mask=mask,
                                      chunks=(258, 258)
                                     )
    @staticmethod
    def correct_image(image):
        """
            Mind the gap.
        """
        image *= 9   # TODO: warum? the pixel at the edge has 3 times more counts, should be distributed to the other pixels.
        image[255:258] = image[255]/3
        image[258:261] = image[260]/3
        image[:,255:258] = (image[:,255]/3)[:,None]
        image[:,258:261] = (image[:,260]/3)[:,None]

    @staticmethod
    def ff_correct_image(image):
        """
            perhaps a flatfield here
        """
        pass


class MaxiPixGaAs(MaxiPix):
    alias = shortalias = "mpx22"


class Eiger2M(AreaDetector):
    alias = "eiger2M"
    shortalias = "ei2m"
    def __init__(self, mask=None):
        super(Eiger2M, self).__init__(directions=("z-", "y+"),
                                      pixsize=75e-6,
                                      pixnum=(2164,1030),
                                      mask=mask,
                                      chunks = (541, 515)
                                     )
    @staticmethod
    def ff_correct_image(image):
        """
            perhaps a flatfield here
        """
        pass

    @staticmethod
    def mask_image(image):
        """
            Mind the BIG gaps and the bad columns
        """
        pass


class Andor(AreaDetector):
    alias = "andor"
    shortalias = "and"
    def __init__(self, mask=None):
        super(Andor, self).__init__(directions=("z-", "y+"),
                                      pixsize=6.5e-6,
                                      pixnum=(2560,2160),
                                      mask=mask,
                                      chunks = (640, 540) # 4 by 4
                                     )

class Frelon3(AreaDetector):
    alias = "frelon3"
    shortalias = "frel"
    def __init__(self, mask=None):
        super(Andor, self).__init__(directions=("z-", "y+"),
                                      pixsize=14e-6,
                                      pixnum=(2048,2048),
                                      mask=mask,
                                      chunks = (256, 256) # 8 x 8
                                     )


class AStimepix(AreaDetector):
    alias = "AStimepix"
    shortalias = "AStp"
    def __init__(self, mask=None):
        super(AStimepix, self).__init__(directions=("z-", "y+"), # determined by xrayutilities _determine_detdir
                                      pixsize=55e-6,
                                      pixnum=(512,512),
                                      mask=mask,
                                      chunks = (256, 256)
                                     )
    @staticmethod
    def ff_correct_image(image):
        """
            perhaps a flatfield here
        """
        pass

    @staticmethod
    def mask_image(image):
        """
            Mind the BIG gaps and the bad columns
        """
        pass


order = (MaxiPix, Eiger2M, Andor, MaxiPixGaAs, AStimepix) # for hdf writing

aliases = collections.OrderedDict((d.alias, d.shortalias) for d in order)
order = collections.OrderedDict((d.alias, d) for d in order)
