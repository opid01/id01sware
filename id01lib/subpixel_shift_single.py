"""
Script stolen from Carsten to apnpy a shift calculated by other means to a set of
kmap generated images. The shift is assumed to consist of two columns,
[x_pixel_shift, y_pixel_shift]. Modified  the 'order' param of nd.shift to 3 (default),
instead of the =1 used by CR - it resulted in a segfault.
"""


import os
import sys
import numpy as np
import h5py
import multiprocessing
import time
import glob

from scipy import ndimage
from numpy import array


os.environ["OPENBLAS_MAIN_FREE"] = '1'

# define the directory in which the XSOCS-merged h5 files are contained
basedir = '/mntdirect/_data_id01_inhouse/edo/20180406_HC3505_ID01/id01/analysis/3dkmaps/e17089_both_fast19/'

# get the relevant file list contained in that directory
flist = glob.glob('{0}/kmap_*.1.h5'.format(basedir))

# select number of multiprocessing cores
nproc = None

# input the drift
def get_shifts():

    d={'22.615599': array([0., 0.]),
     '22.715597': array([3., 0.]),
     '22.815598': array([6., 1.]),
     '22.915598': array([10.,  1.]),
     '23.015598': array([12.,  1.]),
     '23.115599': array([12.,  0.]),
     '23.215597': array([15.,  0.]),
     '23.315598': array([17.,  0.]),
     '23.415598': array([20.,  0.]),
     '23.515598': array([23.,  0.]),
     '23.615599': array([26.,  0.]),
     '23.715597': array([31.,  0.]),
     '23.815598': array([35.,  0.]),
     '23.915598': array([36.,  0.]),
     '24.015598': array([39.,  1.]),
     '24.115599': array([41.,  1.]),
     '24.215597': array([43.,  1.]),
     '24.315598': array([45.,  1.]),
     '24.415598': array([48.,  1.]),
     '24.515598': array([52.,  1.])}

    return d


def shift_kmap(fname):
    with h5py.File(fname, libver='latest') as f:

        # Init
        print('Loaded {0}'.format(fname))
        t = time.time()
        d = get_shifts()

        # Select the scan
        entry = list(f.values())[0]
        det = entry["instrument/detector"]
        data = det["data"]
        scan = entry["scan"]
        eta = str(entry['instrument/positioners/eta'].value)

        # Get the shift in microns
        drift = d[eta]
        shape = tuple(scan["motor_%i_steps"%i].value for i in (0,1))
        scanrange = tuple(scan["motor_%i_end"%i].value-scan["motor_%i_start"%i].value for i in (0,1))
        deltaxy = [scanrange[i]/shape[i] for i in (0,1)]
        pixdrift = np.array(drift)/np.array(deltaxy)
        shift = -pixdrift[::-1]
        print("Calculated shift: {0} rows, {1} cols for eta={2}".format(shift[0], shift[1], eta))

        # Get the shapes of the data
        cs = data.chunks[1:]
        ds = data.shape

        # Create a temporary file
        t2 = 0.
        tmpfile = os.path.join("/tmp", "kmap%i%f%s"%(hash(fname), time.time(), os.path.basename(fname)))
        print('Temp file created: {0}'.format(tmpfile))
        tmpfile = h5py.File(tmpfile)

        # Create a new dataset in the tempfile
        if "data" not in tmpfile:
            datanew = tmpfile.create_dataset("data_shifted",
                                 shape=data.shape,
                                 chunks=data.chunks,
                                 dtype=data.dtype,
                                 compression="gzip",
                                 compression_opts=4)

        # For each chunked data point
        for i1 in range(ds[1]//cs[0]):
            for i2 in range(ds[2]//cs[1]):

                # Read the data point
                _t2 = time.time()
                chunk = data[:,
                         i1*cs[0]:(i1+1)*cs[0],
                         i2*cs[1]:(i2+1)*cs[1]].astype(np.float32)
                t2 += time.time()-_t2
                chunk= chunk.reshape(shape[::-1]+(-1,))

                # If shifts are non-zero within tolerance
                if not np.allclose(shift, 0, atol=1e-3):
                    print('')
                    for i in range(chunk.shape[-1]):

                        # Shift the data point
                        chunk[...,i] = ndimage.shift(chunk[...,i], shift)

                chunk.resize((np.prod(shape),) + cs)
                chunk = (chunk*10).astype(np.uint16)


                # Write the shifted chunks
                _t2 = time.time()
                datanew[:,
                     i1*cs[0]:(i1+1)*cs[0],
                     i2*cs[1]:(i2+1)*cs[1]] = chunk
                t2 += time.time()-_t2
                del chunk
        del det["data"]
        del entry['measurement/image/data'] # remove both positions of data
        f.flush()

    tmpfile.flush()

    # Now copy the tmp file shifted data to the original file
    with h5py.File(fname, libver='latest') as f:
        entry = list(f.values())[0]
        det = entry["instrument/detector"]
        det.copy(datanew, "data")
        entry['measurement/image/data'] = det["data"] # hard link
        f.flush()

    tmpfile.clear()
    tmpfile.close()

    print("%s finished after %fs. I/O time: %f"%(fname, time.time()-t, t2))
#
# pool = multiprocessing.Pool(nproc) #  cores
# #
# pool.map(shift_kmap, flist)
# pool.close()
# pool.join()
# # #pool.terminate()
#del pool


shift_kmap(basedir+'kmap_00679_7.1.h5')
