import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import ImageGrid


def plot_kmap(specfile, scan_no, rois, log=False, axeslabels=False, save=False,
              saveformat='pdf', savedir=None, extra=None, plotshape=None,
              colormap='viridis', m=',', scatter=False, **kwargs):

    """
    Plot selected ROIs from a given kmap fast_ file.

    Parameters
    ----------
    TBD!

    Returns
    --------
    fig: matplotlib.pyplot.figure instance
    """

    # get the data
    data = specfile[scan_no].data
    # get the spec indexes (list)
    index_list = specfile.labels(scan_no)
    # other counters
    sfname = specfile.file_header(0)[0][::-1][21::-1]
    thx = specfile[scan_no].motor_position_by_name('thx')
    thy = specfile[scan_no].motor_position_by_name('thy')
    date = specfile.scan_header(scan_no)[3][3:]
    phi = specfile[scan_no].motor_position_by_name('phi')
    eta = np.round(specfile[scan_no].motor_position_by_name('eta'), decimals=3)
    delval = np.round(specfile[scan_no].motor_position_by_name('del'), decimals=3)

    fig = plt.figure(edgecolor='red',**kwargs)
    fig.suptitle('{0} #{1} | {2}'.format(sfname, scan_no, extra))
    grid = ImageGrid(fig, 111,
                    nrows_ncols=plotshape,
                    axes_pad = (0.1,0.5),
                    cbar_mode = 'each',
                    cbar_size = '2%',
                    cbar_pad = '2%',
                    cbar_location = 'top')

    for index, roi in enumerate(rois):

        # load data
        rawdata =  data[index_list.index(roi)]
        rawdata[rawdata > 1e8] = 1
        # load motor positions
        motor_1 = data[index_list.index('adcX')]
        motor_2 = data[index_list.index('adcY')]
        command = specfile.command(scan_no).split()
        new_shape = (int(command[8]), int(command[4]))
        # apply dimensions
        rawdata.shape, motor_1.shape, motor_2.shape = new_shape, new_shape, new_shape
        # plot it
        if not log:
            I = rawdata
        else:
            I = np.log(rawdata)
        if scatter:
            im = grid[index].scatter(motor_1, motor_2, c=I, cmap=colormap, marker=m)
        else:
            im = grid[index].pcolormesh(motor_1, motor_2, I, cmap=colormap)
        im.axes.set_aspect('equal')
        if axeslabels:
            grid[index].set_ylabel(r'x piezo [$\mu m$]',fontsize=8)
            grid[index].set_xlabel(r'y piezo [$\mu m$]',fontsize=8)
        grid[index].set_title('{0}'.format(roi), pad=30,fontdict={'fontsize':9, 'fontweight':'bold'})
        cbar = grid.cbar_axes[index].colorbar(im)
        cbar.ax.tick_params(labelsize=8)

        plt.gcf().text(0.54, 0.01, 'eta={0}; del={1}; phi={2}\nthx={3}, thy={4}\n{5}'.format(eta, delval, phi, thx, thy, date),
                       size=10, weight='normal', va='top', ha='center', bbox=dict(boxstyle="round", facecolor='orange', alpha=0.1))

        if save:
            plt.savefig('{0}/{1}__{2}.{3}'.format(savedir,sfname,scan_no,saveformat),
                        dpi=100, bbox_inches='tight')

    return grid
