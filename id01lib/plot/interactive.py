import os
import collections
import numpy as np
from scipy import interpolate, ndimage

import matplotlib.pyplot as plt
from matplotlib import colors
from matplotlib.widgets import Slider, Button, Cursor, AxesWidget
from matplotlib.patches import Rectangle, Polygon
from matplotlib.figure import Figure
from matplotlib import gridspec

from id01lib import image

def cnorm(data, normtype, dmin=None, dmax=None):
    if dmax is None:
        dmax = data.max()
    if dmin is None:
        ipos = data>0
        if ipos.any():
            dmin = data[ipos].min()
        else:
            dmin = data.min()


    if normtype=="log":
        return colors.LogNorm(dmin, dmax)

    if isinstance(normtype, float):
        return colors.PowerNorm(normtype, dmin, dmax)

    return colors.Normalize(dmin, dmax)




def qspace_slide_grid(qxg, qyg, qzg, data, start=1, cut='x'):

    # prepare figure
    fig, ax = plt.subplots()
    plt.subplots_adjust(bottom=0.25)

    #  mesh
    if cut == 'x':
        im = plt.pcolormesh(qyg[0,:,:],qzg[0,:,:],data[start,:-1,:-1],cmap='jet')
        plt.axis('image')
        axcolor = 'lightgoldenrodyellow'
        axslide = plt.axes([0.2, 0.1, 0.65, 0.03], facecolor=axcolor)
        slide = Slider(axslide, 'imnr',1, data.shape[0],valinit=start, valfmt='%1d')
        def update(val):
            imnr = slide.val
            im.set_array(data[int(imnr),:-1,:-1].ravel())
            im.set_clim(vmin=data.min(),vmax=data.max())
            fig.canvas.draw()
    if cut == 'y':
        im = plt.pcolormesh(qxg[:,0,:], qzg[:,0,:],data[:-1,start,:-1],cmap='jet')
        plt.axis('image')
        axcolor = 'lightgoldenrodyellow'
        axslide = plt.axes([0.2, 0.1, 0.65, 0.03], facecolor=axcolor)
        slide = Slider(axslide, 'imnr',1, data.shape[1],valinit=start, valfmt='%1d')
        def update(val):
            imnr = slide.val
            im.set_array(data[:-1,int(imnr),:-1].ravel())
            im.set_clim(vmin=data.min(),vmax=data.max())
            fig.canvas.draw()
    if cut == 'z':
        im = plt.pcolormesh(qxg[:,:,0], qyg[:,:,0],data[:-1,:-1,start],cmap='jet')
        plt.axis('image')
        axcolor = 'lightgoldenrodyellow'
        axslide = plt.axes([0.2, 0.1, 0.65, 0.03], facecolor=axcolor)
        slide = Slider(axslide, 'imnr',1, data.shape[2],valinit=start, valfmt='%1d')
        def update(val):
            imnr = slide.val
            im.set_array(data[:-1,:-1,int(imnr)].ravel())
            im.set_clim(vmin=data.min(),vmax=data.max())
            fig.canvas.draw()

    slide.on_changed(update)
    plt.show()




class GenericIndexTracker(object):
    """
        Just a matplotlibe widget that allows scrolling through
        several images and picking a point.
        No connection to spec.
    """
    _axes_properties = {}
    def __init__(self,
                 ax,
                 data=None,
                 norm="linear",
                 quantum=1.,
                 exit_onclick=False,
                 rectangle_onclick=False,
                 imshow_kw={}):

        self.ax = ax
        self.fig = ax.figure
        ax.format_coord = self.format_coord

        self._norm = norm

        if data is None:
            if not ax.images:
                raise ValueError("No data found.")
            data = ax.images[-1].get_array()
        self.data = data = np.array(data, ndmin=3)
        self.slices = data.shape[0]
        self.ind = 0 # starting picture

        if self.slices>1:
            ax.set_title('use scroll wheel to navigate images')
            #self.fig.suptitle('use scroll wheel to navigate images')


        imkwargs = dict(interpolation="nearest",
                        origin="lower",
                        norm=cnorm(data[self.ind], norm))
        imkwargs.update(imshow_kw)
        self.clims = {}
        if not ax.images:
            self.im = ax.imshow(data[self.ind], **imkwargs)
            self.cb = plt.colorbar(self.im)
        else:
            self.im = ax.images[-1]

        if not hasattr(self, "cursor"): # first time
            #print('switch on onclick',rectangle_onclick)
            self.cursor = Cursor(ax, useblit=True, color='red', linewidth=1)
            if not rectangle_onclick:
                self.fig.canvas.mpl_connect('button_release_event', self.onclick)
                print('switch on onclick')
            self.fig.canvas.mpl_connect('scroll_event', self.onscroll)
        self.update(True)

        self.exit_onclick = exit_onclick
        self.rectangle_onclick = rectangle_onclick

        if self.rectangle_onclick and not self.exit_onclick:
            self.rect = Rectangle((0,0), 1, 1, facecolor='None', edgecolor='green')
            self.x0 = 0
            self.y0 = 0
            self.x1 = 0
            self.y1 = 0
            self.ax.add_patch(self.rect)
            self.toggle = False
            self.ax.figure.canvas.mpl_connect('button_press_event', self.on_press)
            self.ax.figure.canvas.mpl_connect('button_release_event', self.on_release)
            self.ax.figure.canvas.mpl_connect('motion_notify_event', self.on_motion)
        self.POI_list=[]


    def set_extent(self, xmin, xmax, ymin, ymax):
        extent = (xmin, xmax, ymin, ymax)
        self.im.set_extent(extent)

    def set_percentile(self, lower, upper):
        for i,d in enumerate(self.data):
            vmin, vmax = image.percentile_interval(d, lower, upper)
            self.clims[i] = vmin, vmax

    def set_axes_properties(self, **prop):
        """
            This allows to define multiple properties for
            a matplotlib subplot which will be used for the different
            frames when scrolling the mouse wheel.
        """
        for k in prop:
            setter = "set_%s"%k
            val = prop[k]
            if not hasattr(self.ax, setter):
                continue
            if hasattr(val, "__iter__") and len(val)==self.slices:
                self._axes_properties[setter] = val
            else:
                self._axes_properties[setter] = [val]*self.slices


    def format_coord(self, x, y):
        xlabel = self.ax.xaxis.label._text
        ylabel = self.ax.yaxis.label._text
        ext = self.im._extent
        A = self.im._A
        ix = int((x - ext[0]) / (ext[1] - ext[0]) * A.shape[1])
        iy = int((y - ext[2]) / (ext[3] - ext[2]) * A.shape[0])
        ix = np.clip(ix, 0, A.shape[1]-1)
        iy = np.clip(iy, 0, A.shape[0]-1)
        I = A[iy, ix]
        return '%s=%1.4f, %s=%1.4f, I=%g'%(xlabel, x, ylabel, y, I)

    def onscroll(self, event):
        if self.slices==1:
            return
        #print("%s %s" % (event.button, event.step))
        if event.button == 'up': #up should be previous
            ind = np.clip(self.ind - 1, 0, self.slices - 1)
        else:
            ind = np.clip(self.ind + 1, 0, self.slices - 1)

        if self.ind != ind:
            self.ind = ind
            self.update(props=True)

    def onclick(self, event):
        if not event.inaxes==self.ax:
            print("\nYou did not click on the display.")
            return
        xlabel = self.ax.xaxis.label._text
        ylabel = self.ax.yaxis.label._text
        xdata = event.xdata
        ydata = event.ydata
        print("You selected:    %s %.2f    %s %.2f"%(xlabel, xdata, ylabel, ydata))
        self.POI = xdata, ydata
        self.POI_mot_nm = xlabel, ylabel
        self.POI_list.append((xdata, ydata))
        #self.ax.annotate("p%i"%len(self.POI_list), xy=(ydata, xdata),)
        if self.exit_onclick:
            plt.close(self.fig)

    def on_press(self,event):
        self.x0 = event.xdata
        self.y0 = event.ydata
        self.x1 = event.xdata
        self.y1 = event.ydata
        try:
            self.rect.set_width(self.x1 - self.x0)
            self.rect.set_height(self.y1 - self.y0)
            self.rect.set_xy((self.x0, self.y0))
        except TypeError:
            print("You clicked outside the window - try again")

        self.rect.set_linestyle('dashed')
        self.ax.figure.canvas.draw()
        self.toggle = True

    def on_motion(self,event):
        if self.on_press is True:
            return
        if self.toggle:
            self.x1 = event.xdata
            self.y1 = event.ydata
            try:
                self.rect.set_width(self.x1 - self.x0)
                self.rect.set_height(self.y1 - self.y0)
                self.rect.set_xy((self.x0, self.y0))
            except TypeError:
                print("You moved the mouse outside the window - try again")
            self.rect.set_linestyle('dashed')
            self.ax.figure.canvas.draw()

    def on_release(self, event):
        if not event.inaxes==self.ax:
            print("\nYou did not click on the display.")
            self.on_press=False
            self.toggle=False
            return
        #print 'release'
        self.x1 = event.xdata
        self.y1 = event.ydata
        try:
            self.rect.set_width(self.x1 - self.x0)
            self.rect.set_height(self.y1 - self.y0)
            self.rect.set_xy((self.x0, self.y0))
        except TypeError:
            print("You clicked outside the window - try again")
        self.rect.set_linestyle('solid')
        self.ax.figure.canvas.draw()
        plt.close(self.fig)

    def update(self, props=False):
        if props:
            for k,v in self._axes_properties.items():
                getattr(self.ax, k)(v[self.ind])
        data = self.data[self.ind]
        self.im.set_data(data)
        vmin, vmax = self.clims.get(self.ind, (None, None))
        self.im.set_norm(cnorm(data, self._norm, vmin, vmax))
        self.fig.canvas.draw()



class GenericIndexTracker1D(object):
    """
        Just a matplotlibe widget that allows scrolling through
        several images and picking a point.
        No connection to spec.
    """
    _axes_properties = {}
    def __init__(self, ax, data=None, norm="linear", quantum=1.,exit_onclick=False,rectangle_onclick=False):
        self.ax = ax
        self.fig = ax.figure
        #ax.format_coord = self.format_coord

        self._norm = norm

        if data is None:
            if not ax.lines:
                raise ValueError("No data found.")
            data = ax.lines[-1].get_array()
        self.data = data = np.array(data, ndmin=3)
        self.slices = data.shape[0]

        self.ind = 0 # starting picture

        if self.slices>1:
            ax.set_title('use scroll wheel to navigate images')
            #self.fig.suptitle('use scroll wheel to navigate images')


        imkwargs = dict(interpolation="nearest",
                        origin="lower",
                        norm=cnorm(data[self.ind], norm))
        if not ax.lines:
            self.ln = ax.plot(data[self.ind,:,0])
            #self.cb = plt.colorbar(self.im)
        else:
            self.ln = ax.lines[-1]

        if not hasattr(self, "cursor"): # first time
            self.cursor = Cursor(ax, useblit=True, color='red', linewidth=1)
            self.fig.canvas.mpl_connect('button_release_event', self.onclick)
            self.fig.canvas.mpl_connect('scroll_event', self.onscroll)
        self.update(True)

        self.exit_onclick = exit_onclick

        self.POI_list=[]


    def set_axes_properties(self, **prop):
        """
            This allows to define multiple properties for
            a matplotlib subplot which will be used for the different
            frames when scrolling the mouse wheel.
        """
        for k in prop:
            setter = "set_%s"%k
            val = prop[k]
            if not hasattr(self.ax, setter):
                continue
            if hasattr(val, "__iter__") and len(val)==self.slices:
                self._axes_properties[setter] = val
            else:
                self._axes_properties[setter] = [val]*self.slices


    def format_coord(self, x, y):
        xlabel = self.ax.xaxis.label._text
        ylabel = self.ax.yaxis.label._text
        ext = self.im._extent
        A = self.im._A
        ix = int((x - ext[0]) / (ext[1] - ext[0]) * A.shape[1])
        iy = int((y - ext[2]) / (ext[3] - ext[2]) * A.shape[0])
        ix = np.clip(ix, 0, A.shape[1]-1)
        iy = np.clip(iy, 0, A.shape[0]-1)
        I = A[iy, ix]
        return '%s=%1.4f, %s=%1.4f, I=%g'%(xlabel, x, ylabel, y, I)

    def onscroll(self, event):
        if self.slices==1:
            return
        #print("%s %s" % (event.button, event.step))
        if event.button == 'up': #up should be previous
            ind = np.clip(self.ind - 1, 0, self.slices - 1)
        else:
            ind = np.clip(self.ind + 1, 0, self.slices - 1)

        if self.ind != ind:
            self.ind = ind
            self.update(props=True)

    def onclick(self,event):
        if not event.inaxes==self.ax:
            print("\nYou did not click on the display.")
            return
        xlabel = self.ax.xaxis.label._text
        ylabel = self.ax.yaxis.label._text
        xdata = event.xdata
        ydata = event.ydata
        print("You selected:    %s %.2f  "%(xlabel, xdata))
        self.POI = xdata
        self.POI_mot_nm = xlabel
        self.POI_list.append((xdata, ydata))
        #self.ax.annotate("p%i"%len(self.POI_list), xy=(ydata, xdata),)
        if self.exit_onclick:
            plt.close(self.fig)


    def update(self, props=False):
        if props:
            for k,v in self._axes_properties.items():
                getattr(self.ax, k)(v[self.ind])
        data = self.data[self.ind,:,0]
        self.ln[0].set_ydata(data)
        self.ax.set_ylim(min(data),max(data))
        self.fig.canvas.draw()







class Overlay(GenericIndexTracker):
    def __init__(self, maxshift=0.25, *args, **kwargs):
        self.maxshift = maxshift
        super(Overlay, self).__init__(*args, **kwargs)
        ax = self.ax
        fig = self.fig
        fig.subplots_adjust(bottom=0.2)



        self.reference = self.data.copy()

        self.ax_slider_x =  fig.add_axes((0.1,0.05,0.25,0.05))
        self.ax_slider_y =  fig.add_axes((0.45,0.05,0.25,0.05))
        self.ax_button   =  fig.add_axes((0.8,0.05,0.1,0.05))

        self.button = Button(self.ax_button, 'Reset', hovercolor='0.975')
        self.button.on_clicked(self._reset)

        self.slider_x = Slider(self.ax_slider_x,
                               "dx",
                               -maxshift,
                               maxshift,
                               valinit=0)

        self.slider_y = Slider(self.ax_slider_y,
                               "dy",
                               -maxshift,
                               maxshift,
                               valinit=0)


        self.slider_x.on_changed(self.update_img)
        self.slider_y.on_changed(self.update_img)


        fig.canvas.draw()
        self.current_shift = [(0,0)]*self.slices
        self.lock_sliders = False



    def update_img(self, val):
        if self.lock_sliders:
            return

        idx = self.ind
        dx = self.slider_x.val
        dy = self.slider_y.val
        self.current_shift[idx] = (dx, dy)

        new = ndimage.shift(self.reference[idx],
                            (dy, dx),
                            output=self.data[idx],
                            order=1,
                            mode='constant', cval=0.0)
        self.update()


    def update(self, props=False):
        if hasattr(self, "current_shift"):
            idx = self.ind
            dx, dy = self.current_shift[idx]
            self.lock_sliders = True
            self.slider_x.set_val(dx)
            self.slider_y.set_val(dy)
            self.lock_sliders = False
        super(Overlay, self).update(props)

    def _reset(self):
        self.xsliders[idx].reset()
        self.ysliders[idx].reset()


class CommonCursor(AxesWidget):
    """
        The same as the matplotlib cursor but being shown on several axes
    """
    def __init__(self, axes, horizOn=True, vertOn=True, useblit=False,
                 **lineprops):
        """
        Add a cursor to each axis *axes*.  If ``useblit=True``, use the
        backend-dependent blitting features for faster updates (GTKAgg
        only for now).  *lineprops* is a dictionary of line properties.

        .. plot :: mpl_examples/widgets/cursor.py
        """
        super(CommonCursor, self).__init__(axes[0])
        self.axes = axes
        self.connect_event('motion_notify_event', self.onmove)
        self.connect_event('draw_event', self.clear)

        self.visible = True
        self.horizOn = horizOn
        self.vertOn = vertOn
        self.useblit = useblit and self.canvas.supports_blit

        if self.useblit:
            lineprops['animated'] = True
        self.linesh = [ax.axhline(ax.get_ybound()[0], visible=False, **lineprops) for ax in axes]
        self.linesv = [ax.axvline(ax.get_xbound()[0], visible=False, **lineprops) for ax in axes]

        self.backgrounds = None
        self.needclear = False

    def clear(self, event):
        """clear the cursor"""
        if self.ignore(event):
            return
        if self.useblit:
            self.backgrounds = [self.canvas.copy_from_bbox(ax.bbox) for ax in self.axes]
        for i in range(len(self.axes)):
            self.linesv[i].set_visible(False)
            self.linesh[i].set_visible(False)

    def onmove(self, event):
        """on mouse motion draw the cursor if visible"""
        if self.ignore(event):
            return
        if not self.canvas.widgetlock.available(self):
            return
        if event.inaxes not in self.axes:
            for i in range(len(self.axes)):
                self.linesv[i].set_visible(False)
                self.linesh[i].set_visible(False)

            if self.needclear:
                self.canvas.draw()
                self.needclear = False
            return
        self.needclear = True
        if not self.visible:
            return
        for i in range(len(self.axes)):
            self.linesv[i].set_xdata((event.xdata, event.xdata))
            self.linesh[i].set_ydata((event.ydata, event.ydata))
            self.linesv[i].set_visible(self.visible and self.vertOn)
            self.linesh[i].set_visible(self.visible and self.horizOn)

        self._update()

    def _update(self):

        if self.useblit:
            for i, ax in enumerate(self.axes):
                if self.backgrounds is not None:
                    self.canvas.restore_region(self.backgrounds[i])
                ax.draw_artist(self.linesv[i])
                ax.draw_artist(self.linesh[i])
                self.canvas.blit(ax.bbox)
        else:

            self.canvas.draw_idle()

        return False


class Match2d(Figure):
    def __init__(self, n_imgs, maxshift=10, *args, **kwargs):
        """
            This widget allows to align up to three 2D-datasets f(x,y).
            The offset can be corrected using Sliders in both directions.
            The independent x,y will be correctly processed during
            resampling.

            Inputs:
                n_imgs : int
                    number of datasets (more than could be possible)

                maxshift : float
                    maximum shift of each slider in pixels

                *args and **kwargs for the pyplot.Figure class
        """
        Figure.__init__(self, *args, **kwargs)
        #self.canvas = FigureCanvasGTKAgg(self)
        self._maxshift = maxshift
        self._gs = gs = gridspec.GridSpec(4, n_imgs+1, height_ratios=[7,2,1,1], hspace=0.5, wspace=0.5)
        self._n = n_imgs

        sp_kwarg = dict(aspect="equal")
        for i in range(n_imgs):
            _ax = self.add_subplot(gs[i], **sp_kwarg)
            if not i:
                sp_kwarg["sharex"] = _ax
                sp_kwarg["sharey"] = _ax
                _ax.set_title("master")
            else:
                _ax.set_title("slave")
            _ax.autoscale_view(True)
        _ax = self.add_subplot(gs[self._n], **sp_kwarg)
        #_ax.autoscale(False)

        self.xsliders = {}
        self.ysliders = {}
        self.buttons = {}
        self._reference = {}
        self._resampled = {}
        self._args = collections.defaultdict(list)
        self._kwargs = collections.defaultdict(dict)
        #plt.draw()


    def addResetButton(self):
        _ax = self.add_subplot(self._gs[-1])
        self.button = Button(_ax, 'Reset', hovercolor='0.975')
        self.button.on_clicked(self._reset)


    def plot2d(self, X, Y, Z, idx=None, *args, **kwargs):
        if idx is None:
            idx = min(max([-1]+list(self._reference))+1, self._n-1)
        ax = self.axes[idx]
        if len(ax.collections):
            raise ValueError("Already found plot data in axis %i"%idx)

        im = ax.pcolormesh(X, Y, Z, *args, **kwargs)
        self._args[idx] = args
        self._kwargs[idx] = kwargs
        #plt.colorbar(im)
        self._reference[idx] = (X, Y, Z)

        ax_btn = self.add_subplot(self._gs[1*(self._n+1)+idx])
        ax_sx =  self.add_subplot(self._gs[2*(self._n+1)+idx])
        ax_sy =  self.add_subplot(self._gs[3*(self._n+1)+idx])
        #print ax_sx, ax_sy

        button = Button(ax_btn, 'Reset', hovercolor='0.975')
        self.buttons[idx] = button
        sx = Slider(ax_sx, "dx", -self._maxshift, self._maxshift, valinit=0)
        sy = Slider(ax_sy, "dy", -self._maxshift, self._maxshift, valinit=0)

        button.on_clicked(lambda val: self._reset(idx))
        sx.on_changed(lambda val: self.update_plot(idx))
        sy.on_changed(lambda val: self.update_plot(idx))
        self.xsliders[idx] = sx
        self.ysliders[idx] = sy
        self.update_plot()


    def addCursor(self):
        self.cursor = CommonCursor(self.axes[:self._n])

    def get_shift(self, idx):
        _x, _y, _ = self._reference[idx]
        dx = self.xsliders[idx].val
        dy = self.ysliders[idx].val
        return dx, dy


    def interpolate(self, idx, data=None):
        _x, _y, _data = self._reference[idx]
        if data is not None:
            _data = data
        _xy = np.vstack((_x.ravel(), _y.ravel())).T
        if 0 in self._reference: # has master
            _x_m, _y_m, _ = self._reference[0]
            _xy_m = np.stack((_x_m.ravel(), _y_m.ravel())).T
        else:
            _x_m, _y_m = _x, _y
            _xy_m = _xy.copy()

        dx, dy = self.get_shift(idx)
        _xy += np.array((dx, dy))
        #_xy[:,0] += dx
        #_xy[:,1] += dy
        #print dx, dy, [arr.shape for arr in (_xy, _data, _xy_m)]
        #print _x.shape, _y.shape, _data.shape
        resampled = interpolate.griddata(_xy,
                                         _data.ravel(),
                                         _xy_m,
                                         method='linear',
                                         fill_value = np.nan)#, rescale=False)
        #print idx, resampled.shape
        resampled = resampled.reshape(_x_m.shape)
        return _x_m, _y_m, resampled
        

    def update_plot(self, idx=None):
        if idx is None:
            idx = range(self._n)
        else:
            idx = [idx]
        for i in idx:
            ax = self.axes[i]
            if not i in self._reference:
                continue
            #print i, self._reference.keys()

            _x_m, _y_m, resampled = self.interpolate(i)

            coll = ax.collections[0]
            newA = resampled[:-1,:-1].ravel()
            #print i, newA.shape, idx, newA.min(), newA.max()
            kwargs = self._kwargs[i]
            args = self._args[i]
            if not "vmax" in kwargs:
                kwargs["vmax"] = np.nanmax(resampled)
            if not "vmin" in kwargs:
                kwargs["vmin"] = np.nanmin(resampled)
            if coll._coordinates[...,0].shape != _x_m.shape:
                #print "Redrawing %i"%i
                coll.remove()
                ax.pcolormesh(_x_m, _y_m, resampled, *args, **kwargs)
            else:
                coll.set_array(newA)
                coll._coordinates = np.array((_x_m, _y_m)).transpose(1,2,0)

        if 0 in self._reference:
            ax = self.axes[3]
            _x_m, _y_m, _z_m = self._reference[0]
            allD = []
            for i in range(self._n):
                coll = self.axes[i].collections
                if coll:
                    A = coll[0].get_array()
                    vmin, vmax = coll[0].get_clim()
                    norm = colors.Normalize(vmin, vmax)
                    A = norm(np.clip(A, vmin, vmax))
                    allD.append(A)
                    #allD[-1] = allD[-1] / allD[-1].max()
                else:
                    allD.append(np.zeros_like(newA))
            #print([d.shape for d in allD], allD)

            if not ax.collections:
                ax.pcolormesh(_x_m, _y_m, _z_m)#, *args, **kwargs)
            coll = ax.collections[0]
            coll.set_array(None)
            coll.set_color(np.array(allD).T)


        self.canvas.draw()

    def _reset(self, idx):
        self.xsliders[idx].reset()
        self.ysliders[idx].reset()




class LineCut:
    '''
        Allows to take arbitrary line cuts from pcolormesh plots, plotting the
        result into a separate axis and taking into account the independent axes.

    Example
    -------
    fig, (ax1, ax2) = plt.subplots( nrows=2 )    # one figure, two axes
    img = ax1.pcolormesh( x, y, Z )     # pcolormesh on the 1st axis
    lntr = LineCut( img, ax2 )        # Connect the handler, plot LineCut onto 2nd axis
    
    lntr.linecut # contains the data of the most resent cut

    Arguments
    ---------
    img: the pcolormesh plot to extract data from and that the User's clicks will be recorded for.
    ax2: the axis on which to plot the data values from the dragged line.
    integrate_width: number of pixels to average over perpendicular to the line cut


    '''
    def __init__(self, img, ax, integrate_width=1):
        '''
        img: the pcolormesh instance to get data from/that user should click on
        ax: the axis to plot the line slice on
        '''
        self.img = img
        self.ax = ax
        # self.data = img.get_array().reshape(img._meshWidth, img._meshHeight)
        self.data = img.get_array().reshape(img._meshHeight, img._meshWidth)
        self.coords = img._coordinates

        # register the event handlers:
        self.cidclick = img.figure.canvas.mpl_connect('button_press_event', self)
        self.cidrelease = img.figure.canvas.mpl_connect('button_release_event', self)

        self.markers, self.arrow = None, None   # the lineslice indicators on the pcolormesh plot
        self.line = None    # the lineslice values plotted in a line
        self.box = None
        self.linecut = None
        self.integrate_width = integrate_width

        
    def __call__(self, event):
        '''Matplotlib will run this function whenever the user triggers an event on our figure'''
        if event.inaxes != self.img.axes:
            return     # exit if clicks weren't within the `img` axes
        if self.img.figure.canvas.manager.toolbar._active is not None:
            return   # exit if pyplot toolbar (zooming etc.) is active

        if event.name == 'button_press_event':
            self.p1 =  (event.xdata, event.ydata)    # save 1st point
        elif event.name == 'button_release_event':
            self.p2 = (event.xdata, event.ydata)    # save 2nd point
            self.drawLineCut()    # draw the Line Slice position & data

    def drawLineCut( self ):
        ''' Draw the region along which the Line Slice will be extracted, 
            onto the original self.img pcolormesh plot.  
            Also update the self.axis plot to show the line slice data.
        '''
        '''Uses code from these hints:
        http://stackoverflow.com/questions/7878398/how-to-extract-an-arbitrary-line-of-values-from-a-numpy-array
        http://stackoverflow.com/questions/34840366/matplotlib-pcolor-get-array-returns-flattened-array-how-to-get-2d-data-ba
        '''

        x0,y0 = self.p1[0], self.p1[1]  # get user's selected coordinates
        x1,y1 = self.p2[0], self.p2[1]
        
        
        i1, j1 = np.unravel_index(norm(self.coords - self.p1, axis=2).argmin(), self.coords.shape[:2])
        i2, j2 = np.unravel_index(norm(self.coords - self.p2, axis=2).argmin(), self.coords.shape[:2])
        
        
        length = int(np.hypot(i2-i1, j2-j1))
        cols, rows = np.linspace(i1, i2, length),   np.linspace(j1, j2, length)
        x = np.linspace(x0, x1, length)
        y = np.linspace(y0, y1, length)
        
        if abs(x0-x1) > abs(y0-y1):
            xplot = x
            xlabel = self.img.axes.get_xlabel()
        else:
            xplot = y
            xlabel = self.img.axes.get_ylabel()

        # Extract the values along the line with nearest-neighbor pixel value:
        # get temp. data from the pcolor plot
        if self.integrate_width > 1:
            zi = np.zeros(len(cols))
            # vec_par  = np.array((i2-i1, j2-j1), dtype=float)
            self.ij = i1, j1 = int(i1), int(j1)
            self.Qtrafo = (self.coords[[i1+1,i1],[j1,j1+1]] - self.coords[i1,j1]).T
            Qtrafo_inv = np.linalg.inv(self.Qtrafo)
            
            vec_par  = np.array((x1-x0, y1-y0), dtype=float)
            vec_perp = np.array([[0,-1], [1,0]]).dot(vec_par)
            vec_perp = Qtrafo_inv.dot(vec_perp)
            vec_perp /=np.linalg.norm(vec_perp)

            w = self.integrate_width
            for i in np.linspace(-w/2, w/2, 2*w-1):
                _cols = cols + vec_perp[0]*i
                _rows = rows + vec_perp[1]*i
                zi += self.data[_cols.round().astype(np.int), _rows.round().astype(np.int)]
            zi /= 2*w-1
            
            col_min = cols - vec_perp[0]*w/2
            col_max = cols + vec_perp[0]*w/2
            row_min = rows - vec_perp[1]*w/2
            row_max = rows + vec_perp[1]*w/2
            x0min, x1min = col_min[[0,-1]].round().astype(int)
            x0max, x1max = col_max[[0,-1]].round().astype(int)
            y0min, y1min = row_min[[0,-1]].round().astype(int)
            y0max, y1max = row_max[[0,-1]].round().astype(int)
        else:
            zi = self.data[cols.round().astype(np.int), rows.round().astype(np.int)]
        # Extract the values along the line, using cubic interpolation:
        #import scipy.ndimage
        #zi = scipy.ndimage.map_coordinates(self.data, np.vstack((x,y)))
        
        self.linecut = np.array((x, y, zi)).T # this allows to access the result

        # if plots exist, delete them:
        if self.markers != None:
            if isinstance(self.markers, list):
                self.markers[0].remove()
            else:
                self.markers.remove()
        if self.arrow != None:
            self.arrow.remove()

        # plot the endpoints
        self.markers = self.img.axes.plot([x0, x1], [y0, y1], 'wo')   
        # plot an arrow:
        self.arrow = self.img.axes.annotate("",
                    xy=(x0, y0),    # start point
                    xycoords='data',
                    xytext=(x1, y1),    # end point
                    textcoords='data',
                    arrowprops=dict(
                        arrowstyle="<-",
                        connectionstyle="arc3", 
                        color='white',
                        alpha=0.7,
                        linewidth=2
                        ),

                    )
        if self.integrate_width > 1:
            if self.box is not None:
                self.box.remove()
            self.boxcoords = [self.coords[x0min, y0min],
                   self.coords[x0max, y0max],
                   self.coords[x1max, y1max],
                   self.coords[x1min, y1min],
                   ]
            self.box = Polygon(self.boxcoords, color="w", edgecolor=None, alpha=0.25)
            self.img.axes.add_patch(self.box)
        # plot the data along the line on provided `ax`:
        if self.line != None:
            self.line[0].remove()   # delete the plot
        self.line = self.ax.plot(xplot, zi)
        self.ax.relim()
        self.ax.autoscale_view()
        self.ax.set_xlabel(xlabel)


