####################################################
#  general tolls that do not have a home yet
####################################################

from time import sleep
import sys, os
import numpy as np
import scipy.fftpack as sf
#import tifffile as tiff
#import CXDFile as cxdf
from PIL import Image
import h5py as h5
import pdb
#import scipy.ndimage as spyndim
#from skimage.feature import register_translation
#from scipy.ndimage.fourier import fourier_shift
from id01lib.process.ImageRegistration import *
from scipy.stats import pearsonr

import matplotlib
import matplotlib.pyplot as plt
import matplotlib.animation as manimation
from matplotlib.colors import LogNorm



# ---------------------
# from PythonPhasing.Tools
# shift an n-dimensional array with sub pixel precision
# ---------------------
def Shift(arr, Shifty):
    # pass the FT of the fftshifted array you want to shift
    # you get back the actual array, not the FT.
    dims = arr.shape
    if len(dims) < len(Shifty):
      raise Exception("Shift past must have same number of dimensions as the array")
    rootN = arr.size**0.5
    # scipy does normalized ffts!
    ftarr = sf.fftn(arr) #* rootN
    r=[]
    for d in dims:
      r.append(slice(int(np.ceil(-d/2.)), int(np.ceil(d/2.)), None))
    idxgrid = np.mgrid[r]
    for d in range(len(dims)):
      ftarr *= np.exp(-1j*2*np.pi*Shifty[d]*sf.fftshift(idxgrid[d])/float(dims[d]))
    shiftedarr = sf.ifftn(ftarr) #* rootN
    return shiftedarr

    
def addDetectorGaps(a): #,ROI=[0,a.shape[0],0,a.shape[1],0,a.shape[2]]):
    if a.shape[-1]==512:
        if len(a.shape)==3:
            b = np.zeros((a.shape[0],517,516),float)
            b[:,:256,:256] = a[:,:256,:256] #Quad top left unchanged
            b[:,:256,260:] = a[:,:256,256:] #Quad top right moved 4 right
            b[:,261:,:256] = a[:,256:,:256] #Quad bot left moved 6 down
            b[:,261:,260:] = a[:,256:,256:] #Quad bot right
        elif len(a.shape)==2:
            b = np.zeros((517,516),float)
            b[:256,:256] = a[:256,:256] #Quad top left unchanged
            b[:256,260:] = a[:256,256:] #Quad top right moved 4 right
            b[261:,:256] = a[256:,:256] #Quad bot left moved 6 down
            b[261:,260:] = a[256:,256:] #Quad bot right
        return b
    else:
        return a
# ---------------------
# Combine scans borrowed from APS34IDC and cleaned up
# ---------------------
def ConcatenateDatasets(darktifpath,whitefieldpath,h5filename,h5datapath,scanrange,min_threshold_data,pcoeffthreshold=0.95,tau=2.0e-6,expose=1.0,dodead=False):
    refcrop, N = None, 0
    reject_list =[]

    with h5.File(h5filename,'r') as h5file:
        scannos = h5file[h5datapath.split('%')[0]].keys()
        print("###################\ndatasets to be analysed:")
        for i in scanrange:
            if scannos.count("%i.1"%i)==0:
                print(i, "..does not exist ...")
                sys.exit()
            print(i)
        print("###################")                
    #Convert to float when dividing out the whitefield
    print('whitefield: %s'%whitefieldpath)
    whitefieldImage = np.array(Image.open(whitefieldpath))
    white = whitefieldImage.astype('float32') #[s1,s2] #For fourth quadrant only
    normwhite = white/np.mean(white[white>0]) #normalise to the mean until the raw images are available.
    normwhite = addDetectorGaps(normwhite)
    normwhite[normwhite==0]  = 1e20 # nan/inf killer

    for i in scanrange:
        print("Processing Scan: ",i)
        with h5.File(h5filename,'r') as h5file:
            darray = h5file[h5datapath%i][()].astype('float32')
            darray = addDetectorGaps(darray)
            darray[darray<min_threshold_data] = 0
            for j in range(darray.shape[0]): 
                darray[j]/=normwhite
            #darray*=1e5 #Some medium value
            darray[np.isnan(darray)] = 0 

            #Remove hot pixels from data
            #print "Removing hot pixels"
            #print "Data shape is", darray.shape
            darkImage = np.array(Image.open(darktifpath))
            darkImage = addDetectorGaps(darkImage)

            for j in range(darray.shape[0]):
                darray[j,:,:][darkImage>5]==0.0
                #darray[j,:,:] = np.where(darkImage>5,0,darray[j,:,:]) #Ignore cosmic rays
             
            #print "Max in array after dark field correction", np.where(darray==darray.max())

            #Correct for dead time
            #Formula is Ncorrect=Nmeas/(1-tau*Nmeas)
            if(dodead):
                print("Correcting for dead time!!!")
                print("Tau and exposure time are:", tau, expose)
                darray/=expose
                darray=darray/(1-tau*darray)

            # set first data file as reference
            if refcrop is None:
                sum = np.abs(darray.copy())
                refcrop = sf.fftn(darray)
                sumCC = np.zeros(darray.shape)
                refCC = np.abs(darray.copy())
                continue

            # aligns each file to the reference using the peak of cross correlation.
            
            """
            #get shift and do shift
            crop=sf.fftn(darray)
            cropsize=crop.shape
            #  print "cropsize", cropsize

            # get cross correlation and pixel shift
            CC = sf.ifftn(refcrop*np.conj(crop))
            c=np.array(CC.shape)
            amp = np.abs(CC)
            maxcc = amp.max()
            rfzero = np.sum(np.abs(refcrop)**2)/crop.size
            rgzero = np.sum(np.abs(crop)**2)/crop.size
            err = (abs(1.-maxcc**2/(rgzero*rgzero)))**0.5
            intshift = np.unravel_index(amp.argmax(),c) 
            s=np.array(intshift)
            pixelshift = np.where(s>=c/2, s-c, s)
            print("   pixelshift: %s %s"%(err,pixelshift))
            """
            # skimage is only 2D 
            #shift, error, diffphase = register_translation(refCC, darray, upsample_factor=100)
            #print("Detected pixel offset [y,x]: [%g, %g]" % (shift[0], shift[1]))
            #offset_darray = np.fft.ifftn(fourier_shift(np.fft.fftn(darray), shift))
            CCshift = GetImageRegistration(refCC, darray, 100)# this does 3D in 2D steps so manual correlation coeff

            # pearsonr on 10000 brightest pixels
            intLimit = np.sort(refCC.flatten())[-10000]
            shifteddarray = Shift(darray,CCshift)
            pcoeff = pearsonr(refCC[refCC>intLimit].flatten(),
                                np.abs(shifteddarray[refCC>intLimit].flatten()))
            print("...Pearson Correlation Coeff: ", pcoeff[0])
            
            da=darray/darray.sum()
            refa=refCC/refCC.sum()
            
            #N2=np.sum(da*refa)/np.sum(refa*refa)
            #print("...N2 Coeff: ", N2)

            if pcoeff[0]>pcoeffthreshold:
                sum = sum + shifteddarray
            else:
                reject_list.append(i)
                print("......rejected <%.2f"%(pcoeffthreshold))
                
    if refcrop is None:
        print(rootpath+'/'+IDstring+'*')
        print('No scans to import')
    
    print("SUMMARY:")
    print("...No scans accepted %i/%i"%(len(scanrange)-len(reject_list),len(scanrange)))
    print("...Scans rejected :",reject_list)
    print("...Total photons counted: ", np.sum(sum))
    
    return sum, reject_list
    
    
def ConcatenateDatasetsBlind(darktifpath,whitefieldpath,h5filename,h5datapath,scanrange,min_threshold_data,pcoeffthreshold=0.95,tau=2.0e-6,expose=1.0,dodead=False):
    with h5.File(h5filename,'r') as h5file:
        scannos = h5file[h5datapath.split('%')[0]].keys()
        print("###################\ndatasets to be analysed:")
        for i in scanrange:
            if scannos.count("%i.1"%i)==0:
                print(i, "..does not exist ...")
                sys.exit()
            print(i)
        print("###################")                
    #Convert to float when dividing out the whitefield
    print('whitefield: %s'%whitefieldpath)
    whitefieldImage = np.array(Image.open(whitefieldpath))
    white = whitefieldImage.astype('float32') #[s1,s2] #For fourth quadrant only
    normwhite = white/np.mean(white[white>0]) #normalise to the mean until the raw images are available.
    normwhite = addDetectorGaps(normwhite)
    normwhite[normwhite==0]  = 1e20 # nan/inf killer

    fidelity_dict = dict()
    for ii in scanrange:
        
        fidelity_dict[ii]=0
        refcrop, N = None, 0
        reject_list =[]
        print("###################\nReference Scan: ",ii)
        for i in scanrange:
            print("Processing Scan: ",i)
            with h5.File(h5filename,'r') as h5file:
                darray = h5file[h5datapath%i][()].astype('float32')
                darray = addDetectorGaps(darray)
                #check the data shape is the same
                # TODO: check if the data could be binned to match
                if refcrop is not None and refdarray.shape!=darray.shape:
                    reject_list.append(i)
                    print("......rejected wrong shape ",refcrop.shape,darray.shape)
                    continue 
                darray[darray<min_threshold_data] = 0
                for j in range(darray.shape[0]): 
                    darray[j]/=normwhite
                #darray*=1e5 #Some medium value
                darray[np.isnan(darray)] = 0 

                #Remove hot pixels from data
                #print "Removing hot pixels"
                #print "Data shape is", darray.shape
                darkImage = np.array(Image.open(darktifpath))
                darkImage = addDetectorGaps(darkImage)

                for j in range(darray.shape[0]):
                    darray[j,:,:][darkImage>5]==0.0
                    #darray[j,:,:] = np.where(darkImage>5,0,darray[j,:,:]) #Ignore cosmic rays
                 
                #print "Max in array after dark field correction", np.where(darray==darray.max())

                #Correct for dead time
                #Formula is Ncorrect=Nmeas/(1-tau*Nmeas)
                if(dodead):
                    print("Correcting for dead time!!!")
                    print("Tau and exposure time are:", tau, expose)
                    darray/=expose
                    darray=darray/(1-tau*darray)

                # set first data file as reference
                if refcrop is None:
                    refdarray = h5file[h5datapath%ii][()].astype('float32')
                    refdarray = addDetectorGaps(refdarray)
                    sum = np.abs(refdarray.copy())
                    refcrop = sf.fftn(refdarray)
                    sumCC = np.zeros(refdarray.shape)
                    refCC = np.abs(refdarray.copy())
                    refshape = refdarray.shape
                    continue

                # aligns each file to the reference using the peak of cross correlation.
                
                """
                #get shift and do shift
                crop=sf.fftn(darray)
                cropsize=crop.shape
                #  print "cropsize", cropsize

                # get cross correlation and pixel shift
                CC = sf.ifftn(refcrop*np.conj(crop))
                c=np.array(CC.shape)
                amp = np.abs(CC)
                maxcc = amp.max()
                rfzero = np.sum(np.abs(refcrop)**2)/crop.size
                rgzero = np.sum(np.abs(crop)**2)/crop.size
                err = (abs(1.-maxcc**2/(rgzero*rgzero)))**0.5
                intshift = np.unravel_index(amp.argmax(),c) 
                s=np.array(intshift)
                pixelshift = np.where(s>=c/2, s-c, s)
                print("   pixelshift: %s %s"%(err,pixelshift))
                """
                # skimage is only 2D 
                #shift, error, diffphase = register_translation(refCC, darray, upsample_factor=100)
                #print("Detected pixel offset [y,x]: [%g, %g]" % (shift[0], shift[1]))
                #offset_darray = np.fft.ifftn(fourier_shift(np.fft.fftn(darray), shift))
                CCshift = GetImageRegistration(refCC, darray, 100)# this does 3D in 2D steps so manual correlation coeff

                # pearsonr on 10000 brightest pixels
                intLimit = np.sort(refCC.flatten())[-10000]
                shifteddarray = Shift(darray,CCshift)
                pcoeff = pearsonr(refCC[refCC>intLimit].flatten(),
                                    np.abs(shifteddarray[refCC>intLimit].flatten()))
                print("...Pearson Correlation Coeff: ", pcoeff[0])
                
                da=darray/darray.sum()
                refa=refCC/refCC.sum()
                
                #N2=np.sum(da*refa)/np.sum(refa*refa)
                #print("...N2 Coeff: ", N2)

                if pcoeff[0]>pcoeffthreshold:
                    sum = sum + shifteddarray
                else:
                    reject_list.append(i)
                    fidelity_dict[ii]+=1
                    print("......rejected <%.2f"%(pcoeffthreshold))
                    
        fidelity_dict["%i_sum"%ii]=sum
        fidelity_dict["%i_rejectlist"%ii]=reject_list

    #pick the winner
    key,value = max(zip(fidelity_dict.keys(),fidelity_dict.values()))
    
    if refcrop is None:
        print(rootpath+'/'+IDstring+'*')
        print('No scans to import')
    
    print("SUMMARY:")
    print("...No scans accepted %i/%i"%(len(scanrange)-len(fidelity_dict["%s_rejectlist"%key.split('_')[0]]),len(scanrange)))
    print("...Scans rejected :",fidelity_dict["%s_rejectlist"%key.split('_')[0]])
    print("...Total photons counted: ", np.sum(fidelity_dict[key]))
    
    return fidelity_dict
    
def make_mp4(array,dim=0,fps=10,comment=None,path='.',fn='test.mp4',figsize=(5,5),dpi=100):
	"""
	Make an mp4 of a 3D array, slicing along the desired dimension
    
	array - 3D numpy array
	dim - 0,1,2
	fps - frames per second
	comment - mp4 metadata
	path - output directory #TODO:(will generate)
	fn - output filename
    
	NB: if you want a gif (ffmpeg -i fn.mp4 fn.gif)
	"""
	
	mpl_backend = matplotlib.get_backend()
	matplotlib.use("Agg")#, warn=False)
    
	FFMpegWriter = manimation.writers['ffmpeg']

	writer = FFMpegWriter(fps=fps, metadata=comment)
	fontsize = 12

	os.makedirs(path, exist_ok=True)
	filename = os.path.join(path, "scan-movie.mp4")

	fig = plt.figure(figsize=figsize)

	sys.stdout.write("Generating movie frames...")
	sys.stdout.flush()
	filename = os.path.join(path,fn)
	with writer.saving(fig, filename, dpi=dpi):
		for i in range(array.shape[dim]):
			plt.clf()
			plt.subplot(111)
			if dim ==0:
				plt.imshow(array[i,:,:],norm=LogNorm())
			if dim ==1:
				plt.imshow(array[:,i,:],norm=LogNorm())
			if dim ==2:
				plt.imshow(array[:,:,i],norm=LogNorm())
			plt.title("idx: %i"%i)

			writer.grab_frame()
	print('\nSaved movie to: %s' % filename)
	matplotlib.use(mpl_backend)#, warn=False)

if __name__ == '__main__':

    darktifpath = "dark.tif"
    whitefieldpath = "CelaWhiteField.tif"
    h5filename = "rawdata.h5"
    h5datapath = "/C5/%i.1/measurement/image_0/data"
    #scanrange = [1703,1706] #,1709,1712,1715]
    scanranges = [[167, 180, 364, 361, 376, 394, 397, 367, 370, 373, 379, 382, 385, 388, 391, 1146, 1152, 1149, 1143, 1140],
    [1900, 1894, 1906, 1909, 1903, 1897],
    [1542, 1706, 1730, 1703, 1611, 1712, 1745, 1709, 1724, 1721, 1736, 1715, 1733, 1739, 1742, 1727, 1718, 1548, 1554, 1551, 1545, 1557],
    [1134, 1131, 1128, 1125, 1122],
    [1605, 1518, 1515, 1533, 1527, 1524, 1521, 1530, 1536],
    [1362, 1374, 1116, 1113, 1371, 1368, 1365, 1110, 1107, 1104, 535, 722, 713, 728, 725, 734, 743, 740, 719, 716, 731, 746, 737],
    [1509, 1494, 1506, 1874, 1503, 1497, 1491, 1500, 1488, 1599, 1886, 1889, 1877, 1880, 1883, 1171, 1168, 1165, 854, 437, 434, 416, 407, 425, 440, 413, 419, 1077, 431, 422, 443, 446, 216, 428, 404, 1074, 1068, 1071, 1080, 1162, 842, 517, 848, 851, 520, 523, 845, 547, 538, 544, 541, 550, 1098, 1158, 1174, 1086, 1089, 1095, 1092],
    [1652, 1655, 1658, 1661, 1664],
    [1461, 637, 1479, 1470, 1482, 1467, 1593, 1476, 1473, 1464, 1050, 1062, 1056, 1059, 640, 1053, 643, 646, 661, 667, 658, 664, 652, 655, 649],
    [1854, 1317, 1866, 1035, 1041, 1857, 1863, 1860, 1314, 1869, 1038, 1032, 1323, 1044, 1320, 1326, 302, 198, 308, 311, 329, 335, 305, 314, 317, 320, 323, 326, 332],
    [1846, 1843, 1849, 1840, 1837, 1834, 1359, 1356, 1353, 1350, 1347, 1026, 1017, 1020, 1023, 1185, 1188, 1191, 1194, 1197, 1200, 1203, 1206, 1209, 1212, 1215, 1218, 1014, 577, 586, 580, 589, 562, 571, 574, 583, 526, 559, 565, 568, 556],
    [37, 52, 55, 58, 61, 64, 67, 70, 73, 76, 79, 82, 85],
    [1440, 1443, 1446, 1452, 1449, 1455, 1434, 1008, 999, 1437, 1002, 996, 1587, 1005, 225],
    [752, 785, 758, 761, 776, 782, 755, 764, 767, 770, 773, 779, 990, 981, 984, 987, 1829, 978, 1820, 1823, 1826, 1817, 1305, 1296, 1299, 1308, 1814, 1302],
    [1685, 1688],
    [253, 488, 259, 265, 268, 277, 286, 289, 292, 1332, 491, 1281, 250, 256, 262, 271, 274, 280, 283, 972, 1335, 247, 151, 157, 963, 154, 160, 966, 969, 1278, 1287, 1338, 1284, 1800, 1803, 1809, 1806, 1341, 1290, 1797, 960, 1794, 1259],
    [942, 945, 954, 948, 951],
    [1422, 1581, 1416, 821, 827, 1425, 1413, 833, 830, 1428, 1419, 824, 836],
    [674, 695, 532, 692, 698, 707, 680, 704, 686, 701, 677, 689, 683, 933, 927, 930, 936, 924, 232, 1789, 1780, 1569, 1783, 1774, 1777, 1786, 1575, 1386, 1383, 1401, 1392, 1398, 1389, 1395, 1380],
    [607, 610, 595, 529, 604, 613, 897, 622, 625, 616, 619, 598, 915, 906, 628, 601, 1754, 912, 909, 918, 1760, 1763, 1769, 1766, 1757],
    [1697, 1616, 1622, 1625, 1628, 1631, 1634, 1637, 1640, 1643, 1646, 1649, 1694, 1700],
    ] # all datasets - to be used with ConcatenateDatasetsBlind < needs speeding up...

    #manually define each reference dataset
    scanrange = [391, 167, 180, 364, 361, 376, 394, 397, 367, 370, 373, 379, 382, 385, 388, 391]
    scanrange = [1146, 1152, 1149, 1143, 1140]
    scanrange = [1152, 1149, 1143, 1140] # 0.9 threshold

    """
    [1900, 1894, 1906, 1909, 1903, 1897],
    [1706, 1542, 1730, 1703, 1611, 1712, 1745, 1709, 1724, 1721, 1736, 1715, 1733, 1739, 1742, 1727, 1718, 1548, 1554, 1551, 1545, 1557],
    [1548, 1554, 1551, 1545, 1557,1542,1545],
    [1134, 1131, 1128, 1125, 1122],
    [1518, 1515, 1533, 1527, 1524, 1521, 1530, 1536, 1605],
    [1362, 1374, 1371, 1368, 1365, 1110, 1107, 1104, 1116, 1113]
    [1110, 1107, 1104, 1116, 1113]
    [722, 713, 728, 725, 734, 743, 740, 719, 716, 731, 746, 737,535,]

    [1509, 1494, 1506, 1874, 1503, 1497, 1491, 1500, 1488, 1599, 1886, 1889, 1877, 1880, 1883, 1171, 1168, 1165, 854, 437, 434, 416, 407, 425, 440, 413, 419, 1077, 431, 422, 443, 446, 216, 428, 404, 1074, 1068, 1071, 1080, 1162, 842, 517, 848, 851, 520, 523, 845, 547, 538, 544, 541, 550, 1098, 1158, 1174, 1086, 1089, 1095, 1092],
    [1652, 1655, 1658, 1661, 1664],
    [1461, 637, 1479, 1470, 1482, 1467, 1593, 1476, 1473, 1464, 1050, 1062, 1056, 1059, 640, 1053, 643, 646, 661, 667, 658, 664, 652, 655, 649],
    [1854, 1317, 1866, 1035, 1041, 1857, 1863, 1860, 1314, 1869, 1038, 1032, 1323, 1044, 1320, 1326, 302, 198, 308, 311, 329, 335, 305, 314, 317, 320, 323, 326, 332],
    [1846, 1843, 1849, 1840, 1837, 1834, 1359, 1356, 1353, 1350, 1347, 1026, 1017, 1020, 1023, 1185, 1188, 1191, 1194, 1197, 1200, 1203, 1206, 1209, 1212, 1215, 1218, 1014, 577, 586, 580, 589, 562, 571, 574, 583, 526, 559, 565, 568, 556],
    [37, 52, 55, 58, 61, 64, 67, 70, 73, 76, 79, 82, 85],
    [1440, 1443, 1446, 1452, 1449, 1455, 1434, 1008, 999, 1437, 1002, 996, 1587, 1005, 225],
    [752, 785, 758, 761, 776, 782, 755, 764, 767, 770, 773, 779, 990, 981, 984, 987, 1829, 978, 1820, 1823, 1826, 1817, 1305, 1296, 1299, 1308, 1814, 1302],
    [1685, 1688],
    [253, 488, 259, 265, 268, 277, 286, 289, 292, 1332, 491, 1281, 250, 256, 262, 271, 274, 280, 283, 972, 1335, 247, 151, 157, 963, 154, 160, 966, 969, 1278, 1287, 1338, 1284, 1800, 1803, 1809, 1806, 1341, 1290, 1797, 960, 1794, 1259],
    [942, 945, 954, 948, 951],
    [1422, 1581, 1416, 821, 827, 1425, 1413, 833, 830, 1428, 1419, 824, 836],
    [674, 695, 532, 692, 698, 707, 680, 704, 686, 701, 677, 689, 683, 933, 927, 930, 936, 924, 232, 1789, 1780, 1569, 1783, 1774, 1777, 1786, 1575, 1386, 1383, 1401, 1392, 1398, 1389, 1395, 1380],
    [607, 610, 595, 529, 604, 613, 897, 622, 625, 616, 619, 598, 915, 906, 628, 601, 1754, 912, 909, 918, 1760, 1763, 1769, 1766, 1757],
    [1697, 1616, 1622, 1625, 1628, 1631, 1634, 1637, 1640, 1643, 1646, 1649, 1694, 1700],
    ] 
    """
    #scanranges=[[942, 945]]# 954, 948, 951]]
    outfilename = "rawdata_concat1.h5"
    min_threshold_data = 1 
    pcoeffthreshold = 0.9


    concat, rejects = ConcatenateDatasets(darktifpath,
                                whitefieldpath,
                                h5filename,
                                h5datapath,
                                scanrange,
                                min_threshold_data, 
                                pcoeffthreshold=pcoeffthreshold, 
                                tau=2.0e-6,
                                expose=1.0,
                                dodead=False)

    with h5.File(outfilename, 'w') as outputfile:
        outputfile.create_dataset("/concat_%i_%i/"%(scanrange[0],scanrange[1]),data = concat,compression=6)
        outputfile["/concat_%i_%i_rejects"%(scanrange[0],scanrange[1])] = rejects
        outputfile["/concat_%i_%i_scans"%(scanrange[0],scanrange[1])] = scanrange
          
     

    # for ConcatenateDatasetsBlind
    """   
    for scanrange in scanranges:
        output = ConcatenateDatasetsBlind(darktifpath,
                                    whitefieldpath,
                                    h5filename,
                                    h5datapath,
                                    scanrange,
                                    min_threshold_data, 
                                    pcoeffthreshold=pcoeffthreshold, 
                                    tau=2.0e-6,
                                    expose=1.0,
                                    dodead=False)

        #pdb.set_trace()
        with h5.File(outfilename, 'w') as outputfile:
            for key in output.keys():
                #use compression!!!
                try:
                    if key.count("sum")>0:
                        outputfile.create_dataset("/concat_%i_%i/"%(scanrange[0],scanrange[1])+str(key),data = output[key],compression=6)
                    else:
                        outputfile["/concat_%i_%i/"%(scanrange[0],scanrange[1])+str(key)] = output[key]
                except:
                    outputfile["/concat_%i_%i/"%(scanrange[0],scanrange[1])+str(key)] = output[key]

    """
