#----------------------------------------------------------------------
# Description: 
#    wrapper for the SpecClient (python2) package which is optimized
#    for ID01
#
# SL - Library of functions for interaction with spec variables with python
#
# BEWARE: generally this is dangerous as it is run in the background 
# without specs knowledge
# 
# TODO: clean str() calls in all dependent files 
#   
# Author: Steven Leake <steven.leake@esrf.fr>
# Created at: 11. Jun 12:00:00 CET 2017
# Computer: 
# System: 
#
# TODO:
#  
#----------------------------------------------------------------------

SpecCommand = None
SpecVariable = None

try:
    from SpecClient_gevent import SpecVariable
    from SpecClient_gevent import SpecCommand
except ImportError:
    pass

try:
    from SpecClient import SpecVariable
    from SpecClient import SpecCommand
except ImportError:
    pass

if SpecCommand is None or SpecVariable is None:
    print("Warning: Could not import SpecClient module")

import numpy as np


class SpecClientSession(object):
    def __init__(self, sv_limaroi = 'LIMA_ROI',
                       sv_limadev = 'LIMA_DEV',
                       specname ='nano3:psic_nano',
                       verbose=True):
        self.sv_limaroi = sv_limaroi
        self.sv_limadev = sv_limadev
        self.specname = specname
        self.device=''
        self.varcache = dict()
        self.speccmd = SpecCommand.SpecCommand('',self.specname)
        self.verbose = verbose

    def get_sv(self, sv):
        #if sv not in self.varcache:
        #    _sv = SpecVariable.SpecVariable(sv,self.specname)
        #    self.varcache[sv] = _sv
        #else:
        #    _sv = self.varcache[sv]
        _sv = SpecVariable.SpecVariable(sv,self.specname)
        #if self.verbose:
        #    print('polling %s'%sv)
        return _sv.getValue()

    def send_sc(self,sc):
        return self.speccmd.executeCommand(str(sc))

    def set_sv(self, sv, sv_value):
        self.send_sc(sv+'='+str(sv_value))
        return self.get_sv(sv)

    def get_motor(self,mot_nm):
        _mot_no = int(self.get_sv(mot_nm))
        _mot_pos = float(self.get_sv("A[%i]"%_mot_no)['%i'%_mot_no])
        return _mot_pos

    def find_roi_list(self):
        '''
        Finds first active detector from list and populates a ROI_LIST
        '''
        # limaroi params
        _limaroi = self.get_sv(self.sv_limaroi) # TODO: Check if PSCAN_ROICOUNTER[] is better
        no_rois = int(_limaroi['0'])

        # limadevices params
        _limadev =  self.get_sv(self.sv_limadev)
        no_devs = int(_limadev['0'])

        # find rois for active detector
        self.devices = devices = []
        for ii in range(1,no_devs+1,1):
            name = _limadev['%i'%ii]
            dev = _limadev[name]
            if dev.get('active', False) == '1':
                devices.append(name)

        self.device = devices[0] # just takes the first active camera
        roi_list=[]
        for i in range(1,no_rois+1,1):
            name = _limaroi['%i'%i]
            roidict = _limaroi[name]
            if roidict['ccdname']=="%s"%self.device:
                roi_list.append(name)

        return roi_list, self.device


    def get_last_image(self, device):
        '''
            get last image from the detector
        '''
        _limadev =  self.get_sv(self.sv_limadev)
        return self.get_sv('image_data%i'%int(_limadev["%s"%device]["unit"]))

    def get_pscan_vars(self, sv="PSCAN_ARR", pscan_live=False):
        '''
        extract pscan params
        '''
        pscan_vars = self.get_sv(sv)
        cmd = pscan_vars["header/cmd"].split()

        # get motor names
        m1_nm = cmd[1]
        m2_nm = cmd[5]

        # motor start&end positions
        m1_se = map(float, cmd[2:4])
        m2_se = map(float, cmd[6:8])

        # get kmap column number
        piezo_counters = {'piy':'adcX','pix':'adcY','piz':'adcZ'}
        cols = pscan_vars['header/cols'].split()
        m1 = cols.index(piezo_counters[m1_nm])
        m2 = cols.index(piezo_counters[m2_nm])

        # find motor positions
        m1_pos = self.get_sv('pscan_countersdata%i'%m1)
        m2_pos = self.get_sv('pscan_countersdata%i'%m2)

        m1_pts = int(cmd[4])
        m2_pts = int(cmd[8])
        if pscan_live:
            return m1_nm, m1_se, m1_pos, m1_pts, m2_nm, m2_se, m2_pos, m2_pts, pscan_vars
        else:
            return m1_nm, m1_se, m1_pos, m2_nm, m2_se, m2_pos, pscan_vars



