#----------------------------------------------------------------------
# Description: 
#   generic multiple processor for 2D/3D cross correlations
#   
# Author: Steven Leake <steven.leake@esrf.fr>
# Created at: Fri 01. Apr 12:00:00 CET 2019
# Computer: 
# System: 
#
#  #TODO decouple image correction from this class - silly mistake
#  #TODO clean the name = main - DONE
#  # TODO make relevant example in examples directory - DONE
#  
#----------------------------------------------------------------------

import numpy as np
import matplotlib.pyplot as pl
import h5py as h5
import pylab as pl
import sys
import os
from multiprocessing import Process, Lock, Queue
#from multiprocessing import RawArray # could be used to reduce memory
import pdb
from id01lib.ImageRegistration import *
from scipy.stats import pearsonr
import operator
from id01lib.utils import Shift, addDetectorGaps
from PIL import Image



########################
# CLASS
########################

class CrossCorrelator():
    """
    class to execute cross correlation analysis using multiple processors
    """
    def __init__(self, 
                h5fn, 
                ref_array_h5path, 
                all_array_h5path, 
                ref_scanno, 
                all_scannos, 
                darktifpath = "dark.tif", 
                whitefieldtifpath = "CelaWhiteField.tif", 
                pearson_intlimit = 0.1, 
                roi=[[0, 966], [0, 1296]], 
                debug = False):
        """
        initialise the class with
        h5fn - raw data filename
        ref_array_h5path - reference data array h5 path to cross correlate against, 
                                                scannumber represented with a %i
        all_array_h5path - data array h5 path to cross correlate with ref, 
                                                scannumber represented with a %i
        ref_scanno - reference scan number
        all_scannos - all scan numbers to compare
        darktifpath - darkfield image path
        whitefieldtifpath - whitefield image path , 
        pearson_intlimit - number of brightest pixels to consider (*max)
                            for the pearson correlation coefficient,
        """
        self.h5fn = h5fn
        self.darktifpath = darktifpath
        self.whitefieldtifpath = whitefieldtifpath
        self.ref_scanno = ref_scanno
        self.all_scannos = all_scannos
        self.ref_array_h5path = ref_array_h5path%self.ref_scanno
        self.all_array_h5path = all_array_h5path
        
        self.all_array_h5paths = []
        for no in self.all_scannos:
            self.all_array_h5paths.append(all_array_h5path%no)

        self.set_ref_array()
        self.pearson_intlimit = pearson_intlimit
        self.output = np.zeros(0, dtype=[('x', 'f8'), 
                                        ('y', 'f8'), 
                                        ('z', 'f8'), 
                                        ('ref_scanno', 'i8'), 
                                        ('scanno', 'i8')])
        self.roi = roi
        self.debug = debug

    def CC_worker(self, ref_data, tmp_data, ref_scanno, tmp_scanno, dump):
        """
        worker function for multi_CC function
        """
        CCshift = GetImageRegistration(ref_data, tmp_data, 100) # this does 3D in 2D steps so manual correlation coeff

        # do the cross correlation
        #cc, error, diffphase = register_translation(ref_data,tmp_data, upsample_factor=100) # 2D only
        x,y,z = CCshift
        if self.debug:
            sys.stdout.write(x,y,z)
        sys.stdout.write('.')
        
        tmp_data = Shift(tmp_data,CCshift)
        #pearson_coeff = pearsonr(ref_data[ref_data>(self.pearson_intlimit*ref_data.max())].flatten(),
        #                            np.abs(tmp_data[ref_data>(self.pearson_intlimit*ref_data.max())].flatten()))
        pearson_coeff = pearsonr(ref_data.flatten(),np.abs(tmp_data).flatten())
        print(pearson_coeff)
        # define your output
        dump.put([x, y, z, ref_scanno, tmp_scanno, pearson_coeff[0]])
        return
        
    def get_data_array(self,array_h5path,whitefield = False,darkfield = False, detectorGaps = False):
        """
        retrieve an array from an hdf5 file and apply the necessary corrections to it
        """
        with h5.File(self.h5fn,'r') as h5f:
            tmp_array = h5f[array_h5path][()]
            
            if darkfield:
                tmp_array = self.apply_darkfield_correction(tmp_array)
                            
            if whitefield:
                tmp_array = self.apply_whitefield_correction(tmp_array)

            if detectorGaps:
                tmp_array = addDetectorGaps(tmp_array)
                
        return tmp_array

    def set_ref_array(self,):
        self.ref_array = self.get_data_array(self.ref_array_h5path, whitefield = True,darkfield = True, detectorGaps = True)

    def apply_darkfield_correction(self, array, dark_photon_threshold=5):
        """
        apply a darkfield correction to a 2D or 3D input array
        set pixels above dark_photon_threshold to zero
        """

        #self.darktifpath = darktifpath
        darkImage = np.array(Image.open(self.darktifpath))
        #darkImage = addDetectorGaps(darkImage)
        if array.shape[-1]==256:  #if only one quadrant
            darkImage = darkImage[:256,:256]

        if len(array.shape)==3:
            for j in range(array.shape[0]):
                #print(array.shape,j)
                #pdb.set_trace()
                array[j,:,:][darkImage>dark_photon_threshold]==0.0
        if len(array.shape)==2:
            array[darkImage>dark_photon_threshold]==0.0
        return array

    def apply_whitefield_correction(self, array):
        """
        apply a whitefield correction to a 2D or 3D input array
        """
        #self.whitefieldtifpath = whitefieldtifpath
        whitefieldImage = np.array(Image.open(self.whitefieldtifpath))
        white = whitefieldImage.astype('float32') 
        whitefieldmean = np.mean(white[white>0])
        normwhite = white/whitefieldmean
        #normwhite = addDetectorGaps(normwhite)
        normwhite = self.apply_darkfield_correction(normwhite)
        normwhite[normwhite==0] = 1e20
        #pdb.set_trace()
        if array.shape[-1]==256:  #if only one quadrant
            normwhite = normwhite[:256,:256]
        
        array = array.astype("float32")
        if len(array.shape)==3:
            for j in range(array.shape[0]):
                array[j]/=normwhite
        if len(array.shape)==2:
            array/=normwhite
        return array

    def multi_CC(self, output=np.zeros(0, dtype=[('x', 'f8'),
                                                ('y', 'f8'),
                                                ('z', 'f8'),
                                                ('ref_scanno', 'i8'),
                                                ('scanno', 'i8'),
                                                ('pearsonr', 'f8')])):
        """
        do CC on a series of arrays 
        return the CC, as a structured array x,y,z,ref_scanno,scanno
        """
        _dump = Queue()
        _tot_ims = len(self.all_array_h5paths)
        _l = Lock()
        _processes = []

        ii = 0
        _data = self.ref_array

        _l.acquire()
        sys.stdout.write('%i arrays to compare\n' % (_tot_ims))
        sys.stdout.write('%i of %i\n' % (ii, _tot_ims))
        sys.stdout.write('ref scan No: %i\n' % (self.ref_scanno))

        _l.release()
        for tmp_scanno in self.all_scannos:
            # _l.acquire()
            sys.stdout.write('target %i loaded\n' % (tmp_scanno))
            # _l.release()
            tmp_shape = self.get_data_array(self.all_array_h5path%tmp_scanno, 
                                    whitefield = True,
                                    darkfield = True, 
                                    detectorGaps = True).shape
            if tmp_shape!=self.ref_array.shape:
                print("skip array - wrong shape",tmp_shape,self.ref_array.shape)
                continue
            # get the data
            _array2CC = self.get_data_array(self.all_array_h5path%tmp_scanno,whitefield = True,darkfield = True, detectorGaps = True)
            #pdb.set_trace()
            # send to multiple processors
            p = Process(target=self.CC_worker, args=(self.ref_array, _array2CC, self.ref_scanno, tmp_scanno, _dump))
            p.start()
            _processes.append(p)

        for p in _processes:
            p.join()

        output = self.dump_queue_multiCC(_dump, output=output)
        return output

    def dump_queue_multiCC(self, queue, output=np.zeros(0, dtype=[('x', 'f8'),
                                                ('y', 'f8'),
                                                ('z', 'f8'),
                                                ('ref_scanno', 'i8'),
                                                ('scanno', 'i8'),
                                                ('pearsonr', 'f8')])):
        """
        output handler for multiple processes cross correlation
        """
        while True:
            #print(queue.qsize())
            if int(queue.qsize()) == 0:
                break
            a = queue.get()
            #pdb.set_trace()
            
            output = np.r_[output, np.array([(a[0], a[1], a[2], a[3], a[4], a[5])],
                                            dtype=[('x', 'f8'), 
                                                    ('y', 'f8'), 
                                                    ('z', 'f8'),
                                                    ('ref_scanno', 'i8'),
                                                    ('scanno', 'i8'),
                                                    ('pearsonr', 'f8')])]
            
        # print output
        return output


    def shift_worker(self, h5path, shift, tmp_scanno, dump):
        """
        worker function for multishift
        """
        #CCshift = GetImageRegistration(ref_data, tmp_data, 100) # this does 3D in 2D steps so manual correlation coeff
        tmp_data = self.get_data_array(h5path,whitefield = True,darkfield = True, detectorGaps = True)
        tmpshift = (shift['x'],shift['y'],shift['z'])
        subpixel_shiftedarray = Shift(tmp_data,tmpshift)
        sys.stdout.write('.')

        intpixel_shiftedarray = Shift(tmp_data,map(np.int32,tmpshift))
        sys.stdout.write('.')

        if self.debug:
            sys.stdout.write(tmpshift,map(np.int32,tmpshift))
        sys.stdout.write('!')
        
        # define your output
        dump.put([subpixel_shiftedarray,intpixel_shiftedarray])
        return


    def multi_shift(self, CCanalysis, pcoeffthreshold):
        """
        shift multiple arrays  and export the shifted array - memory issues here
        """
        _dump = Queue()
        _l = Lock()
        _processes = []
        
            
        self.all_scannos = CCanalysis[CCanalysis['pearsonr']>pcoeffthreshold]["scanno"]
        ii = 0
        #_data = self.ref_array # make RawArray here

        _l.acquire()
        sys.stdout.write('arrays to add: %s\n' % (str(self.all_scannos)))
        #sys.stdout.write('ref scan No: %i\n' % (self.ref_scanno))
        _l.release()
        
        for tmp_scanno in self.all_scannos:
            # _l.acquire()
            sys.stdout.write('target %i loaded\n' % (tmp_scanno))
            # _l.release()
            shift=CCanalysis[CCanalysis["scanno"]==tmp_scanno]
            # get the data
            #_array2shift = self.get_data_array(self.all_array_h5path%tmp_scanno)
            h5path = self.all_array_h5path%tmp_scanno
            # send to multiple processors
            p = Process(target=self.shift_worker, args=(h5path, shift, tmp_scanno, _dump))
            p.start()
            _processes.append(p)

        for p in _processes:
            p.join()

        output_subpixel, output_intpixel= self.dump_queue_multishift(_dump,)
        return output_subpixel, output_intpixel

    def dump_queue_multishift(self, queue, output_subpixel=[],output_intpixel=[]):
        """
        output handler for multiple processes shift
        """        
        while True:
            #print(queue.qsize())
            if int(queue.qsize()) == 0:
                break
            a = queue.get()
            #pdb.set_trace()
            
            output_subpixel.append(a[0])
            output_intpixel.append(a[1])
            
        # print output
        return output_subpixel, output_intpixel

