#!/usr/bin/env python

#----------------------------------------------------------------------
# Description: 
#    taken from pscan_align (Steven Leake)
#
#    reduced by Carsten Richter
#    will be extended again later

#   
# Author: Steven Leake <steven.leake@esrf.fr>
# Created at: 11. Jun 12:00:00 CET 2016
# Computer: 
# System: 
#
# TODO:
#  ODO: add shexacor to specfile inspector, complete spec interface here + test
#----------------------------------------------------------------------

import os
import collections

import numpy as np
from silx.io.specfile import SpecFile
from id01lib.plot.interactive import GenericIndexTracker, GenericIndexTracker1D
from id01lib.process import SpecClientWrapper

ScanRange = collections.namedtuple("ScanRange", ['name', 'start', 'stop', 'numpoints'])

class PScanTracker(GenericIndexTracker):
    """
        This tracker child does the connection to spec
    """
    def __init__(self, ax, specclient, norm="linear",
                       quantum=1., transposed=True, exit_onclick=False, rectangle_onclick=False):
        """
            Class to fill a matplotlib axes with with data
            from fast spec scans (`pscan`)

            Inputs:
                ax : matplotlib.Axes
                    axes used to show data

                specclient : SpecClientWrapper.SpecClientSession
                    a wrapped SpecClient instance to handle
                    communication with spec

                norm : str, float
                    normalization of the colormap

        """
        if not isinstance(specclient, SpecClientWrapper.SpecClientSession):
            raise ValueError("Need `SpecClientWrapper.SpecClientSession` "
                             "instance as second argument.")

        # get command
        self.pscan_vars = pscan_vars = specclient.get_sv("PSCAN_ARR")
        self.specclient = specclient
        self.command = cmd = pscan_vars["header/cmd"].split()
        print(cmd)

        lima_roi, device = specclient.find_roi_list()

        # pscan motor name, start, stop, numpoints
        m1 = ScanRange(cmd[1], float(cmd[2]), float(cmd[3]), int(cmd[4]))
        m2 = ScanRange(cmd[5], float(cmd[6]), float(cmd[7]), int(cmd[8]))

        self.x, self.y = (m2, m1) if transposed else (m1, m2)
        self.transposed = transposed # to look similar to pymca

        data = self.load_data()

        self._args = norm, quantum, transposed
        super(PScanTracker, self).__init__(ax, data, norm, quantum, exit_onclick)

        self.ax.set_xlabel(self.x.name)
        self.ax.set_ylabel(self.y.name)
        self.set_extent(self.x.start, self.x.stop, self.y.start, self.y.stop)

        self.rois = lima_roi #['%i'%(i+1)] for i in range(self.slices)]
        self.set_axes_properties(title=self.rois)

        basename = os.path.basename(pscan_vars["file"])
        scan_no = pscan_vars["scan_no"]
        self.start_time = start_time = pscan_vars["timestamp/_pscan_doscan01"]
        title = "File: %s; Scan: %s; %s"%(basename, scan_no, start_time)
        if hasattr(self, "figtitle"):
            self.figtitle.set_text(title)
        else:
            self.figtitle = self.fig.suptitle(title)

    
    def load_data(self):
        data = self.specclient.get_sv("PSCAN_ROICOUNTER_DATA")[1:]
        try:
            data = data.reshape((-1, self.x.numpoints, self.y.numpoints, 7))[:,:,:,2] # what is this 7?? answer: 2 is the sum, there are other things like max.
        except ValueError:
            print("Warning: reshape failed.")
            return self.data
        if self.transposed:
            data = data.transpose(0, 2, 1)
        if not (data>0).any():
            return self.data
        return data

    def re_init(self):
        return self.__init__(self.ax, self.specclient, *self._args)

    def reload(self):
        """
            To be called regularly in pscan_live.
        """
        # check if new scan was started:
        pscan_vars = pscan_vars = self.specclient.get_sv("PSCAN_ARR")
        start_time = pscan_vars["timestamp/_pscan_doscan01"]
        if start_time!=self.start_time:
            #print(scan_no,self.scan_no,basename,self.basename)
            #self.specclient.varcache.pop("PSCAN_ARR")
            self.re_init()
        else:
            self.data = self.load_data()
            super(PScanTracker, self).update()



class Annotate(GenericIndexTracker):
    """
        This tracker child does the connection to spec
    """
    def __init__(self, ax, specclient, norm="linear",
                       quantum=1., transposed=True, exit_onclick=False, rectangle_onclick=False):
        """
            Class to fill a matplotlib axes with with data
            from fast spec scans (`pscan`)

            Inputs:
                ax : matplotlib.Axes
                    axes used to show data

                specclient : SpecClientWrapper.SpecClientSession
                    a wrapped SpecClient instance to handle
                    communication with spec

                norm : str, float
                    normalization of the colormap

        """
        if not isinstance(specclient, SpecClientWrapper.SpecClientSession):
            raise ValueError("Need `SpecClientWrapper.SpecClientSession` "
                             "instance as second argument.")
        super(Annotate, self).__init__(ax, norm, quantum, 
                                       exit_onclick, rectangle_onclick)



class PScanTracker1D(GenericIndexTracker1D):
    """
        This tracker child does the connection to spec
    """
    def __init__(self, ax, specclient, norm="linear",
                       quantum=1., transposed=True, exit_onclick=False, rectangle_onclick=False):
        """
            Class to fill a matplotlib axes with with data
            from fast spec scans (`pscan`)

            Inputs:
                ax : matplotlib.Axes
                    axes used to show data

                specclient : SpecClientWrapper.SpecClientSession
                    a wrapped SpecClient instance to handle
                    communication with spec

                norm : str, float
                    normalization of the colormap

        """
        if not isinstance(specclient, SpecClientWrapper.SpecClientSession):
            raise ValueError("Need `SpecClientWrapper.SpecClientSession` "
                             "instance as second argument.")

        # get command
        self.pscan_vars = pscan_vars = specclient.get_sv("PSCAN_ARR")
        self.specclient = specclient
        self.command = cmd = pscan_vars["header/cmd"].split()
        print(cmd)

        lima_roi, device = specclient.find_roi_list()

        # pscan motor name, start, stop, numpoints
        m1 = ScanRange(cmd[1], float(cmd[2]), float(cmd[3]), int(cmd[4]))
        m2 = ScanRange(cmd[5], float(cmd[6]), float(cmd[7]), int(cmd[8]))

        self.x, self.y = (m2, m1) if transposed else (m1, m2)
        self.transposed = transposed # to look similar to pymca

        data = self.load_data()

        self._args = norm, quantum, transposed
        super(PScanTracker1D, self).__init__(ax, data, norm, quantum, exit_onclick)

        self.ln[0].set_xdata(np.arange(float(cmd[2]),float(cmd[3]),(float(cmd[3])-float(cmd[2]))/int(cmd[4])))
        self.ax.set_xlim(float(cmd[2]),float(cmd[3]))
        self.ax.set_xlabel(self.y.name)
        self.ax.set_ylabel("Intensity")
        self.rois = lima_roi #['%i'%(i+1)] for i in range(self.slices)]
        self.set_axes_properties(title=self.rois)

        basename = os.path.basename(pscan_vars["file"])
        scan_no = pscan_vars["scan_no"]
        self.start_time = start_time = pscan_vars["timestamp/_pscan_doscan01"]
        title = "File: %s; Scan: %s; %s"%(basename, scan_no, start_time)
        if hasattr(self, "figtitle"):
            self.figtitle.set_text(title)
        else:
            self.figtitle = self.fig.suptitle(title)



    def load_data(self):
        data = self.specclient.get_sv("PSCAN_ROICOUNTER_DATA")[1:]
        try:
            data = data.reshape((-1, self.x.numpoints, self.y.numpoints, 7))[:,:,:,2] # what is this 7?? answer: 2 is the sum, there are other things like max.
        except ValueError:
            print("Warning: reshape failed.")
            return self.data
        if self.transposed:
            data = data.transpose(0, 2, 1)
        if not (data>0).any():
            return self.data
        return data

    def re_init(self):
        return self.__init__(self.ax, self.specclient, *self._args)

    def reload(self):
        """
            To be called regularly in pscan_live.
        """
        # check if new scan was started:
        pscan_vars = pscan_vars = self.specclient.get_sv("PSCAN_ARR")
        start_time = pscan_vars["timestamp/_pscan_doscan01"]
        if start_time!=self.start_time:
            #print(scan_no,self.scan_no,basename,self.basename)
            self.specclient.varcache.pop("PSCAN_ARR")
            self.re_init()
        else:
            self.data = self.load_data()
            super(PScanTracker1D, self).update()



class PScanTrackerSpecfile(GenericIndexTracker):
    """
        This tracker child does the connection to spec
    """
    def __init__(self, ax,
                       spec_fn,
                       scan_no,
                       norm="linear",
                       quantum=1.,
                       rois=[],
                       transposed=True,
                       monitor=None,
                       exit_onclick=False,
                       ignore_cols=None,
                       rectangle_onclick=False):


        if isinstance(scan_no, int):
            scan_no = "%i.1"%scan_no
        elif scan_no.isdigit():
            scan_no = "%s.1"%scan_no

        self.spec_fn = spec_fn
        self.scan_no = scan_no

        scans = SpecFile(self.spec_fn)
        try:
            index=scans.keys().index(self.scan_no)
        except:
            raise ValueError('%s not in specfile (%s)'%(self.scan_no, self.spec_fn) \
                           + ' --> available scans: %s'%", ".join(k for k in scans.keys()))

        self.scan = scan = scans[index]

        # better way of finding the counters of interest?
        if ignore_cols is None:
            ignore_cols = ['timer','imgnr','adcX','adcY','adcZ','adc3']


        if not rois:
            rois = [l for l in scan.labels if l not in ignore_cols]
        self.rois = rois

        command = scan.scan_header_dict["S"]
        command = command.split()[1:]

        m1 = ScanRange(command[1], float(command[2]), float(command[3]), int(command[4]))
        m2 = ScanRange(command[5], float(command[6]), float(command[7]), int(command[8]))

        self.x, self.y = (m2, m1) if transposed else (m1, m2)
        self.transposed = transposed # to look similar to pymca?


        data = np.stack((scan.data_column_by_name(roi) for roi in rois if roi!=monitor))
        if not monitor is None:
            monitor = scan.data_column_by_name(monitor)
            data /= monitor
        #data = data.reshape((-1, m2.numpoints, m1.numpoints)) # slow to fast axes
        data.resize((len(rois)-(1 if monitor else 0), m2.numpoints, m1.numpoints)) # slow to fast axes

        if transposed:
            data = data.transpose(0, 2, 1)
        self._args = norm, quantum, transposed # maybe not necessary
        super(PScanTrackerSpecfile, self).__init__(ax, data, norm, quantum,exit_onclick,rectangle_onclick)

        self.ax.set_xlabel(self.x.name)
        self.ax.set_ylabel(self.y.name)
        self.set_extent(self.x.start, self.x.stop, self.y.start, self.y.stop)
        self.set_axes_properties(title=self.rois)

        self.start_time = start_time = scan.scan_header_dict['D']

        title = "File: %s; Scan: %s; %s"%(spec_fn, scan_no, start_time)
        if hasattr(self, "figtitle"):
            self.figtitle.set_text(title)
        else:
            self.figtitle = self.fig.suptitle(title)


