#----------------------------------------------------------------------
# Description: 
#   distribution utility functions
#   
# Author: Carsten Richter
# Created at: ?? 2018
# Computer: 
# System: 
#  
#----------------------------------------------------------------------


import numpy as np


def mode1d(y, bins=100, integ_range=5, weights=None):
    """
        Spits out the `mode` (most frequent value) of a continuous
        1D distribution by discretization to a certain number of *bins*.

        To avoid discretization error, the max position is computed by
        via the center of mass of a +-range around the max.
    """

    y = np.ravel(y)
    y = y[~np.isnan(y)]

    yd, xd = np.histogram(y, bins=bins, weights=weights)
    xd = (xd[1] - xd[0])/2 + xd[:-1] # centers of bins

    imax = yd.argmax()

    left = max(0, -integ_range+imax)
    right = min(len(xd)-1, integ_range+imax)

    irange = np.arange(left, right)
    yd, xd = yd[irange], xd[irange]
    yd -= yd.min()

    pmax = (yd*xd).sum() / yd.sum()

    return pmax




if __name__ == "__main__":
    from pylab import *
    y = exp(randn(1001)/1.5)
    hist(y,bins=100)

    xmax = mode1d(y, 100, 5)
    plot([xmax]*2, gca().get_ylim(), label="mode")
    plot([y.mean()]*2, gca().get_ylim(), label="mean")
    plot([median(y)]*2, gca().get_ylim(), label="median")
    plot([sqrt((y**2).mean())]*2, gca().get_ylim(), label="rms")
    legend()
    show()
