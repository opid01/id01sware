#----------------------------------------------------------------------
# Description: 
#   vector utility functions
#   
# Author: Carsten Richter
# Created at: ?? 2018
# Computer: 
# System: 
#  
#----------------------------------------------------------------------

import numpy as np



def Rz(ang, degrees=True):
    """
        Returns the rotation matrix for rotation around
        the z-axis according to the given angle in degrees per default.
    """
    if degrees:
        ang = np.radians(ang)
    One = np.ones_like(ang)
    Zero = np.zeros_like(ang)
    return np.array([
                     [ np.cos(ang), -np.sin(ang), Zero],
                     [ np.sin(ang),  np.cos(ang), Zero],
                     [        Zero,         Zero,  One]
                    ])

Rx = lambda ang, degrees=True: np.roll(Rz(ang, degrees), 1, (0,1))
Ry = lambda ang, degrees=True: np.roll(Rz(ang, degrees), 2, (0,1))

Rx.__doc__ = Rz.__doc__.replace("z-axis", "x-axis")
Ry.__doc__ = Rz.__doc__.replace("z-axis", "y-axis")






def cartesian2spherical(qx, qy, qz, degrees=True):
    """
        Definition of coordinate system when
        all diffractometer angles are zero:
        (see https://pasteboard.co/HmtPj91.png)
            qx -- beam direction
            qz -- vertical up
            qy right handed

            r -- length of q vector
            theta -- angle between q and qx
                (left handed rotation around qy)
            phi -- roll 
                (left handed rotation around qx)
    """
    y = qz
    z = qx
    x = qy

    r = np.sqrt(qx**2 + qy**2 + qz**2)
    theta = np.arccos(z/r)
    phi = np.arctan2(y,x)

    if degrees:
        phi   = np.degrees(phi)
        theta = np.degrees(theta)

    return dict(phi=phi, theta=theta, r=r)



def spherical2cartesian(phi, theta, r, degrees=True):
    """
        Definition of coordinate system when
        all diffractometer angles are zero:
        (see https://pasteboard.co/HmtPj91.png)
            qx -- beam direction
            qz -- vertical up
            qy right handed

            r -- length of q vector
            theta -- angle between q and qx
                (left handed rotation around qy)
            phi -- roll 
                (left handed rotation around qx)
    """
    if degrees:
        theta = np.radians(theta)
        phi   = np.radians(phi)

    vx = r*np.sin(theta)*np.cos(phi)
    vy = r*np.sin(theta)*np.sin(phi)
    vz = r*np.cos(theta)

    qx, qy, qz = vz, vx, vy

    return dict(qx=qx, qy=qy, qz=qz)




def unit_vector(vector):
    """ Returns the unit vector of the vector.  """
    return vector / np.linalg.norm(vector)

def angle_between(v1, v2):
    """ Returns the angle in radians between vectors 'v1' and 'v2'::

            >>> angle_between((1, 0, 0), (0, 1, 0))
            1.5707963267948966
            >>> angle_between((1, 0, 0), (1, 0, 0))
            0.0
            >>> angle_between((1, 0, 0), (-1, 0, 0))
            3.141592653589793
    """
    v1_u = unit_vector(v1)
    v2_u = unit_vector(v2)
    return np.arccos(np.clip(np.dot(v1_u, v2_u), -1.0, 1.0))



