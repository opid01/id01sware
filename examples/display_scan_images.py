import fabio
from silx.io.specfile import SpecFile
import matplotlib.pyplot as plt
import numpy as np
import os
import timeit
import sys
import argparse

parser = argparse.ArgumentParser(
	description='Simple visualization of intensity from .spec file.')

parser.add_argument('specfile', type=str, nargs=1,
					help='spec file.')

parser.add_argument('scan', type=str, nargs='+',
					help='scan number to plot.')

args = parser.parse_args()

filename = args.specfile[0]
if not "spec" in filename:
	raise ValueError("Only spec files are eligible.")
sf = SpecFile(filename)
cols = sf.labels(0)

scanNb = int(args.scan[0])

# filename = '/data/id01/inhouse/girard/201706id01/id01/spec/Q615074P01_D2.spec'
# filename = '/data/visitor/ma3571/id01/spec/align_bridge.spec'
# filename = sys.argv[1]
#
#
#
# sf = SpecFile(filename)
# scanNb = 65


iobs = None
mask = None
params = dict()

s = sf["%d.1"%scanNb]
h = s.scan_header_dict

#if 'UDETCALIB' in h:
#	params['detector_distance'] = float(h['UDETCALIB'].split('stance_CC=')[1].split(',')[0])
#	print("Reading detector distance from spec data: %6.3fm", self.params['detector_distance'])

nrj = float(h['UMONO'].split('mononrj=')[1].split('ke')[0])
w = 12.384 / nrj
params['wavelength'] = w
print("Reading nrj from spec data: nrj=%6.3fkeV, wavelength=%6.3fA" % (nrj, w))

if 'ULIMA_eiger2M' in h:
	imgn = s.data_column_by_name('ei2minr').astype(np.int)
	imgname = h['ULIMA_eiger2M'].strip().split('_eiger2M_')[0] + '_eiger2M_%05d.edf.gz'
	print("Using Eiger 2M detector images: %s" % (imgname))
else:
	imgn = s.data_column_by_name('mpx4inr').astype(np.int)
	imgname = h['ULIMA_mpx4'].strip().split('_mpx4_')[0] + '_mpx4_%05d.edf.gz'
	print("Using Maxipix mpx4 detector images: %s" % (imgname))
	mask = 'Maxipix'

if "inhouse" in filename:
	imgname = imgname.replace("visitor/ma3323", "id01/inhouse/girard/201706id01")

t0 = timeit.default_timer()
sys.stdout.write('Reading frames: ')
ii = 0
for i in imgn:
	if (i - imgn[0]) % 20 == 0:
		sys.stdout.write('%d ' % (i - imgn[0]))
		sys.stdout.flush()
	frame = fabio.open(imgname % i).data
	#if mask is 'Maxipix':
		#print('Initializing MAXIPIX mask')
		#mask = np.zeros(frame.shape, dtype=np.int8)
		#ny, nx = frame.shape[-2:]
		#if frame.ndim == 2:
			#for i in range(258, ny, 258):
				#mask[i - 3:i + 3] = 1
			#for i in range(258, nx, 258):
				#mask[:, i - 3:i + 3] = 1
		#else:
			#for i in range(258, ny, 258):
				#mask[:, i - 3:i + 3] = 1
			#for i in range(258, nx, 258):
				#mask[:, :, i - 3:i + 3] = 1
	if iobs is None:
		iobs = np.empty((len(imgn), frame.shape[0], frame.shape[1]), dtype=frame.dtype)
	#iobs[ii] = np.ma.masked_array(frame, mask=mask)
	iobs[ii] = frame
	ii += 1
print("\n")
dt = timeit.default_timer() - t0
print('Time to read all frames: %4.1fs [%5.2f Mpixel/s]' % (dt, iobs.size / 1e6 / dt))

"""

plt.figure()
plt.imshow(np.log10(iobs.sum(axis=0)))
plt.colorbar()
plt.title('Integrated intensity over scan nb : %3d' %scanNb)
plt.show()

"""

from silx.gui import qt
from silx.gui.plot.StackView import StackView, StackViewMainWindow

app = qt.QApplication([])

#sv = StackViewMainWindow()# yinverted=True)
sv = StackView(yinverted=True)
sv.setColormap("viridis", autoscale=True, normalization="log", vmin=0)
#plotdata[pl.isnan(iobs)] = 0
sv.setStack(iobs)
sv.setYAxisInverted(True)
sv.show()

app.exec_()
