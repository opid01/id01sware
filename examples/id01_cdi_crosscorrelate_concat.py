
from id01lib.process.CrossCorrelator import CrossCorrelator

scanrange = [1146, 1152, 1149, 1143, 1140]

h5fn = "/data/id01/inhouse/leake/20190313_34IDC/Analysis/rawdata.h5"
ref_array_h5path = "/C5/%i.1/measurement/image_0/data"
all_array_h5path =  "/C5/%i.1/measurement/image_0/data"
ref_scanno = scanrange[0]
pearsoncoeff = 0.95
outfilename = "/data/id01/inhouse/leake/20190313_34IDC/Analysis/rawdata_concat1.h5"
darktifpath = "/data/id01/inhouse/leake/20190313_34IDC/Analysis/dark.tif"
whitefieldtifpath = "/data/id01/inhouse/leake/20190313_34IDC/Analysis/CelaWhiteField.tif"

"""
cc = CrossCorrelator(h5fn, 
                    ref_array_h5path, 
                    all_array_h5path, 
                    ref_scanno, 
                    scannos, 
                    darktifpath = darktifpath, 
                    whitefieldtifpath = whitefieldtifpath)
"""
"""
results = dict()
for ref in scannos:
    cc = CrossCorrelator(h5fn, 
                        ref_array_h5path, 
                        all_array_h5path, 
                        ref, 
                        scannos,
                        darktifpath = darktifpath, 
                        whitefieldtifpath = whitefieldtifpath)
                        
    output = cc.multi_CC()
    results[ref] = output
    #print(output)
    
# pick the winner
ranking = dict()
for item in results:
    ranking[item]=results[item][results[item]['pearsonr']>pearsoncoeff].shape[0]

maxitem = max(ranking.iteritems(),key=operator.itemgetter(1))[0]

# combine the data after shifting both integer and subpixel option
CCanalysis = results[maxitem]

# memory issues here
#output_subpixel, output_intpixel = cc.multi_shift(CCanalysis,pearsoncoeff)

total_intpixel = np.empty(cc.ref_array.shape)
total_subpixel = np.empty(cc.ref_array.shape)
# no good workaround in a multiprocessing sense so do it manually
scans2concat = CCanalysis[CCanalysis['pearsonr']>pearsoncoeff]["scanno"]
for scan in scans2concat:
      
    tmp_data = cc.get_data_array(all_array_h5path%scan,whitefield = True,darkfield = True, detectorGaps = True)
    shift = CCanalysis[CCanalysis["scanno"]==scan]
    tmpshift = (shift['x'],shift['y'],shift['z'])
    total_subpixel += np.abs(Shift(tmp_data,tmpshift))
    total_intpixel += np.roll(tmp_data,tuple(int(x) for x in tmpshift))
    print("Scan %i complete.."%scan)
    
with h5.File(outfilename, 'w') as outputfile:
    outputfile.create_dataset("/concat_%i_%i/sub_pixel"%(scanrange[0],scanrange[1]),data = total_subpixel,compression=6)
    outputfile.create_dataset("/concat_%i_%i/int_pixel"%(scanrange[0],scanrange[1]),data = total_subpixel,compression=6)
    outputfile["/concat_%i_%i/scans_included"%(scanrange[0],scanrange[1])] = scans2concat
"""


#scanranges = [[167, 180, 364, 361, 376, 394, 397, 367, 370, 373, 379, 382, 385, 388, 391, 1146, 1152, 1149, 1143, 1140],
#scanranges = [[1906, 1894, 1900, 1909, 1903, 1897],
scanranges = [[1542, 1706, 1730, 1703, 1611, 1712, 1745, 1709, 1724, 1721, 1736, 1715, 1733, 1739, 1742, 1727, 1718, 1548, 1554, 1551, 1545, 1557],
[1134, 1131, 1128, 1125, 1122],
[1605, 1518, 1515, 1533, 1527, 1524, 1521, 1530, 1536],
[1362, 1374, 1116, 1113, 1371, 1368, 1365, 1110, 1107, 1104, 535, 722, 713, 728, 725, 734, 743, 740, 719, 716, 731, 746, 737],
[1509, 1494, 1506, 1874, 1503, 1497, 1491, 1500, 1488, 1599, 1886, 1889, 1877, 1880, 1883, 1171, 1168, 1165, 854, 437, 434, 416, 407, 425, 440, 413, 419, 1077, 431, 422, 443, 446, 216, 428, 404, 1074, 1068, 1071, 1080, 1162, 842, 517, 848, 851, 520, 523, 845, 547, 538, 544, 541, 550, 1098, 1158, 1174, 1086, 1089, 1095, 1092],
[1652, 1655, 1658, 1661, 1664],
[1461, 637, 1479, 1470, 1482, 1467, 1593, 1476, 1473, 1464, 1050, 1062, 1056, 1059, 640, 1053, 643, 646, 661, 667, 658, 664, 652, 655, 649],
[1854, 1317, 1866, 1035, 1041, 1857, 1863, 1860, 1314, 1869, 1038, 1032, 1323, 1044, 1320, 1326, 302, 198, 308, 311, 329, 335, 305, 314, 317, 320, 323, 326, 332],
[1846, 1843, 1849, 1840, 1837, 1834, 1359, 1356, 1353, 1350, 1347, 1026, 1017, 1020, 1023, 1185, 1188, 1191, 1194, 1197, 1200, 1203, 1206, 1209, 1212, 1215, 1218, 1014, 577, 586, 580, 589, 562, 571, 574, 583, 526, 559, 565, 568, 556],
[37, 52, 55, 58, 61, 64, 67, 70, 73, 76, 79, 82, 85],
[1440, 1443, 1446, 1452, 1449, 1455, 1434, 1008, 999, 1437, 1002, 996, 1587, 1005, 225],
[752, 785, 758, 761, 776, 782, 755, 764, 767, 770, 773, 779, 990, 981, 984, 987, 1829, 978, 1820, 1823, 1826, 1817, 1305, 1296, 1299, 1308, 1814, 1302],
[1685, 1688],
[253, 488, 259, 265, 268, 277, 286, 289, 292, 1332, 491, 1281, 250, 256, 262, 271, 274, 280, 283, 972, 1335, 247, 151, 157, 963, 154, 160, 966, 969, 1278, 1287, 1338, 1284, 1800, 1803, 1809, 1806, 1341, 1290, 1797, 960, 1794, 1259],
[942, 945, 954, 948, 951],
[1422, 1581, 1416, 821, 827, 1425, 1413, 833, 830, 1428, 1419, 824, 836],
[674, 695, 532, 692, 698, 707, 680, 704, 686, 701, 677, 689, 683, 933, 927, 930, 936, 924, 232, 1789, 1780, 1569, 1783, 1774, 1777, 1786, 1575, 1386, 1383, 1401, 1392, 1398, 1389, 1395, 1380],
[607, 610, 595, 529, 604, 613, 897, 622, 625, 616, 619, 598, 915, 906, 628, 601, 1754, 912, 909, 918, 1760, 1763, 1769, 1766, 1757],
[1697, 1616, 1622, 1625, 1628, 1631, 1634, 1637, 1640, 1643, 1646, 1649, 1694, 1700],
] # all datasets - to be used with ConcatenateDatasetsBlind < needs speeding up...

for scanrange in scanranges:
                        
    results = dict()
    for ref in scanrange:
        cc = CrossCorrelator(h5fn, 
                            ref_array_h5path, 
                            all_array_h5path, 
                            ref, 
                            scanrange,
                            darktifpath = darktifpath, 
                            whitefieldtifpath = whitefieldtifpath)
                            
        output = cc.multi_CC()
        results[ref] = output
        #print(output)
        
    # pick the winner
    ranking = dict()
    for item in results:
        ranking[item]=results[item][results[item]['pearsonr']>pearsoncoeff].shape[0]

    maxitem = max(ranking.iteritems(),key=operator.itemgetter(1))[0]
    
    # combine the data after shifting both integer and subpixel option
    CCanalysis = results[maxitem]
    
    # memory issues here
    #output_subpixel, output_intpixel = cc.multi_shift(CCanalysis,pearsoncoeff)

    total_intpixel = np.empty(cc.ref_array.shape)
    total_subpixel = np.empty(cc.ref_array.shape)
    # no good workaround in a multiprocessing sense so do it manually
    scans2concat = CCanalysis[CCanalysis['pearsonr']>pearsoncoeff]["scanno"]
    for scan in scans2concat:
          
        tmp_data = cc.get_data_array(all_array_h5path%scan,whitefield = True,darkfield = True, detectorGaps = True)
        shift = CCanalysis[CCanalysis["scanno"]==scan]
        tmpshift = (shift['x'],shift['y'],shift['z'])
        total_subpixel += np.abs(Shift(tmp_data,tmpshift))
        total_intpixel += np.roll(tmp_data,tuple(int(x) for x in tmpshift))
        print("Scan %i complete.."%scan)
        
    with h5.File(outfilename, 'w') as outputfile:
        outputfile.create_dataset("/concat_%i_%i/sub_pixel"%(scanrange[0],scanrange[1]),data = total_subpixel,compression=6)
        outputfile.create_dataset("/concat_%i_%i/int_pixel"%(scanrange[0],scanrange[1]),data = total_subpixel,compression=6)
        outputfile["/concat_%i_%i/scans_included"%(scanrange[0],scanrange[1])] = scans2concat
        for key in results.keys():
            outputfile["/concat_%i_%i/results/s%i"%(scanrange[0],scanrange[1],key)] = results[key]
            outputfile["/concat_%i_%i/ranking/s%i"%(scanrange[0],scanrange[1],key)] = ranking[key]
