"""
This script generates an .h5 file containing spec parameters as well as images
the sample and scan numbers specified in the input.
The script is optimised for experiment hc3737 made on BM32. See logbook for
details. Check directories specified in script before running.

19/07/18 E Zatterin
"""
import fabio
import numpy as np
import os
import sys
import h5py
import argparse
import pyFAI
import re

from pyFAI.distortion import Distortion
from silx.io.convert import write_to_h5
from scipy.signal import medfilt2d
from silx.io.specfile import SpecFile
from itertools import compress

#############
### INPUT ###
#############

sample = 'e17059'       ## assumes existence of <sample_name>.spec
scan_list = [140,150]   ## As contained in <sample_name>.spec
overwrite_imgs = True   ## overwrite data in h5 group
rm_unwanted_imgs = True ## To remove imgs where SAXS from the protective foil
						# of the Xpad detector is present
scanned_motor = 'THETA' ## To make sure only the required number of images
						# is appended in case both .edf.gz and .edf are
						# generated

#############
### CODE ####
#############

if overwrite_imgs:
	print('\n>>  overwrite is set to True!  <<')

# Define a function for natural sorting to get the imgs in the right way
_nsre = re.compile('([0-9]+)')
def natural_sort_key(s):
    return [int(text) if text.isdigit() else text.lower()
            for text in re.split(_nsre, s)]

# Specify base dir and h5 file
basedir = '/data/visitor/hc3737/bm02'
h5file = '{0}/analysis/{1}/{1}.h5'.format(basedir, sample)

# Write the specfile to h5
specfile = '{0}/dataspec/{1}.spec'.format(basedir, sample)
write_to_h5(specfile, h5file, mode="w") # always overwrite spec data

# Specify image directory list
with h5py.File(h5file, 'a') as h5f:

	for scan in scan_list:

		# Load the scan
		print('--> Scan {0} being processed'.format(scan))
		s = h5f['/{0}.1/'.format(scan)]

		# Stop if images exists and overwrite set to False
		if 'imgs' in s.keys() and not overwrite_imgs:
			print('>> ACHTUNG! Images of scan {0} already in h5 file, skipping\
					- set overwrite=True if you want them'.format(scan))

		else:

			# Set images directory
			imgdir = '{0}/dataspec/{1}.spec_S{2}/'.format(basedir, sample, scan)

			# List files in image directory
			imglist = os.listdir(imgdir)
			imglist.sort(key=natural_sort_key)

			# Take only edf.gz in case other edf files have been generated
			scan_length = s['measurement/{0}'.format(scanned_motor)].value.shape[0]
			if len(imglist) > scan_length:
				imglist =  list(compress(imglist, ['gz' in x for x in imglist]))
			imglistlen = len(imglist)

			# Load distortion correction
			D5 = pyFAI.detector_factory(basedir+"/analysis/D5Geom_JK.h5")
			D5_dis = Distortion(D5, resize=True)

			# Load hot pix mask
			hotpix = fabio.open(basedir+'/analysis/maskD5_V1_9keV_fastOTNpulse-20171122.edf.gz').data

			# Load FF correction
			ff = fabio.open(basedir+'/analysis/flatD5_V1_9keV_fastOTNpulse-20171122.edf.gz').data

			# Open each image with FabIO and correct it for distorion, hotpix, FF
			openimglist = []
			for index, img  in enumerate(imglist):
				sys.stdout.write("Loading image {0} of {1}".format(index, imglistlen))
				sys.stdout.write("\r")

				with np.errstate(divide='ignore'): # to avoid error in dividing by zero
					raw_img = fabio.open(imgdir+img).data / ff
					raw_img[np.isnan(raw_img)] = 0
					raw_img[hotpix==1] = 0
					raw_img = raw_img.astype('float32')
					raw_img = medfilt2d(raw_img, [3,3])

				# This correction reshapes the images to account for gaps
				corr_img = D5_dis.correct(raw_img)

				openimglist.append(corr_img)

			# Stack the images (dety, detx, depth)
			imstack = np.dstack(tuple(openimglist)).transpose(2,0,1)
			print('--> Done. Copying array to H5 file.')

			# Correct for the high int imgs due to sub peak
			# scattering off the detector gaps
			if rm_unwanted_imgs:

				# Make a list of the mean of each img in the stack
				lsmean = []
				for index in range(imstack.shape[0]):
					lsmean.append(imstack[index,:,:].mean())

				# Find the index with the highest intensity
				lsmean = np.array(lsmean)
				maxidx = lsmean.argsort()[::-1][0]

				# Replace idxs left and right of max idx
				imstack[maxidx-1,:,:] = imstack[maxidx-2,:,:]
				imstack[maxidx+1,:,:] = imstack[maxidx+2,:,:]
				imstack[maxidx,:,:] = (imstack[maxidx-1,:,:]+imstack[maxidx+1,:,:])/2

			# Overwrite dataset if it exists
			if 'imgs' in s.keys() and overwrite_imgs:
				del s['imgs']

			s.create_dataset('imgs', data=imstack, chunks=True,
									compression='gzip')
