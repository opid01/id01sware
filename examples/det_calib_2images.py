# BASIC detector calibration 
#    Maxipix detector
#    Take two images argv[1] , argv[2], seperated by a deltaDEL
#    Return the detector distance center of mass and cross correlation
#
#    CC closest to center of mass is the right one

import fabio
import scipy.ndimage as spyndim
from skimage.feature import register_translation
from scipy.ndimage.fourier import fourier_shift
import numpy as np
import sys

#images at start and end of scan
image1 = fabio.open(sys.argv[1]).getData()
image2 = fabio.open(sys.argv[2]).getData()

deltaDEL = float(sys.argv[3]) # degrees between images

def det_distance(pixperdeg,pix_size=55E-6):
    return (pixperdeg*pix_size)/np.tan(np.deg2rad(1.0))
    
COM1 = spyndim.measurements.center_of_mass(image1)
COM2 = spyndim.measurements.center_of_mass(image2)

if abs(COM2[0]-COM1[0]) < abs(COM2[1]-COM1[1]):
	pixperdeg = (COM2[1]-COM1[1])/deltaDEL
else:	 
	pixperdeg = (COM2[0]-COM1[0])/deltaDEL

COM = det_distance(pixperdeg)

shift, error, diffphase = register_translation(image1, image2, upsample_factor=100)
print("Detected pixel offset [y,x]: [%g, %g]" % (shift[0], shift[1]))

if shift[1]>shift[0]:
	shift[0]=shift[1]
pixperdeg = (516-shift[0])/deltaDEL

CC = det_distance(pixperdeg)

pixperdeg = (shift[0])/deltaDEL

CC1 = det_distance(pixperdeg)

print("COM: ",COM)
print("CC: ",CC," / ",CC1)



#cenpix=COM2+np.array([-0.052,0.124])*pixperdeg
#
#print("central pixel: ",cenpix)

