#!/usr/bin/env python
#  how to use overlay to characterize drift
# 
import os
import pylab as pl
import h5py
from silx.image import sift
from scipy import ndimage
from silx.io import specfile
from id01lib.plot.interactive import Overlay


import collections
import argparse
import re

_motordef = dict(pix="adcY",
                 piy="adcX",
                 piz="adcZ")

_printmotors = ['eta', 'phi', 'del', 'nu', 'thx', 'thy', 'thz']


base = "/data/id01/inhouse/crichter/test/id01sware_testdata/spec"



######## Scan File
scans = collections.OrderedDict()

#scans["PZO/KMAP_2018_04_19_022945/spec/KMAP_2018_04_19_022945_fast_00001.spec"] = range(0,15)
#scans["PZO/KMAP_2018_04_19_022945/spec/KMAP_2018_04_19_022945_fast_00001.spec"] = "8.1","5.1"
#scans["PZO/KMAP_2018_04_19_051408/spec/KMAP_2018_04_19_051408_fast_00001.spec"] = range(0,19)
scans["KMAP_2018_04_20_034246_fast_00001.spec"] =  0,1,2,3,4,5,7,8,9,10,11,12,13,14,15,16,17,18,19
#scans["align_fast_00154.spec"] = "1.1",
#scans["align_fast_00156.spec"] = "0.1",
#scans["PZO/KMAP_2018_04_20_034246/spec/KMAP_2018_04_20_034246_fast_00001.spec"] = "10.1"
#scans["PZO/KMAP_2018_04_20_034246/spec/KMAP_2018_04_21_051744_fast_00001.spec"] = "6.1"
#scans["align_fast_00212.spec"] = "3.1",
#scans["align_fast_00219.spec"] = "4.2",
#scans["align_fast_00223.spec"] = "2.1",
#scans["align_fast_00270.spec"] = "1.1",

#scans["PZO/KMAP_2018_04_21_051744/spec/KMAP_2018_04_21_051744_fast_00001.spec"] = range(0,12)

monitor = "cnt1"
col = "mpx4ro1"
gamma = 0.5
norm = 'linear'
cmap = "jet"
pixshift = 40

######################################




formatter = None
if norm=="log":
    _norm = lambda vmin, vmax: colors.LogNorm(vmin, vmax)
elif norm=="power":
    _norm = lambda vmin, vmax: Normalize.Normalize(vmin=vmin, vmax=vmax, stretch="power", exponent=gamma, clip=False)
    formatter = LogFormatter(10, labelOnlyBase=False)
else:
    _norm = lambda vmin, vmax: colors.Normalize(vmin, vmax)




def get_scan_data(fname, scanno):
    path = os.path.join(base, fname)
    print(path,scanno)
    sf = specfile.SpecFile(path)
    sc = sf[scanno]
    h = sc.scan_header_dict["S"].split()
    print(h)
    motor1 = h[2]
    motor2 = h[6]
    arraysizey = int(h[5])
    arraysizex = int(h[9])
    pscan_shape = arraysizey, arraysizex

    x = sc.data_column_by_name("adcY")
    y = sc.data_column_by_name("adcX")
    I0 = sc.data_column_by_name(monitor) if monitor in sc.labels else 1.

    roidata = sc.data_column_by_name(col)/I0
    x = x.reshape(pscan_shape)
    y = y.reshape(pscan_shape)
    roidata = roidata.reshape(pscan_shape)
    motorpos = dict(zip(sc.motor_names, sc.motor_positions))
    return x, y, roidata, motorpos


data = []
titles = []
for path in scans:
    for number in scans[path]:
        x1, y1, im1, motors = get_scan_data(path, number)
        data.append(im1)
        fname = os.path.basename(path)
        titles.append("%s : %s -- eta=%.3f"%(fname, number, motors["eta"]))

data = pl.array(data)

fig = pl.figure()
ax = fig.add_subplot(111)

tracker = Overlay(pixshift, ax, data, norm="linear")
tracker.set_extent(x1.min(), x1.max(), y1.min(), y1.max())
tracker.set_axes_properties(title=titles)
tracker.ax.set_xlabel("pix")
tracker.ax.set_ylabel("piy")




pl.show()

answer = raw_input("Save to filename .h5 (Enter to cancel): ")
if answer:
    if answer.endswith(".h5"):
        fname = answer
    else:
        fname = "%s.h5"%answer


    with h5py.File(fname) as h5f:
        h5f.clear()
        h5f.create_dataset("original", data = tracker.reference)
        h5f.create_dataset("shifted", data = tracker.data)
        h5f.create_dataset("shift", data = pl.array(tracker.current_shift))
        h5f.create_dataset("scans", data = titles)
