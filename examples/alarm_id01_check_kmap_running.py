# poll the kmap counters to see if they are still being updated

import time
from SpecClient import SpecVariable
import os

specname = "nano3:psic_nano"
email_add = "steven.leake@esrf.fr"
poll_time=300

pscan_data = SpecVariable.SpecVariable('PSCAN_ROICOUNTER_DATA',specname)
_pscan_data_ref = pscan_data.getValue()[1:]

while True:
    _pscan_data_nxt = pscan_data.getValue()[1:]
    if (_pscan_data_nxt!=_pscan_data_ref).sum()==0:
        # do something =  send an email
        os.system('echo `date`  | mail -s " kmap array did not update for %iseconds check the maxipix server!!! " %s'%(poll_time,email_add))
        print("oh dear")
    else:
        print("all OK")
    _pscan_data_ref = _pscan_data_nxt
    time.sleep(poll_time)

    
