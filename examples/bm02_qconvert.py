"""
This script generates perfomrs the conversion from angular to qspace for a set
of images contained in a specified .h5 file. It then writes the converted data
to a new .h5 file. All directories and data in this script are relative to
experiment hc3737 performed on BM32. See logbook for details.

19/07/18 E Zatterin
"""

import numpy as np
import xrayutilities as xu
import argparse
import h5py

from silx.io.specfile import SpecFile

#############
### INPUT ###
#############

# User input - incl. angular offsets (dth and dchi are calc below)
sample = 'e17059'
overwrite = True
max_bins = True
dphi, dnu, dtth = 0,0,0
roi=[0, 579, 600, 1153]
nbins = (600, 576, 549) # only relevant if maxbins false

# Set up detector parameters
en = 8000
distance = 0.512
cch = [292,997] # <-- the only thing that must be watched
npix = [579,1153]
pixsize = [130e-6, 130e-6]
chpdeg = [68.7, 68.7]

# "del" scans and the corresponding peak in two theta
# copy them from the spreadsheet
scan_no = [140,150]
tthsub_no = [138,148]
tthsub = [46.5684,46.6231]

############
### CODE ###
############

if overwrite:
	print('\n>>  overwrite is set to True!  <<')

# Specify base dir, h5 files, specfile
basedir = '/data/visitor/hc3737/bm02'
h5file_read = '{0}/analysis/{1}/{1}.h5'.format(basedir, sample)
h5file_write = '{0}/analysis/{1}/{1}_qconv.h5'.format(basedir, sample)
specfile = '{0}/dataspec/{1}.spec'.format(basedir, sample)

# Init foil correction LUT - bm02
foils = {0:1, 1:2.817, 2:7.934, 3:22.35, 4:62.95, 5:177.3, 6:499.4,
		 7:1407, 8:3963, 9:1.116e4, 10:3.144e4, 11:8.856e4, 12:2.494e5,
		 13:7.026e5, 14:1.979e6, 15:5.574e6}

# Load the specfile
sf = SpecFile('{0}/dataspec/{1}.spec'.format(basedir, sample))

# Calc offsets in angles from the tthsub scans #TODO use h5
dchi, dth = [], []
for scan, tth in zip(tthsub_no, tthsub):
	chi = np.round(sf[scan].motor_position_by_name('CHI'),decimals=3)
	th = np.round(sf[scan].motor_position_by_name('THETA'),decimals=3)
	dchi.append(chi-90)
	dth.append(th-tth/2)

print('\nAngular corrections loaded:\n')
print('Scan  |  dTheta  |  dChi')
print('-------------------------')
for scan, t, c in zip(scan_no, dth, dchi):
	print('{0}...{1}...{2}'.format(scan, t, c))
print('\n')

# Kappa goniometer:
# Theta = LH, Chi = RH, Phi = LH, Nu= RH, tth=LH ,
# x is along the beam, y is outboard, z is up.
qconv = xu.experiment.QConversion(['y-','x+','z-'],['z+','y-'],[1,0,0])

# Init the experiment class feeding it the geometry
hxrd = xu.HXRD([1,0,0], [0,0,1], en=en, qconv=qconv)

# Init the area detector
hxrd.Ang2Q.init_area('y+','z-',
					cch1=cch[0],
					cch2=cch[1],
					Nch1=npix[0],
					Nch2=npix[1],
					pwidth1=pixsize[0],
					pwidth2=pixsize[1],
					distance=distance,
					roi=roi)

with h5py.File(h5file_write, 'a') as h5f:

	for scan, t, c in zip(scan_no, dth, dchi):

		# Stop if scan exists and overwrite off
		if str(scan) in h5f.keys() and not overwrite:
			print('Scan {0} written and overwrite OFF - skipping'.format(scan))

		else:
			print('\n** Scan {0} being processed **'.format(scan))

			# Read motors and images from h5
			motors = dict()
			with h5py.File(h5file_read, 'r') as f:
				pos = f['{0}.1/instrument/positioners'.format(scan)]
				for motor in pos:
					motors[motor] = pos[motor].value
				raw_imgs = f['{0}.1/imgs'.format(scan)].value
				print('--> Images loaded'.format(scan))
				# correct for filters
				filterval = f['{0}.1/measurement/pfoil'.format(scan)].value
				for filt, index in zip(filterval, range(raw_imgs.shape[0])):
					mult = foils[filt]
					raw_imgs[index] = raw_imgs[index] * mult

			# Read temp - TODO use h5
			sf = SpecFile('{0}/dataspec/{1}.spec'.format(basedir, sample))
			temp = sf['{0}.1'.format(scan)].motor_position_by_name('euroc')
			print('--> Temp: {0}'.format(temp))

			# Set the angles
			angles = {'2THETA':0, 'THETA':0, 'nu':0, 'PHI':0, 'CHI':0}

			# Read the angles from the h5 file
			maxlen = 1
			for angle in angles:
				dset = motors[angle if angle is not "delta" else "del"]
				 # if it's 0 motor is still, if it's 1 motor is changing during scan
				if len(dset.shape):
					maxlen = max(maxlen, dset.shape[0])
				if angle == 'CHI':
					position = dset-90
				else:
					position = dset
				angles[angle] = position

			# Turn fixed angles into array
			for angle in angles:
				if np.isscalar(angles[angle]):
					angles[angle] = np.ones(maxlen, dtype=float) * angles[angle]

			# Calculate q space values
			qx, qy, qz = hxrd.Ang2Q.area(angles['THETA'],angles['CHI'],
										 0,angles['nu'],angles['2THETA'],
										 delta=[t, c, dphi, dnu, dtth])
			print('--> Size of loaded q and Int arrays: {0}'.format(qx.shape))

			# Calc max number of bins
			maxbins = []
			safemax = lambda arr: arr.max() if arr.size else 0
			for dim in (qx, qy, qz):
				maxstep = max((safemax(abs(np.diff(dim, axis=j))) for j in range(3)))
				maxbins.append(int(abs(dim.max()-dim.min())/maxstep))
			print('--> Maxbins is set to {0}'.format(max_bins))

			# Grid qspace
			if max_bins:
				nbins = maxbins
			gridder = xu.Gridder3D(*nbins)
			print('--> N of bins ={0}'.format(nbins))
			gridder(qx, qy, qz,
					raw_imgs.transpose(0,2,1)[:, roi[0]:roi[1], roi[2]:roi[3]])

			# Retrieve gridded values
			qxx, qyy, qzz = gridder.xaxis, gridder.yaxis, gridder.zaxis
			gint = gridder.data
			print('--> Finished gridding')

			# Write the gridded values to file
			if str(scan) in h5f.keys() and overwrite:
				del h5f[str(scan)]

			s = h5f.create_group(str(scan))
			s.create_dataset('qxx', data=qxx.astype('float32'), compression="gzip")
			s.create_dataset('qyy', data=qyy.astype('float32'), compression="gzip")
			s.create_dataset('qzz', data=qzz.astype('float32'), compression="gzip")
			s.create_dataset('gint',data=gint.astype('float32'), chunks=True, compression="gzip")
			s.create_dataset('temp', data=temp)

			print('--> Scan {0} written'.format(scan))
