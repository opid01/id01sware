#!/usr/bin/env python
#  Plot a KMAP as a function of rotation angle for a defined ROI
#    # needs to be agnostic to the type of kmap e.g mot1,mot2 
import os
import silx

#print silx.version
from silx.io import specfile
from id01lib.plot import Normalize
from id01lib import image
import pylab as pl
from matplotlib import colors
from matplotlib.ticker import LogFormatter
from scipy import ndimage


import collections
import argparse
import re

_motordef = dict(pix="adcY",
                 piy="adcX",
                 piz="adcZ")

_printmotors = ['eta', 'phi', 'del', 'nu', 'thx', 'thy', 'thz']


base = "/data/id01/inhouse/crichter/test/id01sware_testdata/spec"

# scans:number (neg==all)
scans = {
            "align_fast_00023.spec":range(2,100), 
            #"align_fast_00019.spec":[]
            #"align_fast_00020.spec":[], 
            #"align_fast_00019.spec":[]
            #"align_fast_00026.spec":[]
            #"align_fast_00028.spec":[0]+range(2,99)
            #"align_fast_00033.spec":[0]
            #"align_fast_00036.spec":["0.2"],
            #"PZO/KMAP_2018_04_19_051408/spec/KMAP_2018_04_19_051408_fast_00001.spec":["8.1"],
            #"/data/visitor/hc3496/id01/align_fast_00081.spec":range(0,99)
            #"/data/visitor/hc3496/id01/align_fast_00093.spec":range(0,99)
            #"/data/visitor/hc3496/id01/align_fast_00095.spec":[1,2]
            #"align_fast_00219.spec":["3.1","4.1"]
            #"align_fast_00345.spec":["2.1"]
            #"align_fast_00219.spec":["3.1"]
            #"PZO/KMAP_2018_04_20_034246/spec/KMAP_2018_04_21_051744_fast_00001.spec":["6.1"]
            #"/data/visitor/hc3496/id01/align_fast_00116.spec":[3]
            #"/data/visitor/hc3496/id01/align_fast_00121.spec":[0]
        }


monitor = "cnt1"
#plotcols = ["mpx4int", "mpx4ro1", "mpx4ro2", "mpx4ro3"]
plotcols = ["mpx4ro2"]
gamma = 0.5
norm = 'linear'
cmap = "jet"



######################################




formatter = None
if norm=="log":
    _norm = lambda vmin, vmax: colors.LogNorm(vmin, vmax)
elif norm=="power":
    _norm = lambda vmin, vmax: Normalize.Normalize(vmin=vmin, vmax=vmax, stretch="power", exponent=gamma, clip=False)
    formatter = LogFormatter(10, labelOnlyBase=False)
else:
    _norm = lambda vmin, vmax: colors.Normalize(vmin, vmax)




plotdata = collections.defaultdict(list)
VMIN = collections.defaultdict(lambda:  pl.inf)
VMAX = collections.defaultdict(lambda: -pl.inf)
for fname, scanno in scans.items():
    path = os.path.join(base, fname)
    print(path)
    sf = specfile.SpecFile(path)
    if not scanno:
        scanno = sf.keys()
    for num in scanno:
        try:
            sc = sf[num]
        except:
            break
        h = sc.scan_header_dict["S"].split()
        print(sc.number, h)
        motor1 = h[2]
        motor2 = h[6]
        arraysizey = int(h[5])
        arraysizex = int(h[9])
        pscan_shape = arraysizex, arraysizey

        if "align_fast" in fname and int(fname[-10:-5])>190: # the PI 100 100 10
            x = -sc.data_column_by_name("adcY")/1000 + sc.motor_position_by_name("thx")
        else:
            x = sc.data_column_by_name("adcY")/1000 + sc.motor_position_by_name("thx")

        y = sc.data_column_by_name("adcX")/1000 + sc.motor_position_by_name("thy")
        I0 = sc.data_column_by_name(monitor) if monitor in sc.labels else 1.
        for ii, col in enumerate(plotcols):
            roidata = sc.data_column_by_name(col)/I0
            try:
                x = x.reshape(pscan_shape)
            except:
                continue
            y = y.reshape(pscan_shape)
            roidata = roidata.reshape(pscan_shape)
            vmin, vmax = image.percentile_interval(roidata[roidata>0])
            VMIN[col] = min(vmin, VMIN[col])
            VMAX[col] = max(vmax, VMAX[col])

            plotdata[col].append((x, y, roidata))



for ii, col in enumerate(plotdata):
    fig = pl.figure(col)
    if not fig.axes:
        ax = fig.add_subplot(111)
    else:
        ax = fig.axes[0]
    norm = _norm(VMIN[col], VMAX[col])
    for (x,y,roidata) in plotdata[col]:
        im = ax.pcolormesh(x, y, roidata, cmap=cmap, norm=norm)
    pl.colorbar(im)
    ax.set_aspect('equal', adjustable="box-forced")
    ax.set_title(col)

pl.show()
