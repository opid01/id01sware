# -*- coding: utf-8 -*-
"""
ID01_polefigure.py

Generate a stereographic projection of a 3D diffraction pattern.

Adapted from J. Carnis script xrutils_polarplot.py by SJL

DONE: make a polar plot actually work - I seem incapable. I am now capable
DONE: make the sanity check go through the central pixel - COM 
DONE: or peak intensity or user defined
TODO: make previous point more robust
DONE: make a series of plots i.e. chose dr and take dr steps out to the edge of the array
TODO: when number of points is too low I get a segmentation fault
TODO: multiprocessors for the radius step

# theta is inclination from Z (qZ), if you use arcsin you get elevation from xy plane
# phi is measured form the x-axis such that y-axis is phi=+90

# in the polar plots if you were to superimpose an x/y axis, they correspond to qx,qy respectively

"""
import numpy as np
import xrayutilities as xu
import os
import scipy.io  # for savemat
import scipy.signal  # for medfilt2d
from  scipy.ndimage import center_of_mass
import matplotlib as mpl
import matplotlib.pyplot as plt
from scipy.interpolate import griddata
from scipy import interpolate
import sys
import h5py
import argparse
from id01lib.xrd import qconversion, geometries, detectors
import pdb
#pdb.set_trace()


##############################################################################
##############################################################################
# parameters for plotting
params = {'backend': 'ps',
          'axes.labelsize': 20,
          'font.size': 20,
          'legend.fontsize': 20,
          'axes.titlesize': 20,
          'xtick.labelsize': 20,
          'ytick.labelsize': 20,
          'text.usetex': False,
          'figure.figsize': (11, 9)}
mpl.rcParams.update(params)
# define a colormap
cdict = {'red':  ((0.0, 1.0, 1.0),
                  (0.11, 0.0, 0.0),
                  (0.36, 0.0, 0.0),
                  (0.62, 1.0, 1.0),
                  (0.87, 1.0, 1.0),
                  (1.0, 0.0, 0.0)),
         'green': ((0.0, 1.0, 1.0),
                   (0.11, 0.0, 0.0),
                   (0.36, 1.0, 1.0),
                   (0.62, 1.0, 1.0),
                   (0.87, 0.0, 0.0),
                   (1.0, 0.0, 0.0)),
         'blue': ((0.0, 1.0, 1.0),
                  (0.11, 1.0, 1.0),
                  (0.36, 1.0, 1.0),
                  (0.62, 0.0, 0.0),
                  (0.87, 0.0, 0.0),
                  (1.0, 0.0, 0.0))}
my_cmap = mpl.colors.LinearSegmentedColormap('my_colormap', cdict, 256)


parser = argparse.ArgumentParser(
            description='Script to generate pole figures from a 3D reciprocal space map.')

parser.add_argument('sample_name', type=str, nargs=1,
            help='sample identifier')

parser.add_argument('h5file', type=str, 
        help='Location of images.')
            
parser.add_argument('--scannos', nargs='+', type=int,
            help='Number of the scans in the spec file')
            
parser.add_argument('--offsets', nargs=7, type=float,  metavar=('eta', 'phi', 'del', 'nu', 'rhx', 'rhy','rhz'), default=[0,0,0,0,0,0,0],
            help='eta,phi,del,nu,rhx,rhy,rhz')

parser.add_argument('--bin', nargs=3, type=int, metavar=('Qz', 'Qy', 'Qx'),
            help='Qz,Qy,Qx')

parser.add_argument('--det_cenpixel', nargs=2, type=float, metavar=('x', 'y'),
            help='central pixel location in pixels x,y')
            
parser.add_argument('--det_distance', nargs=1, type=float, default= [1],
            help='detector distance')
            
parser.add_argument('--nrj', nargs=1, type=float, default = [8],
            help='x-ray energy keV')
            
parser.add_argument('--monitor', nargs=1, type=str, default=None,
            help='intensity monitor name (exp1/mon2...)')   

parser.add_argument('--outdir', nargs=1, type=str, default = "",
            help='directory to save the data to') 
            
parser.add_argument('--threshold', nargs=1, type=int, default = [0],
            help='intensity threshold in photons')  
            
parser.add_argument('--dr', nargs=1, type=float, default= [0.005], 
            help='delta radius to integrate along radius')  

parser.add_argument('--chkpeakpos',  dest='chkpeakpos', action='store_true', default=False,
            help='allow interaction to select centre')   
            

parser.add_argument('--scaleR', dest='scaleR', action='store_true', default=False,
            help='scale intensity as a function of R**2')   
            

parser.add_argument('--debug', dest='debug', action='store_true', default=False,
            help='some sanity checks')   
            
parser.add_argument('--roi', nargs=4, type=int,default=[0,516,0,516], 
            help='region of interest')


parser.add_argument('--cmap', type=str, default=my_cmap,
            help='pick your colourmap') 
            
#parser.add_argument('--log', type=bool, default=False,
#            help='log scale')  

parser.add_argument('--nolog', dest='log', action='store_false', default=True,
            help='log scale') 
                                         
#parser.add_argument('-m', '--monitor', type=str, default='cnt1',
#                    help='Name of counter for prim. beam normalization.')

args = parser.parse_args()

# TODO
#from multiprocessing import Pool
#from functools import partial
#import pdb


############################################
# user defined variables
############################################

################
#qconversion
################
sample = args.sample_name[0] #'align'
h5fn = args.h5file #"../../raw_data_BCDI.h5"  # rawdata in hdf5 format
scannos = args.scannos # list of scannos
nrj=args.nrj[0]
outdir=args.outdir
cmap=args.cmap

if args.outdir!="":
	try:
		os.mkdir(outdir)
	except:
		print("directory (outdir) cannot be created")
		sys.exit()

cen_pix_hor, cen_pix_vert = args.det_cenpixel # center channel of the detector
det_distance = args.det_distance[0] # from logbook, in meters
geometry = qconversion.ID01psic() # that's the default

# optional parameters
nav = None #[1,1] # reduce data: number of pixels to average in each detector direction
roi = args.roi#[cen_pix_vert-pixels,cen_pix_vert+pixels,cen_pix_hor-pixels,cen_pix_hor+pixels] #[0,516,0,516] # define a region of interest, 0-516 is the whole thing
bins = [-args.bin[0],-args.bin[1],-args.bin[2]] # in reciprocal space. Multiples of minimum. [-1,-1,-1] for highest resolution
monitor = args.monitor #"exp1" # try =None if normalization seems wrong

geometry.set_offsets(rhx=args.offsets[4], rhy=args.offsets[5], rhz=args.offsets[6],eta=args.offsets[0], phi=args.offsets[1],delta=args.offsets[2],nu=args.offsets[3]) # example to correct sample tilt if needed (rhx~-eta at phi=0)
# rhx: pitch, offset like eta at phi=0
# rhy: roll, offset like eta at phi=90

################
#polar plotting
################
threshold = args.threshold[0]  # photon threshold in detector counts
dr = args.dr[0] #0.02           # delta_q over which to integrate
qz_off = 0
user_interaction = args.chkpeakpos
scaleR = args.scaleR  # scale as a function of R**2 to favour streaks
sanityCHK = args.debug

def max_indices(arr, k):
    '''
    Returns the indices of the k first largest elements of arr
    (in descending order in values)
    '''
    assert k <= arr.size, 'k should be smaller or equal to the array size'
    arr_ = arr.astype(float)  # make a copy of arr
    max_idxs = []
    for _ in range(k):
        max_element = np.max(arr_)
        if np.isinf(max_element):
            break
        else:
            idx = np.where(arr_ == max_element)
        max_idxs.append(idx)
        arr_[idx] = -np.inf
    return max_idxs

def interpolate1D(y,pos):
        x=np.arange(0,y.shape[0])
        f=interpolate.interp1d(x,y)
        return float(f(pos))

def plot2Dprojections(QX,QY,QZ,DATA,outfile=None,plotCrosshair=True):
	# sanity check of chosen data
	fig, ax = plt.subplots(num=3, figsize=(24, 8), dpi=80, facecolor='w', edgecolor='k')
	plt.clf()
	plt.subplot(1, 3, 1)
	plt.contourf(QZ, QX, xu.maplog(DATA.sum(axis=1), 6, 1), 75, cmap=my_cmap)
	if plotCrosshair:
		plt.plot([min(QZ), max(QZ)], [0,0], color='k', linestyle='-', linewidth=2)
		plt.plot([0,0], [min(QX), max(QX)], color='k', linestyle='-', linewidth=2)
	plt.xlabel(r"Q$_z$ ($1/\AA$)")
	plt.xlim(min(QZ), max(QZ))
	plt.ylabel(r"Q$_x$ ($1/\AA$)")
	plt.ylim(min(QX), max(QX))
	if plotCrosshair:
		plt.title(r'$\sum Intensity(Q_{y})$')
	#plt.axis('scaled')
	plt.subplot(1, 3, 2)
	plt.contourf(QY, QX, xu.maplog(DATA.sum(axis=2), 6, 1), 75, cmap=my_cmap)
	if plotCrosshair:
		plt.plot([min(QY), max(QY)], [0,0], color='k', linestyle='-', linewidth=2)
		plt.plot([0,0], [min(QX), max(QX)], color='k', linestyle='-', linewidth=2)
	plt.xlabel(r"Q$_y$ ($1/\AA$)")
	plt.xlim(min(QY), max(QY))
	plt.ylabel(r"Q$_x$ ($1/\AA$)")
	plt.ylim(min(QX), max(QX))
	if plotCrosshair:
		plt.title(r'$\sum Intensity(Q_{z})$')
	#plt.axis('scaled')
	plt.subplot(1, 3, 3)
	plt.contourf(QY, QZ, xu.maplog(DATA.sum(axis=0).transpose(), 6, 1), 75, cmap=my_cmap)
	if plotCrosshair:
		plt.plot([0,0], [min(QZ), max(QZ)], color='k', linestyle='-', linewidth=2)
		plt.plot([min(QY), max(QY)], [0, 0], color='k', linestyle='-', linewidth=2)
	plt.xlabel(r"Q$_y$ ($1/\AA$)")
	plt.xlim(min(QY), max(QY))
	plt.ylabel(r"Q$_z$ ($1/\AA$)")
	plt.xlim(min(QZ), max(QZ))
	if plotCrosshair:
		plt.title(r'$\sum Intensity(Q_{x})$')
	#plt.axis('scaled')
	fig.tight_layout()
	if outfile:
		plt.savefig(outfile)
	else:
		plt.show()
	plt.close()
	
def sanity_check(arr,indexCenter=None,outfile=None):
	
	if not indexCenter:
		indexCenter=int(arr.shape/2.)
	else:
		indexCenter=map(int,indexCenter)
		
	#print(indexCenter)
	
	fig, ax = plt.subplots(num=1, figsize=(24, 8), dpi=80, facecolor='w', edgecolor='k')
	plt.clf()
	plt.subplot(1, 3, 1)
	plt.imshow(arr[:,:,indexCenter[2]])
	plt.subplot(1, 3, 2)
	plt.imshow(arr[:,indexCenter[1],:])
	plt.subplot(1, 3, 3)
	plt.imshow(arr[indexCenter[0],:,:])
	fig.tight_layout()

	if outfile:
		plt.savefig(outfile)
	else:
		plt.show()
	plt.close()

def sanity_check_fancy(QX,QY,QZ,DATA,radius_mean,dr,outfile=None,forpublication=False):
	# sanity check of chosen data
	fig, ax = plt.subplots(num=2, figsize=(24, 8), dpi=80, facecolor='w', edgecolor='k')
	plt.clf()
	plt.subplot(1, 3, 1)
	plt.contourf(QZ, QX, xu.maplog(DATA.sum(axis=1), 6, 1), 75, cmap=my_cmap)
	if forpublication:
		plt.plot([min(QZ), max(QZ)], [0,0], color='k', linestyle='-', linewidth=2)
		plt.plot([0,0], [min(QX), max(QX)], color='k', linestyle='-', linewidth=2)
	circle = plt.Circle((0, 0), radius_mean+dr, color='0', fill=False, linestyle='dotted',linewidth=2.)
	fig.gca().add_artist(circle)
	circle = plt.Circle((0, 0), radius_mean-dr, color='0', fill=False, linestyle='dotted',linewidth=2.)
	fig.gca().add_artist(circle)
	plt.xlabel(r"Q$_z$ ($1/\AA$)")
	plt.ylabel(r"Q$_x$ ($1/\AA$)")
	if forpublication:
		plt.title(r'$\sum Intensity(Q_{y})$')
	#plt.axis('scaled')
	plt.subplot(1, 3, 2)
	plt.contourf(QY, QX, xu.maplog(DATA.sum(axis=2), 6, 1), 75, cmap=my_cmap)
	if forpublication:
		plt.plot([min(QY), max(QY)], [0,0], color='k', linestyle='-', linewidth=2)
		plt.plot([0,0], [min(QX), max(QX)], color='k', linestyle='-', linewidth=2)
	circle = plt.Circle((0, 0), radius_mean+dr, color='0', fill=False, linestyle='dotted',linewidth=2.)
	fig.gca().add_artist(circle)
	circle = plt.Circle((0, 0), radius_mean-dr, color='0', fill=False, linestyle='dotted',linewidth=2.)
	fig.gca().add_artist(circle)
	plt.xlabel(r"Q$_y$ ($1/\AA$)")
	plt.ylabel(r"Q$_x$ ($1/\AA$)")
	if forpublication:
		plt.title(r'$\sum Intensity(Q_{z})$')
	#plt.axis('scaled')
	plt.subplot(1, 3, 3)
	plt.contourf(QY, QZ, xu.maplog(DATA.sum(axis=0).transpose(), 6, 1), 75, cmap=my_cmap)
	if forpublication:
		plt.plot([0,0], [min(QZ), max(QZ)], color='k', linestyle='-', linewidth=2)
		plt.plot([min(QY), max(QY)], [0, 0], color='k', linestyle='-', linewidth=2)
	circle = plt.Circle((0, 0), radius_mean+dr, color='0', fill=False, linestyle='dotted',linewidth=2.)
	fig.gca().add_artist(circle)
	circle = plt.Circle((0, 0), radius_mean-dr, color='0', fill=False, linestyle='dotted',linewidth=2.)
	fig.gca().add_artist(circle)
	plt.xlabel(r"Q$_y$ ($1/\AA$)")
	plt.ylabel(r"Q$_z$ ($1/\AA$)")
	if forpublication:
		plt.title(r'$\sum Intensity(Q_{x})$')
	#plt.axis('scaled')
	fig.tight_layout()
	#plt.show()
	
	if outfile:
		plt.savefig(outfile)
	else:
		plt.show()
	plt.close()

def gen_polar_plot(radius_mean,dr,scanno, QX,QY,QZ,QXCENTER,QYCENTER,QZCENTER,qR,qPhi,qTheta,DATA):
	print(radius_mean,scanno)
	########################################
	# remove all points within bound set by a radius+dr
	########################################
	mask0 = np.logical_and((qR < (radius_mean+dr)), (qR > (radius_mean-dr)))
	#sanity check
	if sanityCHK:
		sanity_check(mask0,center_of_mass(mask0),outfile="%i_%.3f_sanity_mask0.png"%(scanno,radius_mean))
		
	sanity_check_fancy(QX,QY,QZ,intensity_norm,radius_mean,dr,outfile="%i_%.3f_sanity_fancy.png"%(scanno,radius_mean))

	#sanity_check_fancy(QX,QY,QZ,np.where(mask0==True,0,DATA))

	u=qPhi[mask0]
	v=qTheta[mask0]
	int_temp = DATA[mask0]
	u_grid, v_grid = np.mgrid[-181:181:180j, -1:181:180j]
	int_grid = griddata((u, v), int_temp, (u_grid, v_grid), method='linear')
	int_grid = np.nan_to_num(int_grid)
	#pdb.set_trace()
	if int_grid.max()!=0:
		int_grid = abs(int_grid / int_grid[int_grid > 0].max() * 10000)  # normalize to known max value for easier plotting
		if args.log:
			int_grid = xu.maplog(abs(int_grid),10000,1)			
		fig=plt.figure(num=4)
		plt.clf()
		#pdb.set_trace()
		plt.contourf(u_grid, v_grid, int_grid, np.arange(.1,int_grid.max()*1.1, int_grid.max()*1.1/150.),cmap=cmap)
		plt.colorbar()
		plt.xlabel(r'$\phi$',fontsize=20)
		plt.ylabel(r'$\theta$',fontsize=20)
		#plt.title(r'Probed |$\delta Q$| (%.4f : %.4f )'%(radius_mean-dr,radius_mean+dr))
		plt.savefig("%i_%.3f_%.3f_contourf.png"%(scanno,radius_mean,dr))
		plt.close()

		# north pole i.e qz positive
		fig=plt.figure(num=5,figsize=(16, 8), dpi=80, facecolor='w', edgecolor='k')
		plt.clf()
		ax1=plt.subplot(1,2,1,projection="polar")

		#ax1.set_title(r'North Pole:  Probed |$\delta Q$| (%.4f : %.4f )'%(radius_mean-dr,radius_mean+dr))		
		#plt.pcolormesh(np.deg2rad(u_grid[:,:90]),v_grid[:,:90], int_grid[:,:90],vmin=0,vmax=int_grid.max())
		plt.contourf(np.deg2rad(u_grid[:,:90]),v_grid[:,:90], int_grid[:,:90],np.arange(.1,int_grid[:,:90].max()*1.1, int_grid.max()*1.1/150.),cmap=cmap)

		ax1.set_rlim(0,90)
		ax1.grid(lw=1)
		#plt.savefig("%i_%.3f_%.3f_contourf_pole_north.png"%(scanno,radius_mean,dr))
		# south pole i.e qz negative
		#plt.clf()
		ax2=plt.subplot(1,2,2,projection="polar")
		#ax2.set_title(r'South Pole')
		tmp_u_grid=np.flip(u_grid,axis=0)
		#tmp_v_grid=v_grid
		tmp_v_grid=np.flip(v_grid,axis=1)
		#tmp_u_grid=u_grid
		#plt.pcolormesh(np.deg2rad(tmp_u_grid[:,90:]),tmp_v_grid[:,90:], int_grid[:,90:],vmin=0,vmax=int_grid.max())
		plt.contourf(np.deg2rad(tmp_u_grid[:,90:]),tmp_v_grid[:,90:], int_grid[:,90:],np.arange(.1,int_grid[:,90:].max()*1.1, int_grid.max()*1.1/150.),cmap=cmap)
		plt.tight_layout()
		ax2.set_rlim(0,90)
		ax2.grid(lw=1)
		#plt.colorbar()
		plt.savefig("%i_%.3f_%.3f_contourf_polar.png"%(scanno,radius_mean,dr))
		plt.close()

for scanno in scannos:
	print("Scan Number: %i"%scanno)
	
	with h5py.File(h5fn, "a") as h5f:
		qx, qy, qz, gint = qconversion.scan_to_qspace_h5(
										h5f[sample]["%i.1"%scanno],
										cen_pix=[cen_pix_hor,cen_pix_vert],
										distance=det_distance,
										nbins=bins,
										medfilter=False,
										detector=detectors.MaxiPix(), #detectors.Eiger2M()
										geometry=geometry,
										energy=nrj,
										monitor=monitor,
										roi=roi,
										Nav=nav,
										)
	
	
	intensity=gint
	intensity[intensity <= threshold] = 0   # photon threshold
	qx_I, qy_I, qz_I = qy, qx, qz  # invert x,y

	qCOM=center_of_mass(intensity)
	maxind=max_indices(intensity,1)
	qzCOM = interpolate1D(qz_I,qCOM[2])  # COM in qz
	qxCOM = interpolate1D(qx_I,qCOM[1])  # COM in qy
	qyCOM = interpolate1D(qy_I,qCOM[0])  # COM in qx
	print("Center of mass [qx, qy, qz]: [%.5f,%.5f,%.5f]"%(qxCOM, qyCOM, qzCOM))
	print("Peak intensity [qx, qy, qz]: [%.5f,%.5f,%.5f]"%(qy[maxind[0][1][0]],qx[maxind[0][0][0]],qz[maxind[0][2][0]]))
	qxCOM,qyCOM,qzCOM=[qy[maxind[0][1][0]],qx[maxind[0][0][0]],qz[maxind[0][2][0]]]
	#print(len(qz),len(qx),gint.sum(axis=1).shape)
	#print(len(qy),len(qx),gint.sum(axis=2).shape)
	#print(len(qy),len(qz),gint.sum(axis=0).shape)
	
	while user_interaction:
		plot2Dprojections(qx_I-qxCOM,qy_I-qyCOM,qz_I-qzCOM,intensity.transpose(1,0,2))
		raw_center=raw_input("Type your desired center of Q space: format list '[qx,qy,qz]' \n   or 'q' to accept your changes\n")
		if raw_center=='q':
			break
		else:
			raw_center=eval(raw_center)
		qxCOM+=raw_center[0]
		qyCOM+=raw_center[1]
		qzCOM+=raw_center[2]
		print(qxCOM,qyCOM,qzCOM)

	plot2Dprojections(qx_I-qxCOM,qy_I-qyCOM,qz_I-qzCOM,intensity.transpose(1,0,2),outfile=outdir+"%i_2d_projs.png"%scanno, plotCrosshair=False)
		
	qx1,qy1,qz1=np.meshgrid(qy_I,qx_I,qz_I) # invert x,y
	_x, _y, _z = qx1-qyCOM, qy1-qxCOM, qz1-qzCOM

	# magnitude of radius
	# theta is inclination from Z (qZ), if you use arcsin you get elevation from xy plane
	# phi is measured form the x-axis such that y-axis is phi=+90
	qR = np.sqrt(_x**2 + _y**2 + _z**2) # radial
	qTheta = np.degrees(np.arccos(_z/qR)) #rot arount qy (first)
	qPhi = np.degrees(np.arctan2(_y,_x)) #rot around qx (second)

	# Scale as a function of Radius - need to scale properly here pixel units is probably the best
	delta_qx = abs(qx[1]-qx[0])
	delta_qy = abs(qy[1]-qy[0])
	delta_qz = abs(qz[1]-qz[0])
	mfact=(1./delta_qx,1./delta_qy,1./delta_qz)

	volume_R = np.sqrt(((qx1 - qyCOM)*mfact[0])**2 + ((qy1 - qxCOM)*mfact[1])**2 + ((qz1 - (qzCOM+qz_off))*mfact[2])**2)
	if scaleR:
			intensity_norm=intensity.transpose(1,0,2)/(1/volume_R**2)
	else:
			intensity_norm=intensity.transpose(1,0,2)
	#pdb.set_trace()		
	for radius_mean in np.arange(0.01,qR.max()/2.,dr*2):
		#sanity check
		if sanityCHK: 
			#sanity_check(volume_R,indexCenter=(qxCOM,qyCOM,qzCOM),outfile="%i_%.3f_sanity_volume_R.png"%(scanno,radius_mean))
			sanity_check(volume_R,indexCenter=center_of_mass(volume_R),outfile=outdir+"%i_%.3f_sanity_volume_R.png"%(scanno,radius_mean))
		gen_polar_plot(radius_mean,dr,scanno,qx_I-qxCOM,qy_I-qyCOM,qz_I-qzCOM,qxCOM,qyCOM,qzCOM,qR,qPhi,qTheta,intensity_norm)
	"""
	partial_gen_polar_plot=partial(gen_polar_plot(radius_mean,dr=dr,scanno=scanno,QX=qx_I-qxCOM,QY=qy_I-qyCOM,QZ=qz_I-qzCOM,qR=qR,qPhi=qPhi,qTheta=qTheta,DATA=intensity_norm))
	gen_polar_plot_set()
	if __name__ == '__main__':
		with Pool(10) as p:
			p.map(f, np.arange(0.01,qR.max(),dr*2).tolist())
	"""
