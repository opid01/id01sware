



if __name__=="__main__":
    import matplotlib.pyplot as plt
    import numpy as np
    from scipy import ndimage
    from id01lib.plot.interactive import Overlay
    N = 25


    a = np.zeros((N,N))
    a[N//2,N//2] = 1.
    a = ndimage.gaussian_filter(a, N/10.)
    b = a.copy()
    c = a.copy()

    # just a test
    data = np.array((a, b, c))

    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.set_xlabel("test1")
    ax.set_ylabel("test2")

    tracker = Overlay(5, ax, data, norm="linear")

    try:
        plt.show()
    except AttributeError: # plot closed
        pass

