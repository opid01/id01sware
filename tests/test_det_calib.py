# -*- coding: utf-8 -*-
from id01lib.id01h5 import id01SpecH5
from id01lib import spec

specfile = "/data/id01/inhouse/crichter/data/hc3328_GeSn_mesa/spec/md09b.spec"
scanno = "3.1"



sh5 = id01SpecH5(specfile)
scan = sh5[scanno]

calibration = spec.get_det_calib(scan, rebin=2, upsample=5)
