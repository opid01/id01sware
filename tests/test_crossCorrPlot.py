# Calc correlation coefficients between consecutive images
#
#   i.e. a timescan
#
#   port to multiple processors to speed it up
#

import numpy as np
import h5py as h5
import pylab as pl
import os

def correlation_coefficient(array1, array2):
    product = np.mean((array1 - array1.mean()) * (array2 - array2.mean()))
    stds = array1.std() * array2.std()
    if stds == 0:
        return 0
    else:
        product /= stds
        return product

# 1D plot track correlation coefficient as a function of new image

def correlation_1D(ref_image,all_images):
	corr_coeffs=[correlation_coefficient(ref_image,all_images[ii]) for ii in range(all_images.shape[0])]
	return corr_coeffs #[::-1]
	
# 2D plot track correlation coefficient relative to all images
def correlation_matrix(data):
	corr_coeffs=np.zeros((data.shape[0],data.shape[0]))

	for jj in range(data.shape[0]):
		ref_image=data[jj]
		print("%i / %i "%(jj,data.shape[0]))
		corr_coeffs[-jj-1]=np.array([correlation_coefficient(ref_image,data[ii]) for ii in range(data.shape[0])])
	
	return corr_coeffs
        

data_fn = '/data/id01/inhouse/UPBLcomm/blc11350/analysis/rawdata.h5'
data_h5_path = '/align/8.1/instrument/detector_0/data'
time_h5_path = '/align/8.1/measurement/Time'
# take a timescan / or images coming through the detector device server

rawdata=h5.File(data_fn,'r')

data=rawdata[data_h5_path].value
ref_image=data[0,:,:]

time=rawdata[time_h5_path]

fig = pl.figure()
pl.plot(time,correlation_1D(ref_image,data))
pl.xlabel("time(s)")
pl.ylabel("correlation_coefficient")
pl.show()

fig.clf()
# add labels

fig = pl.figure()
ax = fig.add_subplot(111)
image=correlation_matrix(data)
im=ax.imshow(image)
im.set_extent((time[0], time[-1], time[0], time[-1]))

ax.set_xlabel("time(s)")
ax.set_ylabel("time(s)")

pl.show()



# keep appending images to an array - best if this is in a hdf5 file and not memory

# do not forget the timestamp and add axes, perhpas a pcolormesh in case of non uniform steps in time.

# both timescan and limatake compatible

# use the array generated previously to create a 2D plot but append result to hdf5 file - i.e. do not recalculate everything every time
        
