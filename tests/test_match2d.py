



if __name__=="__main__":
    import matplotlib.pyplot as plt
    import numpy as np
    #from scipy import ndimage
    from id01lib.plot.interactive import Match2d


    x1 = np.linspace(-5,5,101)
    y1 = np.linspace(-5,5,101)
    x1, y1 = np.meshgrid(x1, y1)

    x2 = np.linspace(-4,4,101)
    y2 = np.linspace(-6,6,101)
    x2, y2 = np.meshgrid(x2, y2)

    x3 = np.linspace(-3,7,81)
    y3 = np.linspace(-3,7,91)
    x3, y3 = np.meshgrid(x3, y3)


    d1 = np.exp(- x1**2      -  y1**2     ) *  x1     * y1
    d2 = np.exp(- x2**2      - (y2-0.5)**2) *  x2     *(y2-0.5)
    d3 = np.exp(-(x3-0.5)**2 -  y3**2     ) * (x3-0.5)* y3

    f = plt.figure(FigureClass=Match2d, n_imgs=3, maxshift=10, figsize=(15,7))


    f.plot2d(x1, y1, d1, cmap=plt.cm.cubehelix)
    f.plot2d(x2, y2, d2, cmap=plt.cm.cubehelix)
    f.plot2d(x3, y3, d3, cmap=plt.cm.cubehelix)

    #f.addResetButton()
    f.addCursor()

    plt.show()


    fig, ax = plt.subplots(1,3)
    for j, data in enumerate((d1,d2,d3)):
        newx, newy, newval = f.interpolate(j, data) # can be used for saving / further processing

        ax[j].pcolormesh(newx, newy, newval, vmin=0,vmax=np.nanmax(newval))
        #ax[j].imshow(newval)


    plt.show()


