from PyTango import DeviceProxy
import struct
import numpy as np

tangoDev_limaccd=DeviceProxy("id01/limaccd/mpx_1x4")
tangoDev_limaccd=DeviceProxy("id01/limaccds/eiger2M")
_,raw_data=tangoDev_limaccd.video_last_image

VIDEO_HEADER_FORMAT = '!IHHqiiHHHH'
minimumPacketSize = struct.calcsize(VIDEO_HEADER_FORMAT)

(magic, header_version,
                 image_mode, image_frameNumber,
                 image_width, image_height,
                 endian, header_size,
                 pad0, pad1) = struct.unpack(VIDEO_HEADER_FORMAT,raw_data[:minimumPacketSize])


_limaVideoType2Qub = {0 : np.uint8,
                                   1 : np.uint16,
                                   2 : np.int32,
                                   3 : np.int64}

mode=_limaVideoType2Qub.get(image_mode)

(magic, header_version,
                 image_mode, image_frameNumber,
                 image_width, image_height,
                 endian, header_size,
                 pad0, pad1) = struct.unpack(VIDEO_HEADER_FORMAT,raw_data[:minimumPacketSize])
print(magic, header_version,
                 image_mode, image_frameNumber,
                 image_width, image_height,
                 endian, header_size,
                 pad0, pad1)

data = np.fromstring(raw_data[minimumPacketSize:],dtype=mode)
data.shape = image_height,image_width
