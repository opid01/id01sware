
import pylab as pl

from scipy import ndimage
from id01lib import image

im1 = pl.load('/data/id01/inhouse/crichter/test/id01sware_testdata/im1.npy').astype(float)
im2 = pl.load('/data/id01/inhouse/crichter/test/id01sware_testdata/im2.npy').astype(float)

im0 = im1.copy()
im0b = im2.copy()
im1 /= im1.max()
im2 /= im2.max()

im1 = ndimage.gaussian_laplace(im1, 6.)
im2 = ndimage.gaussian_laplace(im2, 6.)




fig, ax = pl.subplots(2,3, sharex=True, sharey=True, figsize=(14,5.5))
ax[0,0].imshow(im0)
ax[0,0].set_title("reference", fontsize=11)
ax[1,0].imshow(im0b)
ax[1,0].set_title("shifted image", fontsize=11)



### SIFT
res = image.get_shift(im1, im2, method="sift", devicetype="CPU")
cc = res["shift"][0]
imshift = ndimage.shift(im0b, -cc)
ax[0,1].imshow(imshift)
ax[0,1].set_title("SIFT\n" + "shift detected: %.2f %.2f"%tuple(cc), fontsize=11)
diff = imshift-im0
ax[0,2].imshow(diff)
ax[0,2].set_title("difference: %.4g"%abs(diff).sum(), fontsize=11)



### PHASE CORRELATION
res = image.get_shift(im1, im2, method="correlation", upsample=100)
cc = res["shift"][0]
imshift = ndimage.shift(im0b, -cc)
ax[1,1].imshow(imshift)
ax[1,1].set_title("phase correlation\n" + "shift detected: %.2f %.2f"%tuple(cc), fontsize=11)
diff = imshift-im0
ax[1,2].imshow(diff)
ax[1,2].set_title("difference: %.4g"%abs(diff).sum(), fontsize=11)


pl.show()