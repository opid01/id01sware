#!/usr/bin/env python
# 20160727
# make_tiffs.py
# Generate a set of tiffs from the last scan
#
# 20160727  SL first iteration
# 20160730  float32 issue resolved for tiffs
# 
################### le code #################

from SpecClient import SpecVariable
from PyMca5 import specfilewrapper as sfwr
from PyMca5.PyMca import EdfFile
import tifffile
import os
import sys
import numpy as np

specname = 'nano3:psic_nano'
ID01LIMA_PAR = SpecVariable.SpecVariable("ID01LIMA_PAR",specname)
_ID01LIMA_PAR = ID01LIMA_PAR.getValue()
DATAFILE = SpecVariable.SpecVariable("DATAFILE",specname)
_DATAFILE = DATAFILE.getValue()
SCAN_N = SpecVariable.SpecVariable("SCAN_N",specname)
_SCAN_N = SCAN_N.getValue()
specfile = _DATAFILE 
imagedir = _ID01LIMA_PAR["mpx4"]["saving_directory"]
image_prefix = _ID01LIMA_PAR["mpx4"]["saving_prefix"]
image_suffix = _ID01LIMA_PAR["mpx4"]["saving_suffix"]
sigfig = '%.5i'
savetiff = image_prefix
tiffdir = imagedir+'tiff_%.5i'%_SCAN_N

sf = sfwr.Specfile(specfile)
tmp = sf[_SCAN_N-1]
det_im_id = 'mpx4inr'

im_nos = [tmp.data()[(tmp.alllabels()).index(det_im_id)][0],tmp.data()[(tmp.alllabels()).index(det_im_id)][-1]]
im_nos_arr = np.arange(im_nos[0],im_nos[1]+1,1)

if os.path.isdir(tiffdir):
    tiffdir = os.path.abspath(tiffdir)
    print "path exists for tiff files: ", tiffdir
else:
    try:
        os.mkdir(tiffdir)
        tiffdir = os.path.abspath(tiffdir)
    except:
        print tiffdir, 'cannot be created'

for i,im_no in enumerate(im_nos_arr):
    efile = imagedir+image_prefix+sigfig%im_no+image_suffix
    e = EdfFile.EdfFile(efile)
    
    tifffile.imsave(tiffdir+'/'+savetiff+'%05d.tiff'%im_no,e.GetData(0))
    
    #if i==0:
    #    zstack = np.zeros((im_nos_arr.shape[0], e.GetData(0).shape[0], e.GetData(0).shape[1]))
    #
    #zstack[i] = e.GetData(0)

#tifffile.imsave(tiffdir+'/'+'zstack_%05d.tiff'%_SCAN_N,np.float32(e.GetData(0)))  

print "tiffs generated for scan %i"%_SCAN_N
print ".. press any key (except spacebar) to continue.."
sys.exit()
"""    
def make_tiffs '{
  # generate a set of tiffs of the last scan in a folder tiff_XXX
  u python /users/blissadm/local/python/make_tiffs.py & 
}'
"""
