#!/usr/bin/env python
#  Plot a KMAP as a function of rotation angle for a defined ROI
# 
import os
import silx

#print silx.version
from silx.io import specfile
from id01lib.plot import Normalize
from id01lib import image
import pylab as pl
from matplotlib import colors
from matplotlib.ticker import LogFormatter
from scipy import ndimage
import matplotlib
import numpy

##define a colormap
cdict = {'red':  ((0.0, 1.0, 1.0),
                  (0.11, 0.0, 0.0),
                  (0.36, 0.0, 0.0),
                  (0.62, 1.0, 1.0),
                  (0.87, 1.0, 1.0),
                  (1.0, 0.0, 0.0)),
          'green': ((0.0, 1.0, 1.0),
                  (0.11, 0.0, 0.0),
                  (0.36, 1.0, 1.0),
                  (0.62, 1.0, 1.0),
                  (0.87, 0.0, 0.0),
                  (1.0, 0.0, 0.0)),
          'blue': ((0.0, 1.0, 1.0),
                  (0.11, 1.0, 1.0),
                  (0.36, 1.0, 1.0),
                  (0.62, 0.0, 0.0),
                  (0.87, 0.0, 0.0),
                  (1.0, 0.0, 0.0))}
my_cmap = matplotlib.colors.LinearSegmentedColormap('my_colormap',cdict,256)


import collections
import argparse
import re

_motordef = dict(pix="adcY",
                 piy="adcX",
                 piz="adcZ")

_printmotors = ['eta', 'phi', 'del', 'nu', 'thx', 'thy', 'thz']

# /data/visitor//hc3802/id01/spec/2018_10_23_112335_alignment.spec
base = "/data/visitor/ma4162/id01/spec/"

########### scans:number (none==all)
scans = {
            #"2018_08_29_183620_SrTiO3_fast_00026.spec":range(11), 
            #"/data/visitor/ma4166/id01/spec/2018_08_29_183620_SrTiO3_fast_00041.spec":range(55),
            #"2018_08_30_144647_SrTiO3_fast_00199.spec":range(11)+[12],
            #"2018_08_30_144647_SrTiO3_fast_00493.spec":range(111),
            #"2018_08_30_144647_SrTiO3_fast_00507.spec":["0.2", "1.2", "3.1"],
            #"2018_08_30_144647_SrTiO3_fast_00511.spec":range(5,55),
            #"2018_08_30_144647_SrTiO3_fast_00511.spec":["0.2", "1.3", "2.4", "3.2", "4.2", "5.2", "6.2"],
            #"align_fast_00019.spec":[]
            #"align_fast_00020.spec":[], 
            #"align_fast_00019.spec":[]
            #"align_fast_00026.spec":[]
            #"align_fast_00028.spec":[0]+range(2,99)
            #"align_fast_00033.spec":[0]
            #"align_fast_00036.spec":["0.2"],
            #"PZO/KMAP_2018_04_19_051408/spec/KMAP_2018_04_19_051408_fast_00001.spec":["8.1"],
            #"/data/visitor/hc3496/id01/align_fast_00081.spec":range(0,99)
            #"/data/visitor/hc3496/id01/align_fast_00093.spec":range(0,99)
            #"/data/visitor/hc3496/id01/align_fast_00095.spec":[1,2]
            #"align_fast_00219.spec":["3.1","4.1"]
            #"align_fast_00345.spec":["2.1"]
            #"align_fast_00219.spec":["3.1"]
            #"PZO/KMAP_2018_04_20_034246/spec/KMAP_2018_04_21_051744_fast_00001.spec":["6.1"]
            #"/data/visitor/hc3496/id01/align_fast_00116.spec":[3]
            #"/data/visitor/hc3496/id01/align_fast_00121.spec":[0]
            #"2018_10_23_112335_alignment_fast_00093.spec":["0.1","1.1","2.1","3.1","5.1","6.1","8.2"],
            #"2018_10_23_112335_alignment_fast_01975.spec":["0.1","1.1","2.1","3.1"],           
            #
            "2018_11_11_152506Pt_fast_00050.spec":["1.1"],
        }


#monitor = "cnt1"
monitor = None
#plotcols = ["mpx4int", "mpx4ro1", "mpx4ro2", "mpx4ro3"]
plotcols = ["ei2mro1"]
gamma = 0.5
norm = ''
cmap = "jet"



######################################




formatter = None
if norm=="log":
    _norm = lambda vmin, vmax: colors.LogNorm(vmin, vmax)
elif norm=="power":
    _norm = lambda vmin, vmax: Normalize.Normalize(vmin=vmin, vmax=vmax, stretch="power", exponent=gamma, clip=False)
    formatter = LogFormatter(10, labelOnlyBase=False)
else:
    _norm = lambda vmin, vmax: colors.Normalize(vmin, vmax)




plotdata = collections.defaultdict(list)
VMIN = collections.defaultdict(lambda:  pl.inf)
VMAX = collections.defaultdict(lambda: -pl.inf)
for fname, scanno in scans.items():
    path = os.path.join(base, fname)
    print(path)
    sf = specfile.SpecFile(path)
    if not scanno:
        scanno = sf.keys()
    for num in scanno:
        try:
            sc = sf[num]
        except:
            break
        h = sc.scan_header_dict["S"].split()
        print(sc.number, h)
        motor1 = h[2]
        motor2 = h[6]
        range1 = map(float, h[3:6])
        range2 = map(float, h[7:10])
        range1 = pl.linspace(*range1)
        range2 = pl.linspace(*range2)
        ranges=dict()
        ranges[motor1] = range1
        ranges[motor2] = range2
        arraysizey = int(h[5])
        arraysizex = int(h[9])
        pscan_shape = arraysizex, arraysizey
        title = sc.scan_header_dict["S"]

        ### This may change...
        y = -(ranges["pix"]-50)/1000 + sc.motor_position_by_name("thx")
        x = (ranges["piy"]-50)/1000 + sc.motor_position_by_name("thy")
        print(x,y)
        #print y.mean()
        x, y = pl.meshgrid(y,x)
        #x = sc.data_column_by_name("adcY")/1000 + sc.motor_position_by_name("thx")
        #y = sc.data_column_by_name("adcX")/1000 + sc.motor_position_by_name("thy")
        ###

        I0 = sc.data_column_by_name(monitor) if monitor in sc.labels else 1.
        for ii, col in enumerate(plotcols):
            roidata = sc.data_column_by_name(col)/I0
            if not roidata.size:
                continue
            #if not (roidata>0).any():
            #    continue
            try:
                x = x.reshape(pscan_shape)
            except:
                continue
            y = y.reshape(pscan_shape)
            #roidata = roidata.reshape(pscan_shape)
            roidata.resize(pscan_shape)
            #roidata /= pl.median(roidata)
            #roidata.T
            vmin, vmax = image.percentile_interval(roidata[roidata>0], 0, 100)
            VMIN[col] = min(vmin, VMIN[col])
            VMAX[col] = max(vmax, VMAX[col])

            plotdata[col].append((x, y, roidata))


pl.close('all')
for ii, col in enumerate(plotdata):
    fig = pl.figure(col)
    if not fig.axes:
        ax = fig.add_subplot(111)
    else:
        ax = fig.axes[0]
    #norm = _norm(4, .8e1)
    for (x,y,roidata) in plotdata[col]:
        #im = ax.pcolormesh(y, x, numpy.log10(roidata), cmap=my_cmap,vmin=0.4,vmax=1.2)#,vmin=0.8,vmax=1.4)#, norm=norm)
        im = ax.pcolormesh(y, x, numpy.log10(roidata))#,vmin=0.8,vmax=1.4)#, norm=norm)
    pl.colorbar(im)
    pl.xlabel("thy (mm)")
    pl.ylabel("thx (mm)")
    ax.set_aspect('equal', adjustable="box-forced")
    ax.set_title(col)

#pl.show()

'''
pl.plot(1.08,0.6999,'ro')
pl.hold(True)
pl.plot(1.206,0.6119,'ro')
pl.hold(True)
pl.plot(1.2258,0.6718,'go')
pl.hold(True)
pl.plot(1.9231,-2.3214,'ro')
'''
pl.show()
