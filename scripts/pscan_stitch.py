#!/usr/bin/env python
#  stitch multiple kmaps together
#  PI agnostic
#  could be silx plot2D integrated or slider for cmap variation
#  make it the same as pscan_live - integrate into virtual microscope

import os
import silx

#print silx.version
from silx.io import specfile
from id01lib.plot import Normalize
from id01lib import image
import pylab as pl
from matplotlib import colors
from matplotlib.ticker import LogFormatter
from scipy import ndimage


import collections
import argparse
import re

_motordef = dict(pix="adcY",
                 piy="adcX",
                 piz="adcZ")
_hexmotoraxis = dict(pix="thx",
                 piy="thy",
                 piz="thz",)
_pistroke = dict(pix=100,
                 piy=100,
                 piz=20)
_pisense = dict(pix=-1,
                 piy=1,
                 piz=1)


_printmotors = ['eta', 'phi', 'del', 'nu', 'thx', 'thy', 'thz']

# /data/visitor//hc3802/id01/spec/2018_10_23_112335_alignment.spec
base = "/data/visitor/hc3802/id01/spec/"

########### scans:number (none==all)
scans = {
            #"2018_08_30_144647_SrTiO3_fast_00511.spec":range(5,55),
            #"2018_10_23_112335_alignment_fast_00093.spec":["0.1","1.1","2.1","3.1","6.1","7.1","8.1"],
            "2018_10_23_112335_alignment_fast_00640.spec":["0.1","1.1","2.1"],           
        }

monitor = "cnt1"
#plotcols = ["mpx4int", "roi1"]
plotcols = ["mpx4int"]
gamma = 0.5
norm = 'log'
cmap = "jet"



######################################




formatter = None
if norm=="log":
    _norm = lambda vmin, vmax: colors.LogNorm(vmin, vmax)
elif norm=="power":
    _norm = lambda vmin, vmax: Normalize.Normalize(vmin=vmin, vmax=vmax, stretch="power", exponent=gamma, clip=False)
    formatter = LogFormatter(10, labelOnlyBase=False)
else:
    _norm = lambda vmin, vmax: colors.Normalize(vmin, vmax)


import pdb

plotdata = collections.defaultdict(list)
VMIN = collections.defaultdict(lambda:  pl.inf)
VMAX = collections.defaultdict(lambda: -pl.inf)
for fname, scanno in scans.items():
    path = os.path.join(base, fname)
    print(path)
    sf = specfile.SpecFile(path)
    if not scanno:
        scanno = sf.keys()
    for num in scanno:
        try:
            sc = sf[num]
        except:
            break
        h = sc.scan_header_dict["S"].split()
        print(sc.number, h)
        motor1 = h[2]
        motor2 = h[6]
        range1 = map(float, h[3:6])
        range2 = map(float, h[7:10])
        range1 = pl.linspace(*range1)
        range2 = pl.linspace(*range2)
        ranges=dict()
        ranges[motor1] = range1
        ranges[motor2] = range2
        arraysizey = int(h[5])
        arraysizex = int(h[9])
        pscan_shape = arraysizex, arraysizey
        title = sc.scan_header_dict["S"]
        #pdb.set_trace()
        x = (_pisense[motor1]*(ranges[motor1]-_pistroke[motor1]/2.))/1000 + sc.motor_position_by_name(_hexmotoraxis[motor1])
        y = (_pisense[motor2]*(ranges[motor2]-_pistroke[motor2]/2.))/1000 + sc.motor_position_by_name(_hexmotoraxis[motor2])
        #print(ranges,sc.motor_position_by_name("thx"),sc.motor_position_by_name("thy"))
        #print y.mean()
        #pdb.set_trace()
        #print(sc.motor_position_by_name("thx"),sc.motor_position_by_name("thy"))
        x, y = pl.meshgrid(x,y)
        #x = sc.data_column_by_name("adcY")/1000 + sc.motor_position_by_name("thx")
        #y = sc.data_column_by_name("adcX")/1000 + sc.motor_position_by_name("thy")
        ###

        I0 = sc.data_column_by_name(monitor) if monitor in sc.labels else 1.
        for ii, col in enumerate(plotcols):
            roidata = sc.data_column_by_name(col)/I0
            if not roidata.size:
                continue
            #if not (roidata>0).any():
            #    continue
            #try:
            #    x = x.reshape(pscan_shape)
            #except:
            #    continue
            #y = y.reshape(pscan_shape)
            #roidata = roidata.reshape(pscan_shape)
            roidata.resize(pscan_shape)
            #roidata /= pl.median(roidata)
            roidata.T
            vmin, vmax = image.percentile_interval(roidata[roidata>0], 0, 100)
            VMIN[col] = min(vmin, VMIN[col])
            VMAX[col] = max(vmax, VMAX[col])
            #pdb.set_trace()
            plotdata[col].append((y,x, roidata))



for ii, col in enumerate(plotdata):
    fig = pl.figure(col)
    if not fig.axes:
        ax = fig.add_subplot(111)
    else:
        ax = fig.axes[0]
    norm = _norm(4, .8e1)
    for (x,y,roidata) in plotdata[col]:
        im = ax.pcolormesh(x, y, roidata, cmap=cmap, norm=norm)
    pl.colorbar(im)
    pl.xlabel("%s (mm)"%_hexmotoraxis[motor2])
    pl.ylabel("%s (mm)"%_hexmotoraxis[motor1])
    ax.set_aspect('equal', adjustable="box-forced")
    ax.set_title(col)

pl.show()
