import matplotlib.ticker
import numpy as np
import os
import sys
import h5py
import time
import glob
import matplotlib.pyplot as plt
import matplotlib as mpl
import xrayutilities as xu

from matplotlib.patches import Rectangle
from matplotlib.colors import LogNorm
from mpl_toolkits.axes_grid1 import make_axes_locatable, ImageGrid
from silx.io import specfile as spec
from scipy import ndimage
from mayavi import mlab

from id01sware.id01lib.xrd.qconversion import get_qspace_vals
from id01sware.id01lib.xrd.detectors import MaxiPix
from id01sware.id01lib.xrd.geometries import ID01psic

def h5ls(h5file, subdir='/'):
    with h5py.File(h5file) as h5f:
        for i, v in h5f[subdir].items():
            if isinstance(v, h5py.Group):
                print('+ \033[1m {0}\033[0m ({1} members)'.format(i, len(v.keys())))
            elif isinstance(v, h5py.Dataset):
                if len(v.shape)==0:
                    print('- {0}: {1}'.format(i, v[()]))
                else:
                    print('+ \033[1m {0} \033[0m \n - {1}\n - {2}\n - {3}'.format(
                        i, v.shape, v.dtype, v.compression))

def find_roi_idx(roi, qx, qy, qz):

    qx, qy, qz = qx[:,0,0], qy[0,:,0], qz[0,0,:]
    diffx = abs(roi[0]-qx), abs(roi[1]-qx)
    diffy = abs(roi[2]-qy), abs(roi[3]-qy)
    diffz = abs(roi[4]-qz), abs(roi[5]-qz)

    idx_roi = [diffx[0].argmin(), diffx[1].argmin(), diffy[0].argmin(), diffy[1].argmin(), diffz[0].argmin(), diffz[1].argmin()]

    return idx_roi

def hotmaxipix(frame):
    """
    Accepts a single Maxipix frame and corrects for hot pixels.
    """
    frame[304,96] = 1
    frame[304,97] = 1
    frame[230,314] = 1
    frame[471,243] = 1
    frame[161,453] = 1
    frame[160,453] = 1
    frame[159,479] = 1
    frame[201,52] = 1
    frame[162,453] = 1
    frame[409,240] = 1
    frame[498,347] = 1
    frame[509,175] = 1
    frame[280,105] = 1
    frame[281,106] = 1
    frame[60,53] = 1
    frame[44,160] =1
    frame[230,340] = 1
    frame[414,282] =1
    frame[225,247] =1

    return frame

def get_eta(h5file, sample, scan_no):
    with h5py.File(h5file,'r') as h5f:
        data = h5f[sample+'/'+scan_no+'.1/measurement/eta'][()].astype(float)
    return eta

def get_data_roi2(h5file,sample,scan_no):
    with h5py.File(h5file,'r') as h5f:
        data = h5f[sample+'/'+scan_no+'.1/measurement/image_0/data'][()].astype('float32')
        roi2 = h5f[sample+'/'+scan_no+'.1/measurement/roi2'][()].astype('float32')

        return data, roi2

def stitch_scans(h5file,sample,scans, correct=False):
    imgs, roi2s = [], []

    # load the images and the rois for each scan
    for s in scans:
        img, roi2 = get_data_roi2(h5file,sample,str(s))
        imgs.append(img)
        roi2s.append(roi2)

    # calculate the ratio and correct each image by it
    if correct:
        for index in range(1,len(scans)):
            ratio = roi2s[index-1][-1]/roi2s[index][0]
            roi2s[index] *= ratio
            imgs[index][imgs[index]>1] *= ratio

    # make a full stacked array for the scans
    final_array = np.concatenate([*imgs],axis=0)

    # correct imgs in stack for zeroes and hotpix
    for index in range(final_array.shape[0]):
        img = final_array[index,:,:]
        img[img==0.] = 1.
        img = hotmaxipix(img)

        final_array[index,:,:] = img

    # for scan in imgs:
    #     for index in range(scan.shape[0]):
    #         frame = scan[index,:,:]
    #         frame[frame==0.] = 1.
    #         frame = hotmaxipix(frame)

    return final_array

def mlab_axes_tweak(axes):

    # Tweak the appearance of the axes labels
    axes.property.line_width = 2

    axes.label_text_property.font_family = 'arial'
    axes.label_text_property.orientation = 0
    axes.label_text_property.italic = False
    axes.label_text_property.bold = False
    # axes.label_text_property.frame = True


    axes.title_text_property.font_family = 'arial'
    # axes.title_text_property.frame = True
    # axes.title_text_property.font_file = '/users/zatterin/.local/share/fonts/DroidSans-Bold.ttf'

    axes.axes.font_factor = 1.65
    axes.axes.label_format = '%2.2f'
    axes.axes.corner_offset = 0.1

    axes.axes.x_label = 'Qx'
    axes.axes.y_label = 'Qy'
    axes.axes.z_label = 'Qz'

def plone(s, fig, cont, op,  m, mm, vminp=0, vmaxp=0, xshift=0.05, yshift=0.05, zshift=0.05,
          t=1e-6, projections=True, transp=True, colors=None, cmap='file', cmap2='file', z=100,
         extent=None):

    # make the projections
    x,y,z = s[3].sum(0), s[3].sum(1), s[3][:,:,:z].sum(2)

    # Source the 3D data
    surf = mlab.pipeline.scalar_field(*s, figure=fig)

    # Plot it
    if colors is None:
        colors = [None]*len(cont)

    for lvl, o, c in zip(cont, op, colors):
        mlab.pipeline.iso_surface(surf, contours=[lvl], color=c, colormap=cmap,
        opacity=o, transparent=transp, vmin=m, vmax=mm)

    # Source the projections
    if projections:
        proj = []
        for img in (x,y,z):
            im = mlab.pipeline.array2d_source(img, figure=fig)
            proj.append(im)

        # Tweak spacing and origin
        origin = surf.origin
        spacing = surf.spacing

        for p in proj:
            p.origin = origin
        proj[0].spacing = spacing[1], spacing[2], spacing[0]
        proj[1].spacing = spacing[0], spacing[2], spacing[1]
        proj[2].spacing = spacing[0], spacing[1], spacing[2]

        # Threshold them and make the surfaces
        psurf = []
        for p, i in zip(proj, (x,y,z)):
            tresh = mlab.pipeline.threshold(p, low=i.min()+t)
            stresh = mlab.pipeline.surface(tresh, colormap=cmap2, vmin=i.min()+t+vminp, vmax=i.max()+vmaxp)
            print(i.min(), i.max())
            stresh.enable_contours = True
            stresh.contour.number_of_contours = 30
            stresh.contour.filled_contours = True
            psurf.append(stresh)

        # Tweak position of projection
        psurf[0].actor.actor.origin = origin
        psurf[1].actor.actor.origin = origin
        psurf[2].actor.actor.origin = origin

        psurf[0].actor.actor.position = [0-xshift,0,0]
        psurf[1].actor.actor.position = [0,0-yshift,0]
        psurf[2].actor.actor.position = [0,0,0-zshift]

        # Tweak orientation of projection
        psurf[1].actor.actor.orientation = [90,0,0]
        psurf[0].actor.actor.orientation = [90,90,0]

    # Tweak the axes
    qx, qy, qz = s[0][:,0,0], s[1][0,:,0], s[2][0,0,:]

    if extent is None:
        ext = [qx.min()-xshift, qx.max(), qy.min()-yshift, qy.max(), qz.min()-zshift, qz.max()]
    elif extent is not None:
        ext = extent

    axes = mlab.axes(surf, extent=ext,nb_labels=3)
    mlab.outline(extent=ext)
    mlab_axes_tweak(axes)

    fig.scene.parallel_projection = True
    fig.scene.line_smoothing = True
    fig.scene.render_window.point_smoothing = True
    fig.scene.render_window.line_smoothing = True
    fig.scene.render_window.polygon_smoothing = True
    fig.scene.render_window.multi_samples = 8 # Try with 4 if you think this is slow
    # fig.scene.anti_alising_frames = 20
    # fig.scene.render_window.aa_frames = 8 # Try with 4 if you think this is slow

#     mlab.view(azimuth=59)
    fig.scene.reset_zoom()
    fig.scene.light_manager.lights[1].intensity=1
    fig.scene.jpeg_quality = 100

    # mlab.colorbar()

    return fig, surf

def shift_kmap(fname, d):
    """
    d is a dictionary with eta_angle: [shiftx, shifty] (in pixels)
    """
    with h5py.File(fname, libver='latest') as f:

        # Init
        print('Loaded {0}'.format(fname))
        t = time.time()

        # Select the scan
        entry = list(f.values())[0]
        det = entry["instrument/detector"]
        data = det["data"]
        eta = str(entry['instrument/positioners/eta'][()])

        # Get the shift in pixels
        shift = d[eta]
        print("Calculated shift: {0} rows, {1} cols for eta={2}".format(shift[0], shift[1], eta))

        # Shape of the kmap
        shape = tuple(entry["scan/motor_%i_steps"%i][()] for i in (0,1))

        # Get the shapes of the data
        cs = data.chunks[1:]
        ds = data.shape

        # Create a temporary file
        t2 = 0.
        tmpfile = os.path.join("/tmp", "kmap%i%f%s"%(hash(fname), time.time(), os.path.basename(fname)))
        print('Temp file created: {0}'.format(tmpfile))
        tmpfile = h5py.File(tmpfile)

        # Create a new dataset in the tempfile
        if "data" not in tmpfile:
            datanew = tmpfile.create_dataset("data_shifted",
                                 shape=data.shape,
                                 chunks=data.chunks,
                                 dtype=data.dtype,
                                 compression="gzip",
                                 compression_opts=4)

        # For each chunked data point
        for i1 in range(ds[1]//cs[0]):
            for i2 in range(ds[2]//cs[1]):

                # Read the data point
                _t2 = time.time()
                chunk = data[:,
                         i1*cs[0]:(i1+1)*cs[0],
                         i2*cs[1]:(i2+1)*cs[1]].astype(np.float32)
                t2 += time.time()-_t2
                chunk= chunk.reshape(shape[::-1]+(-1,))

                # If shifts are non-zero within tolerance
                if not np.allclose(shift, 0, atol=1e-3):
                    for i in range(chunk.shape[-1]):

                        # Shift the data point
                        chunk[...,i] = ndimage.shift(chunk[...,i], shift)

                print('Shift Done')
                chunk.resize((np.prod(shape),) + cs)
                chunk = (chunk*10).astype(np.uint16)


                # Write the shifted chunks
                _t2 = time.time()
                datanew[:,
                     i1*cs[0]:(i1+1)*cs[0],
                     i2*cs[1]:(i2+1)*cs[1]] = chunk
                t2 += time.time()-_t2
                print('Writing Done!')
                del chunk
        del det["data"]
        del entry['measurement/image/data'] # remove both positions of data
        f.flush()

    tmpfile.flush()

    # Now copy the tmp file shifted data to the original file
    with h5py.File(fname, libver='latest') as f:
        entry = list(f.values())[0]
        det = entry["instrument/detector"]
        det.copy(datanew, "data")
        entry['measurement/image/data'] = det["data"] # hard link
        f.flush()

    tmpfile.clear()
    tmpfile.close()

    print("%s finished after %fs. I/O time: %f"%(fname, time.time()-t, t2))

def read_kmap(specfile, scan_no, rois, motors=False, imshow=False):
    """
    out: dictionary with rois/motors
    """
    # read the specfile
    sf = spec.SpecFile(specfile)
    # get the data
    data = sf[scan_no].data
    # get spec indexes
    index_list = sf.labels(scan_no)
    actual_num = sf.keys()[scan_no]
    # init results dict
    res = {}

    for index, roi in enumerate(rois):
        # load data
        rawdata = data[index_list.index(roi)]
        # remove hot pixels
        rawdata[rawdata>1e7] = 0
        # load motor positions
        motor_1 = data[index_list.index('adcX')]
        motor_2 = data[index_list.index('adcY')]
        command = sf.command(scan_no).split() # the command typed in spec
        new_shape = (int(command[8]), int(command[4]))
        # apply dimensions
        rawdata.shape, motor_1.shape, motor_2.shape = new_shape, new_shape, new_shape
        # append to dict
        if motors:
            res['motors'] = (motor_1, motor_2)
        if imshow:
            res[roi] = rawdata.T[::-1]
        else:
            res[roi] = rawdata

    return res

def plot_kmap(specfile, scan_no, rois, log=False, axeslabels=False, save=False,
              saveformat='pdf', savedir=None, extra=None, plotshape=None,
              colormap='viridis', m=',', scatter=False, **kwargs):

    # get the data
    data = specfile[scan_no].data

    # get the spec indexes (list)
    index_list = specfile.labels(scan_no)

    # other
    sfname = specfile.file_header(0)[0][::-1][21::-1]
    thx = specfile[scan_no].motor_position_by_name('thx')
    thy = specfile[scan_no].motor_position_by_name('thy')
    date = specfile.scan_header(scan_no)[3][3:]
    phi = specfile[scan_no].motor_position_by_name('phi')
    eta = np.round(specfile[scan_no].motor_position_by_name('eta'), decimals=3)
    delval = np.round(specfile[scan_no].motor_position_by_name('del'), decimals=3)

    # init the figure
    fig = plt.figure(edgecolor='red',**kwargs)
    fig.suptitle('{0} #{1} | {2}'.format(sfname, scan_no, extra))
    grid = ImageGrid(fig, 111,
                    nrows_ncols=plotshape,
                    axes_pad = (0.1,0.5),
                    cbar_mode = 'each',
                    cbar_size = '2%',
                    cbar_pad = '2%',
                    cbar_location = 'top')

    for index, roi in enumerate(rois):

        # load data
        rawdata =  data[index_list.index(roi)]
        rawdata[rawdata > 1e8] = 1

        # load motor positions
        motor_1 = data[index_list.index('adcX')]
        motor_2 = data[index_list.index('adcY')]
        command = specfile.command(scan_no).split()
        new_shape = (int(command[8]), int(command[4]))

        # apply dimensions
        rawdata.shape, motor_1.shape, motor_2.shape = new_shape, new_shape, new_shape

        # plot it
        if not log:
            I = rawdata
        else:
            I = np.log(rawdata)
        if scatter:
            im = grid[index].scatter(motor_1, motor_2, c=I, cmap=colormap, marker=m)
        else:
            im = grid[index].pcolormesh(motor_1, motor_2, I, cmap=colormap)
        im.axes.set_aspect('equal')
        if axeslabels:
            grid[index].set_ylabel(r'x piezo [$\mu m$]',fontsize=8)
            grid[index].set_xlabel(r'y piezo [$\mu m$]',fontsize=8)
        grid[index].set_title('{0}'.format(roi), pad=30,fontdict={'fontsize':9, 'fontweight':'bold'})
        cbar = grid.cbar_axes[index].colorbar(im)
        cbar.ax.tick_params(labelsize=8)

        plt.gcf().text(0.54, 0.01, 'eta={0}; del={1}; phi={2}\nthx={3}, thy={4}\n{5}'.format(eta, delval, phi, thx, thy, date),
                       size=10, weight='normal', va='top', ha='center', bbox=dict(boxstyle="round", facecolor='orange', alpha=0.1))

        if save:
            plt.savefig('{0}/{1}__{2}.{3}'.format(savedir,sfname,scan_no,saveformat),
                        dpi=100, bbox_inches='tight')

    return grid

def plot_proj(qs, i):
    qx, qy, qz = qs

    ## PLOT
    fig, ax = plt.subplots(1,3,**kwargs)
    n = LogNorm()

    s0 = ax[0].pcolormesh(qy[0,...],qz[0,...],i.sum(0), norm=n)
    ax[0].set_xlabel(r'$Qy$')
    ax[0].set_ylabel(r'$Qz$')
    ax[0].set_title('Proj. along Qx')

    s1 = ax[1].pcolormesh(qx[:,0,:],qz[:,0,:],i.sum(1), norm=n)
    ax[1].set_xlabel(r'$Qx$')
    ax[1].set_ylabel(r'$Qz$')
    ax[1].set_title('Proj. along Qy')

    s2 = ax[2].pcolormesh(qx[...,0],qy[...,0],i.sum(2), norm=n)
    ax[2].set_xlabel(r'$Qx$')
    ax[2].set_ylabel(r'$Qy$')
    ax[2].set_title('Proj. along Qz')

    for a,s in zip(ax, (s0,s1,s2)):
        a.set_aspect('equal')
        cax = make_axes_locatable(a).append_axes('right', size='5%', pad=0.05)
        fig.colorbar(s,cax=cax)

    plt.tight_layout(pad=0.5)
    plt.show()

def plot_roi(i, qs, roi, mask=None, **kwargs):

    qx, qy, qz = qs

    # mask the dataset if you want
    if mask is not None:
        i[~mask]=0

    # draw the roi as rectangles
    p2 = Rectangle((roi[0],roi[2]),roi[1]-roi[0],roi[3]-roi[2],edgecolor='r',facecolor='none') # xy
    p1 = Rectangle((roi[0],roi[4]),roi[1]-roi[0],roi[5]-roi[4],edgecolor='r',facecolor='none') # xz
    p0 = Rectangle((roi[2],roi[4]),roi[3]-roi[2],roi[5]-roi[4],edgecolor='r',facecolor='none') # yz

    ## PLOT
    fig, ax = plt.subplots(1,3,**kwargs)
    n = LogNorm()

    s0 = ax[0].pcolormesh(qy[0,...],qz[0,...],i.sum(0), norm=n)
    ax[0].set_xlabel(r'$Qy$')
    ax[0].set_ylabel(r'$Qz$')
    ax[0].set_title('Proj. along Qx')

    s1 = ax[1].pcolormesh(qx[:,0,:],qz[:,0,:],i.sum(1), norm=n)
    ax[1].set_xlabel(r'$Qx$')
    ax[1].set_ylabel(r'$Qz$')
    ax[1].set_title('Proj. along Qy')

    s2 = ax[2].pcolormesh(qx[...,0],qy[...,0],i.sum(2), norm=n)
    ax[2].set_xlabel(r'$Qx$')
    ax[2].set_ylabel(r'$Qy$')
    ax[2].set_title('Proj. along Qz')

    for a,s,p  in zip(ax, (s0,s1,s2), (p0,p1,p2)):
        a.add_patch(p)
        a.set_aspect('equal')
        cax = make_axes_locatable(a).append_axes('right', size='5%', pad=0.05)
        fig.colorbar(s,cax=cax)

    plt.tight_layout(pad=0.5)

    # return fig

class OOMFormatter(matplotlib.ticker.ScalarFormatter):
    def __init__(self, order=0, fformat="%1.1f", offset=True, mathText=True):
        self.oom = order
        self.fformat = fformat
        matplotlib.ticker.ScalarFormatter.__init__(self,useOffset=offset,useMathText=mathText)
    def _set_orderOfMagnitude(self, nothing):
        self.orderOfMagnitude = self.oom
    def _set_format(self, vmin, vmax):
        self.format = self.fformat
        if self._useMathText:
            self.format = '$%s$' % matplotlib.ticker._mathdefault(self.format)

def texfig(multiplier, fig_width_pt=221.0, number=8):
    # get the fig width with /showthe/columnwidth

    inches_per_pt = 1.0/72.27               # Convert pt to inches
    golden_mean = (np.sqrt(5)-1.0)/2.0      # Aesthetic ratio
    fig_width = fig_width_pt*inches_per_pt  # width in inches
    fig_height =fig_width*golden_mean       # height in inches
    fig_size = [multiplier*fig_width,multiplier*fig_height]

    mpl.rcParams['font.size'] = number
    # print('Font size: {0}'.format(number*multiplier))

    return fig_size

def calc_com(roi, qs, take=None):

    isort = roi.ravel().argsort()[::-1]
    if take is not None:
        isort = isort[:int(take)]
    roisort = roi.ravel()[isort]  - roi.ravel()[isort].min()
    qCOM = [(arr.ravel()[isort]*roisort).sum() / roisort.sum() for arr in qs]

    return qCOM

def make_mask(roi, qs, qxfilm1, qxfilm2, qyfilm1, qyfilm2, normal=True):
    qx, qy, qz = qs

    # first mask
    mask_x = (qx > roi[0]) & (qx < roi[1]) #& (qx==qfilm)
    mask_y = (qy > roi[2]) & (qy < roi[3]) #^ ((qy > -0.04) & (qy < 0.024))
    mask_z = (qz > roi[4]) & (qz < roi[5])

    mask_x, mask_y, mask_z = np.meshgrid(mask_x, mask_y, mask_z, indexing='ij')

    mask = ((mask_x & mask_y) & mask_z)

    # second mask
    mask_x = ((qx > qxfilm1) & (qx < qxfilm2))
    mask_y = ((qy > qyfilm1) & (qy < qyfilm2))
    mask_z = (qz > roi[4]) & (qz < roi[5])

    mask_x, mask_y, mask_z = np.meshgrid(mask_x, mask_y, mask_z, indexing='ij')

    mask2 = ((mask_x & mask_y) & mask_z)

    # apply both
    mask3 = (mask ^ mask2)
    if normal:
        return mask
    elif not normal:
        return mask3

def calc_coms(fname, dset_name, coms_name, roisum_name,
              t, mask, meshed_qs=None, overwrite=False):

    with h5py.File(fname, 'a') as h5f:
        data = h5f['Data']
        ds = data['qspace_sum'].shape[0] # the 3dkmap shape

        # extract q values, and grid them
        if meshed_qs is None:
            qs = [data[x][()] for x in ('qx','qy','qz')]
            qs = np.meshgrid(*qs, indexing='ij')
            qx, qy, qz = qs
        elif meshed_qs is not None: # ugly way...
            qx, qy, qz = meshed_qs
            qs = meshed_qs

        if coms_name in list(data) and not overwrite:
            print('{0} exists and Overwrite set to FALSE'.format(coms_name))
            return

        print('Initialising dataset {0} using {1} strongest pixels...'.format(dset_name, t))
        # mask the datasets
        print('\n')
        for i in range(ds):
            roi = data['qspace'][i,...]
            roi[~mask] = 0
            data[dset_name][i,...] = roi
            print('\rMask {0}/{1} applied'.format(i,ds),end='',flush=True)

        # calculate the COM of the 3D intensity at each kmap point
        qspace_roi = data[dset_name]
        coms = []
        print('\n')
        for i in range(ds):
            coms.append(calc_com(qspace_roi[i,...],qs,take=t))
            print('\rCOM {0}/{1} calculated'.format(i,ds),end='',flush=True)
        coms = np.array(coms)

        # calc the sum within the roi
        roisum = np.zeros(ds)
        print('\n')
        for i in range(ds):
            roisum[i] = qspace_roi[i,...].sum(0).sum(0).sum(0)
            print('\rSumming point {0}/{1} of the roi'.format(i,ds),end='',flush=True)

        # Write the coms to the file
        if coms_name in list(data):
            del data[coms_name]
        if roisum_name in list(data):
            del data[roisum_name]

        print('\n\nWriting data...', end='')
        data.create_dataset(coms_name, data=coms, compression="gzip")
        data.create_dataset(roisum_name, data=roisum, compression="gzip")
        print('Done.')

def make_qsum(fname, savename, overwrite=False):

    with h5py.File(fname, 'r') as h5f:
        data = h5f['Data']
        qspace = data['qspace']
        qs = [data[m][()] for m in ('qx', 'qy', 'qz')]

        if os.path.isfile(savename) and not overwrite:
            print('File {0} exists and overwrite=False'.format(savename))
        else:
            print('Saving qsum...')
            empty_rsm = np.zeros(qspace[0,...].shape)
            for i in range(qspace.shape[0]):
                empty_rsm += qspace[i,...]
                print('\r{0}/{1}'.format(i, qspace.shape[0]), end='', flush=True)

            np.save(savename, empty_rsm)

def calc_polar(comx, comy, comz):
    az = np.degrees(np.arctan2(comx,comy))
    r = np.sqrt(comx**2+comy**2)
    alt = np.degrees(np.arctan2(r,comz))

    return az, alt

def qconvert_stitched(fname, sample, image_data, scans, cen_pix_x, cen_pix_y, distance, geometry, nbins, maxbins=False):

    # Init values
    detector = MaxiPix()
    energy = 8000
    ipdir = [1,0,0]
    ndir = [0,0,1]

    # Read out positioners from h5
    motors = dict()
    with h5py.File(fname,'r') as h5f:
        pos = h5f[str(sample)+'/'+str(scans[0])+'.1/instrument/positioners']
        for motor in pos:
            motors[motor] = pos[motor][()]

    # add mpx offset to central pixel (as meas from det_calib)
    cen_pix_y += motors["mpxz"]/1000. / detector.pixsize[0]
    cen_pix_x -= motors["mpxy"]/1000. / detector.pixsize[1]

    # initialise the experiment class feeding the ID01psic geometry to it
    hxrd = xu.HXRD([1,0,0], [0,0,1], en=8000, qconv=geometry.getQconversion())

    # select the whole detector as the roi
    roi = [0, detector.pixnum[0], 0, detector.pixnum[1]]

    # initalise the area detector
    hxrd.Ang2Q.init_area(detector.directions[0],
                         detector.directions[1],
                         cch1=cen_pix_x,
                         cch2=cen_pix_y,
                         Nch1=detector.pixnum[0],
                         Nch2=detector.pixnum[1],
                         pwidth1=detector.pixsize[0],
                         pwidth2=detector.pixsize[1],
                         distance=distance,
                         roi=roi)

    # get the angles of the ID01psic geometry
    angles = geometry.sample_rot.copy()
    angles.update(geometry.detector_rot)

    # Get eta values and stitch them
    with h5py.File(fname,'r') as h5f:
        eta_vals = []
        for scan in scans:
            eta_vals.append(h5f[str(sample)+'/'+str(scan)+'.1/instrument/positioners/eta'][()])
        eta = np.concatenate((eta_vals))

    # Get the rest of the positioners and correct them for offsets
    maxlen = 1
    motors['eta'] = eta.astype('float32')
    for angle in angles:
        if angle in geometry.usemotors:
            # fetch experimental angle value
            dset = motors[angle if angle is not "delta" else "del"]
            if len(dset.shape): # if it's 0 motor is still, if it's 1 motor is changing during scan
                maxlen = max(maxlen, dset.shape[0])
            position = dset
        else:
            position = 0.
        angles[angle] = position - geometry.offsets[angle]

    print('Offsets used: \n')
    for key, value in geometry.offsets.items():
        print('{0} = {1}'.format(key, value))
    # output: ordered dict with offset corrected angles of interest used during a scan.

    # if the angle is kept constant during a scan, it is a scalar. If that's the case,
    # make it the same shape as the angle(s) which is being varied during a scan
    for angle in angles:
        if np.isscalar(angles[angle]):
            angles[angle] = np.ones(maxlen, dtype=np.float32) * angles[angle]

    # The actual conversion
    qx, qy, qz = hxrd.Ang2Q.area(*angles.values())
    qx, qy, qz = qx.astype('float32'), qy.astype('float32'), qz.astype('float32')

    # calc max bins -- THIS IS SUPER SLOW?
    if maxbins:
        maxbins = []
        safemax = lambda arr: arr.max() if arr.size else 0
        for dim in (qx, qy, qz):
            maxstep = max((safemax(abs(np.diff(dim, axis=j))) for j in range(3)))
            maxbins.append(int(abs(dim.max()-dim.min())/maxstep))
    else:
        maxbins = nbins


    # load images in scan
    num_im = image_data.shape[0]
    for idx in range(num_im):
        frame = image_data[idx]
        detector.correct_image(frame) # remove gaps
        if not idx: # first iteration
            # create cube of empty data
            intensity = np.empty((num_im, frame.shape[0], frame.shape[1])).astype('float32')
            intensity[idx] = frame.astype('float32')
        else:
            intensity[idx,:,:] = frame.astype('float32')

    # grid data
    gridder = xu.Gridder3D(*maxbins)
    gridder(qx, qy, qz, intensity)

    return (gridder.xaxis.astype('float32'), gridder.yaxis.astype('float32'),
            gridder.zaxis.astype('float32'), gridder.data.astype('float32'))
