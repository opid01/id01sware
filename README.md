# Software package for ID01 @ ESRF
This readme is a work in progress.
Have a look at the [wiki](https://gitlab.esrf.fr/zatterin/id01sware/wikis/home)!

We also have a slack workspace: [https://id01.slack.com/](https://id01.slack.com/)

## Structure

- `/bin` contains ready to use applications:
    * `CamView`
    * `pscan_align`
    * `id01_microscope_cofm`
    * `id01_microscope_contrast`
    * `kmap_showroi`
    * `pscan_detector_average`
    * `pscan_live`
    * `pscan_inspect_specfile`
- `/examples` -  examples on how to use the functions `id01lib` comes with;
- `/id01lib` -  the python module;
- `/scripts` -  some scripts that make use of the contents of `id01lib`
    to accomplish more complicated tasks ?;
- `/tests` - some tests.

## Dependencies

- h5py
- matplolib
- numpy
- scipy
- xrayutilities
- silx 0.7.0
- (PIL)
- (SpecClient)

## Simple installation

- Download package as zip or clone the git repository:
```
    git clone https://gitlab.esrf.fr/opid01/id01sware.git
```


- Install package using pip or setup script. One of:
```
    python setup.py build
```
```
    pip install . (when in the id01sware directory)
```

## Developing guidelines 

- populate this in 10 days....
